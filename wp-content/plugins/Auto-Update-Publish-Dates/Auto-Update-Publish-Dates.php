<?php

/*

Plugin Name: Auto-Update Publish Dates

Description: Take a week off from updating your blog and still give the appearence to new visitors of a blog that is updated daily.  Our plug in automatically updates your blog posts publish date with todays date +/- extra days for each additional post.  Your Wordpress blog will always appear up to date with <a href="http://www.approveme.me/plugin" target="_blank">Approve Me's</a> Auto-Updated Blog Posts.

Author: <a href="http://www.approveme.me/plugin" target="_blank">Approve Me</a>

Version: 1.31

*/

$ddps_version = '1.0';



add_action('admin_menu', 'ddps_add_option_pages');

add_action('ddps_postshift', 'ddps_postshift');







// Setup defaults if options do not exist

add_option('ddps_recentpostupdate', FALSE);

add_option('ddps_allpostsupdate',FALSE);

add_option('ddps_recentpostdisplay', 0);

add_option('ddps_allpostsdisplay', 0);

add_option('ddps_commentdate', 0);

function auto_update_install() {
   $auto_db_version="1.0";
   global $wpdb;
   global $auto_db_version;

   $table_name = $wpdb->prefix . "auto_publishdate";
      
   $sql = "CREATE TABLE IF NOT EXISTS $table_name (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`post_date` DATETIME NOT NULL ,
`post_id` INT( 11 ) NOT NULL ,
`change_date` DATETIME NOT NULL
);";

   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
   dbDelta( $sql );
 
   add_option( "auto_db_version", $auto_db_version );
   
}

function auto_update_data_install() {
       global $wpdb;
	     $table_name = $wpdb->prefix . "auto_publishdate";
   $myrows = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_type ='post' and post_status='publish'");

		foreach($myrows as $row){

			$thispost = $row->ID;
			$revision=$thispost."-revision";
			$post_count = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name WHERE post_id='$thispost'" );
			$post_date=$wpdb->get_var( "SELECT post_date FROM $wpdb->posts WHERE post_name='$revision'" );
			if($post_count==0)
			{
			 $rows_affected = $wpdb->insert("$table_name", array( 'post_date' =>$post_date, 'post_id' => $thispost ) );
			  //  $wpdb->query("UPDATE $table_name SET post_date = '$post_date',change_date='$post_count' WHERE post_id= '$thispost'");
			}
			
          
		}
}

register_activation_hook( __FILE__, 'auto_update_install');

register_activation_hook( __FILE__, 'auto_update_data_install');

function ddps_add_option_pages() {

	if (function_exists('add_options_page')) {

		add_options_page('Auto-Update Publish Dates', 'Auto-Update Publish Dates', 8, __FILE__, 'ddps_options_page');

	}		

}


 if((int)get_option('ddps_commentdate')){  
 add_action ('get_comment_date', 'ddps_get_comment_date');
 add_action ('get_comment_time', 'ddps_get_comment_time');
add_action('wp_head', 'ddps_header_css_style');
 }
 
 
function ddps_get_comment_date()
 {
    return; 
 }
 function ddps_get_comment_time()
 {
    return; 
 }
 function ddps_header_css_style()
 {
 echo "<style type=\"text/css\">
.comment a{display:none;}
</style>\n";
 }
 

function ddps_options_page() {


     global $wpdb;
	global $ddps_version;
	$tp = $wpdb->prefix;
	if ($_POST['info_update']=="Restore All Post Back To Original Date")
	{
	    $post_count = $wpdb->get_var("SELECT COUNT(*) FROM {$tp}auto_publishdate" );
		if($post_count>0)
		{
	   $myrows = $wpdb->get_results( "SELECT post_date,post_id FROM {$tp}auto_publishdate" );

		$datediff = $ddps_allpostsdisplay;

		foreach($myrows as $row){
		
		 $post_date=$row->post_date;
		 $post_id=$row->post_id;
		 
		  $wpdb->query("UPDATE {$tp}posts SET post_date = '$post_date' WHERE ID ='$post_id'");
		  
		  
		  }
		 
		}
		 else 
		  {
		  echo "<div id=\"message\" class=\"updated fade\"><p><strong>No post found</strong></p></div>";
		 return ;
		  }
		   echo "<div id=\"message\" class=\"updated fade\"><p><strong>Successfully restored.</strong></p></div>";
	}

	if (isset($_POST['info_update'])) {



		?><div id="message" class="updated fade"><p><strong><?php 



		update_option('ddps_recentpostupdate', (bool)$_POST["ddps_recentpostupdate"]);

		update_option('ddps_allpostsupdate', (bool)$_POST['ddps_allpostsupdate']);

		update_option('ddps_recentpostdisplay', (int)$_POST['ddps_recentpostdisplay']);

		update_option('ddps_allpostsdisplay', (int)$_POST['ddps_allpostsdisplay']);
		
		update_option('ddps_commentdate', (int)$_POST['ddps_commentdate']);

           // add_action ('get_comment_date', 'ddps_get_comment_date');
			//add_filter('get_comment_date', 'ddps_get_comment_date',10,3);
          // if((int)get_option('ddps_commentdate')){  add_action ('get_comment_date', 'ddps_get_comment_date');}
		wp_clear_scheduled_hook('ddps_postshift');

		wp_schedule_event( time(), 'daily', 'ddps_postshift' );

		//ddps_postshift();

		echo "Configuration Updated!";



	    ?></strong></p></div><?php



	}

	?>



	<div class="metabox-holder" style="width:90%">
     <a href="http://www.approveme.me/wp-digital-e-signature/" target="_blank"><img src="http://www.approveme.me/wp-digital-e-signature/img/logo.png" alt="" /></a>	
							<div class="meta-box-sortables">
											<div id="adminform" class="postbox">
				<div class="handlediv" title="Click to toggle"><br /></div>
				<h3 class="hndle"><span>Auto-Update Publish Dates</span></h3>

	<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">

	<input type="hidden" name="info_update" id="info_update" value="true" />

	<table width="100%" border="0" cellspacing="0" cellpadding="6">

	<tr><td width="20%" style="padding-left:12px;height:50px;border-bottom:1px solid #CCCCCC;"><strong>Update Most Recent Post</strong></td>

	<td width="80%" style="border-bottom:1px solid #CCCCCC;"><input type="checkbox" name="ddps_recentpostupdate" value="1" <?php if((int)get_option('ddps_recentpostupdate')){echo "checked";} ?> /></br>
	</br>
	Check this box to automatically update the most recent blog post's published date with the date specified below.

	</td></tr>

	<tr valign="top"><td width="20%" style="padding-left:12px;height:50px;border-bottom:1px solid #CCCCCC;">

		<strong>Disply date of the most recent post </strong>

	</td><td width="80%" style="border-bottom:1px solid #CCCCCC;">

		<?php $recentpostdisplay = (int)get_option('ddps_recentpostdisplay');?>

		<select name="ddps_recentpostdisplay" >

		<option value="0" <?php if($recentpostdisplay == 0) echo "selected" ?>>Current Date</option>

		<?php for($i=1;$i<=30;$i++){?>

		<option value="<?php echo $i; ?>" <?php if((int)$recentpostdisplay == $i) {echo "selected";}else{} ?>><?php $days = $i==1?' Day':' Days';echo $i.$days;?></option>

		<?php }?>

		</select>

		<br /></br>

		Select the date that the first most recent blog post/article should always display.

	</td></tr>

	

	<tr><td width="20%" style="padding-left:12px;height:50px;border-bottom:1px solid #CCCCCC;"><strong>Update all other posts</strong></td>
       
	<td width="80%" style="border-bottom:1px solid #CCCCCC;"><input type="checkbox" name="ddps_allpostsupdate" value="1" <?php if((int)get_option('ddps_allpostsupdate')){echo "checked";} ?> />
     </br></br>
	 Check this box to automatically update all of the other most recent blog posts published date using the interval specified below. 
	</td></tr>

	<tr valign="top"><td width="20%" style="padding-left:12px;height:50px;border-bottom:1px solid #CCCCCC;">

		<strong>Display dates of all other posts (not including most recent posts) </strong>

	</td><td width="80%" style="border-bottom:1px solid #CCCCCC;">

		<input type="text" name="ddps_allpostsdisplay" value="<?php echo htmlspecialchars(get_option('ddps_allpostsdisplay')); ?>" />
		</br></br>
		This is the interval of dates that the other posts will display. All posts will show your most recent posts date minus the number of days selected (in order). So if todays date is February 15th and we selected the "Update all other posts" interval to be 5 days the second most recent posts publish date would be "February 10th" the third most recent posts publish date would be "February 5th" and so on for all remaining posts . The formula would repeat for posts subtracting five days from the most recent publish date.

	</td></tr>

	
<tr><td width="20%" style="padding-left:12px;height:50px;border-bottom:1px solid #CCCCCC;"><strong>Hide Comments Date</strong></td>

	<td width="80%" style="border-bottom:1px solid #CCCCCC;"><input type="checkbox" name="ddps_commentdate" value="1" <?php if((int)get_option('ddps_commentdate')){echo "checked";} ?> /></br>
	</br>
	Check this box to hide the comments date.

	</td></tr>
        

	
     <tr><td width="20%" style="padding-left:12px;height:50px;border-bottom:1px solid #CCCCCC;">
    

<strong>Restors originals publish dates </strong>

	
	</td>
	<td width="80%" style="border-bottom:1px solid #CCCCCC;">
    

		<input type="submit" name="info_update" value="<?php _e('Restore All Post Back To Original Date'); ?>" /></br></br>
		<span style="padding-top:5px;">At any time you can restore all of your posts to their original publish dates. </span>

	
	</td>
	</tr>
<tr><td colspan="2" style="padding-left:12px;height:50px;">
	<div class="submit">

	<input type="submit" name="info_update" id="submit" class="button button-primary" value="<?php _e('Save Changes'); ?>" />

	</div>
</td></tr>
	</table>

	</form>

	</div>
	
	For plugin support and to see what we've been up to please  <a href="http://www.approveme.me/wp-digital-e-signature/" target="_blank">visit us online</a>
    
</div>

	</div>
		</div>
		</div>
	<?php

}

function ddps_postshift() {



	$ddps_recentpostupdate = (int)get_option('ddps_recentpostupdate');

	$ddps_allpostsupdate = (int)get_option('ddps_allpostsupdate');

	$ddps_recentpostdisplay = (int)get_option('ddps_recentpostdisplay');

	$ddps_allpostsdisplay = (int)get_option('ddps_allpostsdisplay');

	if (($ddps_recentpostupdate == 0) && ($ddps_allpostsupdate == 0)) {

		return;

	}



	global $wpdb;

    $newest_post = 0;

	$tp = $wpdb->prefix;

	if ($ddps_recentpostupdate != 0) {

		$newest_post = $wpdb->get_var("

			SELECT ID 

			FROM {$tp}posts

			WHERE post_type = 'post'

			AND post_status = 'publish' 

			ORDER BY post_date desc

			");

		if ($newest_post > 0) { # we have a post to shift

			$new_time = date('Y-m-d H:i:s');

			
			
			$post_count = $wpdb->get_var( "SELECT COUNT(*) FROM {$tp}auto_publishdate WHERE post_id='$newest_post'" );
			$post_date= $wpdb->get_var( "SELECT post_date FROM {$tp}posts WHERE id='$newest_post'" );
			if($post_count==0)
			{
			  $wpdb->query("INSERT INTO {$tp}auto_publishdate(post_date,post_id,change_date)values('$post_date','$newest_post','$new_time')");
			   // $wpdb->query("UPDATE {$tp}auto_publishdate SET post_date = '$post_date',change_date='$new_time' WHERE post_id= '$newest_post'");
			}
            $wpdb->query("UPDATE {$tp}posts SET post_date = '$new_time' WHERE ID = '$newest_post'");
			wp_publish_post($newest_post);

		}

	}

	if ($ddps_allpostsupdate != 0) {

		$myrows = $wpdb->get_results( "SELECT ID FROM {$tp}posts WHERE post_type = 'post' AND post_status = 'publish' AND ID NOT IN(".$newest_post.")" );

		$datediff = $ddps_allpostsdisplay;

		foreach($myrows as $row){

			$date = -$datediff. " Days";

			$this_time = date('Y-m-d H:i:s',strtotime($date));

			$thispost = $row->ID;
			
			$post_count = $wpdb->get_var( "SELECT COUNT(*) FROM {$tp}auto_publishdate WHERE post_id='$thispost'" );
			
			$post_date= $wpdb->get_var( "SELECT post_date FROM {$tp}posts WHERE id='$thispost'" );
			if($post_count==0)
			{
			  $wpdb->query("INSERT INTO {$tp}auto_publishdate(post_date,post_id,change_date)values('$post_date','$newest_post','$new_time')");
	            // $wpdb->query("UPDATE {$tp}auto_publishdate SET post_date = '$post_date',change_date='$new_time' WHERE post_id= '$newest_post'");
			}

			$wpdb->query("UPDATE {$tp}posts SET post_date = '$this_time' WHERE ID = '$thispost'");

			$datediff = $datediff + $ddps_allpostsdisplay;

		}

	}

	

}









function more_reccurences() {

    return array(

        'ddps_schedule' => array('interval' => (3600*24), 'display' => 'Auto-Update Publish Dates')

    );

}



//add_filter('cron_schedules', 'more_reccurences');
if( !wp_next_scheduled( 'ddps_postshift' )){
	wp_schedule_event( time(), 'daily', 'ddps_postshift' );
}

?>