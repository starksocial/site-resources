<?php
class SEOFriendlyImagesPro {
	var $local_version;
	var $plugin_url;
	var $key;
	var $name;
	var $cap;
	var $rules;
	var $colorbox;
	var $global;
	var $tree;
	var $process_parameters;
	
	function SEOFriendlyImagesPro() {
		$this->local_version = "1.0.3";
		$this->plugin_url = trailingslashit( get_bloginfo( 'wpurl' ) ) . PLUGINDIR . '/' . dirname( plugin_basename(__FILE__) );
		$this->key = 'seo-friendly-images';
		$this->name = 'SEO Friendly Images';
		$this->cap = 'manage_options';
		
		$domain_name = 'seo-friendly-images';
		$locale_name = get_locale();
		$mofile_name = dirname( __FILE__ ) . '/languages';
		$mofile_name .= "/$domain_name-$locale_name.mo";
		load_textdomain( 'seo-friendly-images', $mofile_name );
		load_plugin_textdomain( 'seo-friendly-images', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		
		
		$this->add_filters_and_hooks();
		$options = $this->get_options();
		$this->rules = $options['rules'];
		$this->colorbox = $options['colorbox'];
		$this->global = $options['global'];
		$this->tree = null;
		$this->build_tree();
	}
	
	function add_filters_and_hooks() {
		add_action( 'wp_print_scripts', array( $this, 'load_scripts' ) );
		add_action( 'wp_print_styles', array( $this, 'load_styles' ) );		
		add_action( 'admin_menu', array( $this, 'seo_friendly_images_add_pages' ) );
		add_filter( 'the_content', array( $this, 'seo_friendly_images' ), 500 );
		add_filter( 'post_thumbnail_html', array( $this, 'seo_friendly_images_featured' ), 500 );
		add_action( 'wp_head', array( $this, 'no_frames' ), -10 );
	}
	
	function seo_friendly_images_add_pages() {
		$image = $this->plugin_url . '/i/icon.png';
		
		add_menu_page( $this->name, $this->name, $this->cap, 'sfi_settings', array(
			&$this,
			'handle_settings'
		), $image );
		$page_settings = add_submenu_page( 'sfi_settings', $this->name . ' Settings', 'Settings', $this->cap, 'sfi_settings', array(
			&$this,
			'handle_settings'
		) );
		$page_colorbox_options = add_submenu_page( 'sfi_settings', $this->name . ' Colorbox Options', 'Colorbox Options', $this->cap, 'sfi_colorbox_options', array(
			&$this,
			'handle_colorbox_options'
		) );
		$page_about = add_submenu_page( 'sfi_settings', $this->name . ' About', 'About', $this->cap, 'sfi_about', array(
			&$this,
			'handle_about'
		) );
		
		add_action( 'admin_print_scripts-' . $page_settings, array( $this, 'admin_scripts' ) );
		add_action( 'admin_head-' . $page_settings, array( $this, 'options_head_settings' ) );
		add_action( 'admin_head-' . $page_colorbox_options, array( $this, 'options_head_colorbox' ) );
		add_action( 'admin_head-' . $page_about, array( $this, 'options_head_about' ) );
	}

	function head() {
		$options = $this->get_proper_options();
		if ( $options["enable"] == 'on' ) :
			if ( ( $options["attach_internal_images"] == 'img' && $options["colorbox_internal_images"] == 'on' ) || ( $options["attach_external_images"] == 'img' && $options["colorbox_external_images"] == 'on' ) ) :
	?>
	<script>
		jQuery( document ).ready( function( $) {
	<?php
		if ( is_home() || is_front_page() || is_archive() || is_search() ) {
			$children_posts =& get_children( array( 'post_type' => 'post' ) );
			foreach ( $children_posts as $children_post ) {
	?>
				$("a[rel='cbox<?php echo "_" . $children_post->ID ?>']").colorbox();
	<?php
			}
		} else {
	?>
			$("a[rel='cbox']").colorbox();
	<?php
		}
	?>
		});
	</script>
	<?php
			endif;
		endif;
	}
	
	function admin_scripts() {
		if ( ! empty( $_REQUEST['page'] ) ) {
			$page = $_REQUEST['page'];
		} else {
			$page = false;
		}
		if ( $page == 'sfi_settings' ) {
			$script_path = $this->plugin_url . '/javascripts/sfi.js';
			wp_register_script( 'sfi', $script_path );

			wp_enqueue_script( 'sfi' );
		}
	}
	
	function options_head_settings() {
	?>
	<style type="text/css">
	.settings {						
		margin:5px;
	}
	.holder{
		width:750px;
		margin:0 0 10px;
	}
	h4.big{font-size: 18px;margin: 15px 0 10px 0;padding: 0; background:url('<?php echo $this->plugin_url .'/i/arrows.png'; ?>') no-repeat right -37px;}
	h4.big.col{background-position: right 0px;}
	h4#title_global{ background:none;}
	#icon-sfi_settings { background:transparent url( '<?php echo $this->plugin_url .'/i/logo.png'; ?>' ) no-repeat; }
	.line{display:inline-block; width:220px; padding:0 40px 0 0;}
	.line2{display:inline-block; width:190px; padding:0 40px 0 0;}
	#defualt_settings div{margin:0 0 15px;}
	#mainblock .regular-text{border-color: #DFDFDF;background: white; border-radius:3px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px; width:275px;margin: 0;}
	#mainblock .regular-text.small{width:100px;}
	#mainblock .regular-text.smaller{width:130px;margin:4px 0 0;}
	#default_override_div ul li,.rule,ul.lists li{margin-bottom:15px;}
	#default_override_div input[type=checkbox],#global_settings input[type=checkbox]{vertical-align:top;}
	#default_attach_internal_images_div select{margin:0;}
	#rule_buttons{}
	.settings{margin:0;}
	.settingstop{width: 100%;clear: both;float: left;margin-bottom: 20px;}
	.settingstop div{
		display:inline-block;
	}
	.settingssec{display: block;width: 100%;clear: both;}
	.settingssec .settings, .rew{display: inline-block;width: 170px;vertical-align: top;margin:0 0 20px;}
	.radios ul li input[type=radio], ul.lists li input[type=checbox]{vertical-align:top;}
	h4.big:hover{cursor:pointer;}

	</style>
	<script>
		jQuery(document).ready(function($) {
			expand_cbox( 0 );
			
			$('#add_rule').click(function() {
				var temp = 1;
				while ($('#title_rule_' + temp).length != 0) temp = temp + 1;
				var rule = jQuery('#rule_copy').html();
				rule = rule.replace(/number/g, temp );
				$('#rule_buttons').before(rule);
				load_js(temp, false);
				temp = temp + 1;
			});
			$('#remove_rule').click(function() {
				var temp = 1;
				while ($('#title_rule_' + temp).length != 0 ) temp = temp + 1;
				$('#title_rule_' + (temp - 1 )).remove();
				$('#rule_' + (temp - 1) + '_settings_div').remove();
			});
			$('#post-box h4.big').click(function() {
				$(this).toggleClass('col');
				$(this).next().toggle();
			});
		});
	</script>
	<?php		
	}
	
	function options_head_colorbox() {
	?>
	<style type="text/css">
	#icon-sfi_settings { background:transparent url( '<?php echo $this->plugin_url .'/i/logo.png';?>' ) no-repeat; }
	</style>
	<script>
		jQuery( document ).ready(function($) {
			$("#colorbox_theme").change(function() {
				var src = $("option:selected", this).val();
				if (src != "") {
					var $imgTag = "<img src=\"" + "<?php echo $this->plugin_url; echo '/screenshots/screenshot-'; ?>" + src  + ".jpg\" />";
					$("#screenshot_image").empty().html( $imgTag ).fadeIn();
				}
			});
		});
	</script>
	<?php
	}
	
	function options_head_about() {
	?>
	<style type="text/css">
	#icon-sfi_settings { background:transparent url( '<?php echo $this->plugin_url .'/i/logo.png';?>' ) no-repeat; }
	</style>
	<?php
	}
	
	function load_scripts() {
		$options = $this->get_proper_options();
		
		if ( $options["enable"] == 'on' ) {
			if ( ( $options["attach_internal_images"] == 'img' && $options["colorbox_internal_images"] == 'on' ) || ( $options["attach_external_images"] == 'img' && $options["colorbox_external_images"] == 'on' ) ) {
				$script_path = $this->plugin_url . '/javascripts/jquery.colorbox-min.js';
				wp_register_script( 'colorbox', $script_path );
				
				wp_enqueue_script( 'jquery' );
				wp_enqueue_script( 'colorbox' );
			}
		}
	}
	
	function load_styles() {
		$style_path = $this->plugin_url . '/themes/theme' . $this->colorbox['theme'] . '/colorbox.css';
		wp_register_style( 'colorbox', $style_path );

		$options = $this->get_proper_options();
		
		if ( $options["enable"] == 'on' ) {
			if ( ( $options["attach_internal_images"] == 'img' && $options["colorbox_internal_images"] == 'on' ) || ( $options["attach_external_images"] == 'img' && $options["colorbox_external_images"] == 'on' ) ) {				
				wp_enqueue_style( 'colorbox' );
			}
		}
	}
	
	function remove_from_domains( $rule, $domain ) {
		if ( isset( $this->rules[$rule]['domains'] ) ) {
			if ( ! empty( $this->rules[$rule]['domains'] ) ) {
				if ( in_array( $domain, $this->rules[$rule]['domains'] ) ) {
					foreach( $this->rules[$rule]['domains'] as $key => $value ) {
						if ( $value == $domain ) {
							unset( $this->rules[$rule]['domains'][$key] );
						}
					}
				}
			}
		}
	}
	
	function create_rule_html( $rule ) {
		$html = '<div class="postbox holder"><div class="inside">';
		$html .= '<h4 class="big" id="title_rule_' . $rule . '">' . __( 'Rule', 'seo-friendly-images' ) . ' ' . $rule . '</h4>';
		$html .= '<div id="rule_' . $rule . '_settings_div" style="width:710px;" class="settings">';
		$html .= '<div id="rule_' . $rule . '_domains_div" class="settings">';
		$html .= '<div id="rule_' . $rule . '_domain_main_all_div" class="settings settingstop">';
		$html .= '<div id="rule_' . $rule . '_domain_main_div" class="rew">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_main" id="rule_' . $rule . '_domain_main" />';
		$html .= '<label for="rule_' . $rule . '_domain_main">' . __( 'Main Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_subdomains_main_div" class="rew">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_home" id="rule_' . $rule . '_domain_home" />';
		$html .= '<label for="rule_' . $rule . '_domain_home">' . __( 'Home Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_front" id="rule_' . $rule . '_domain_front" />';
		$html .= '<label for="rule_' . $rule . '_domain_front">' . __( 'Front Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '</div>';
		$html .= '<div style="clear:both">';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_archive_all_div">';
		$html .= '<div id="rule_' . $rule . '_domain_archive_div" class="settings">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_archive" id="rule_' . $rule . '_domain_archive" />';
		$html .= '<label for="rule_' . $rule . '_domain_archive">' . __( 'Archive Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '</div>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_subdomains_archive_div" class="settings settingssec">';
		$html .= '<div id="rule_' . $rule . '_domain_category_all_div" class="settings">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_category" id="rule_' . $rule . '_domain_category" />';
		$html .= '<label for="rule_' . $rule . '_domain_category">' . __( 'All Categories', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_domain_category_ids_div">';
		$html .= __( 'or specify by IDs:', 'seo-friendly-images' );
		$html .= '<br />';
		$html .= '<input class="regular-text smaller" type="text" id="rule_' . $rule . '_domain_category_ids" name="rule_' . $rule . '_domain_category_ids" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_tag_all_div" class="settings">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_tag" id="rule_' . $rule . '_domain_tag" />';
		$html .= '<label for="rule_' . $rule . '_domain_tag">' . __( 'All Tags', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_domain_tag_ids_div">';
		$html .= __( 'or specify by IDs:', 'seo-friendly-images' );
		$html .= '<br />';
		$html .= '<input class="regular-text smaller" type="text" id="rule_' . $rule . '_domain_tag_ids" name="rule_' . $rule . '_domain_tag_ids" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_taxonomy_all_div" class="settings">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_taxonomy" id="rule_' . $rule . '_domain_taxonomy" />';
		$html .= '<label for="rule_' . $rule . '_domain_taxonomy">' . __( 'All Taxonomies', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_domain_taxonomy_ids_div">';
		$html .= __( 'or specify by IDs:', 'seo-friendly-images' );
		$html .= '<br />';
		$html .= '<input class="regular-text smaller" type="text" id="rule_' . $rule . '_domain_taxonomy_ids" name="rule_' . $rule . '_domain_taxonomy_ids" style="width:140px;" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_author_all_div" class="settings">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_author" id="rule_' . $rule . '_domain_author" />';
		$html .= '<label for="rule_' . $rule . '_domain_Author">' . __( 'All Authors', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_domain_author_ids_div">';
		$html .= __( 'or specify by IDs:', 'seo-friendly-images' );
		$html .= '<br />';
		$html .= '<input class="regular-text smaller" type="text" id="rule_' . $rule . '_domain_author_ids" name="rule_' . $rule . '_domain_author_ids"/>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_date_all_div" class="settings settingssec">';
		$html .= '<div id="rule_' . $rule . '_subdomains_date_div">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_date" id="rule_' . $rule . '_domain_date" />';
		$html .= '<label for="rule_' . $rule . '_domain_date">' . __( 'Date Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_subdomains_date_div">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_year" id="rule_' . $rule . '_domain_year" />';
		$html .= '<label for="rule_' . $rule . '_domain_year">' . __( 'Year Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_month" id="rule_' . $rule . '_domain_month" />';
		$html .= '<label for="rule_' . $rule . '_domain_month">' . __( 'Month Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_day" id="rule_' . $rule . '_domain_day" />';
		$html .= '<label for="rule_' . $rule . '_domain_day">' . __( 'Day Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_time" id="rule_' . $rule . '_domain_time" />';
		$html .= '<label for="rule_' . $rule . '_domain_time">' . __( 'Time Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '</div>';
		$html .= '<div style="clear:both">';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div style="clear:both">';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_singular_all_div">';
		$html .= '<div id="rule_' . $rule . '_domain_singular_div">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_singular" id="rule_' . $rule . '_domain_singular" />';
		$html .= '<label for="rule_' . $rule . '_domain_singular">' . __( 'Singular Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '</div>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_subdomains_singular_div" class="settings settingssec">';
		$html .= '<div id="rule_' . $rule . '_domain_post_all_div" class="settings">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_post" id="rule_' . $rule . '_domain_post" />';
		$html .= '<label for="rule_' . $rule . '_domain_post">' . __( 'All Posts', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_domain_post_ids_div">';
		$html .= __( 'or specify by IDs:', 'seo-friendly-images' );
		$html .= '<br />';
		$html .= '<input class="regular-text smaller" type="text" id="rule_' . $rule . '_domain_post_ids" name="rule_' . $rule . '_domain_post_ids" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_page_all_div" class="settings settingssec">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_page" id="rule_' . $rule . '_domain_page" />';
		$html .= '<label for="rule_' . $rule . '_domain_page">' . __( 'All Pages', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_domain_page_ids_div">';
		$html .= __( 'or specify by IDs:', 'seo-friendly-images' );
		$html .= '<br />';
		$html .= '<input class="regular-text smaller" type="text" id="rule_' . $rule . '_domain_page_ids" name="rule_' . $rule . '_domain_page_ids" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_domain_attachment_all_div" class="settings settingssec">';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_domain_attachment" id="rule_' . $rule . '_domain_attachment" />';
		$html .= '<label for="rule_' . $rule . '_domain_attachment">' . __( 'All Attachments', 'seo-friendly-images' ) . '</label>';
		$html .= '<br />';
		$html .= '<div id="rule_' . $rule . '_domain_attachment_ids_div">';
		$html .= __( 'or specify by IDs:', 'seo-friendly-images' );
		$html .= '<br />';
		$html .= '<input class="regular-text smaller" type="text" id="rule_' . $rule . '_domain_attachment_ids" name="rule_' . $rule . '_domain_attachment_ids" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div style="clear:both">';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<input type="hidden" id="rule_' . $rule . '_hidden" name="rule_' . $rule . '_hidden" value="1" /><br />';
		$html .= '<ul><li>';
		$html .= '<input type="radio" id="rule_' . $rule . '_enable" name="rule_' . $rule . '_enable" value="enabled" checked /> <label>Enable plugin for the above rules</label></li><li>';
		$html .= '<input type="radio" id="rule_' . $rule . '_enable" name="rule_' . $rule . '_enable" value="disabled" /> <label>Disable plugin for the above rules</label></li></ul>';  
		$html .= '<br /><br /><div id="rule_' . $rule . '_rules_div">';
		$html .= '<div id="rule_' . $rule . '_alt_div" class="rule">';
		$html .= '<label class="line2" for="rule_' . $rule . '_alt">' . __( 'Image <b>ALT</b> attribute:', 'seo-friendly-images' ) . '</label>';
		$html .= '<input class="regular-text"  type="text" name="rule_' . $rule . '_alt" value="%name %title" id="rule_' . $rule . '_alt" />';
		$html .= '<span class="description"> example: %name %title</span>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_title_div" class="rule">';
		$html .= '<label class="line2"  for="rule_' . $rule . '_title">' . __( 'Image <b>TITLE</b> attribute:', 'seo-friendly-images' ) . '</label>';
		$html .= '<input class="regular-text" type="text" name="rule_' . $rule . '_title" value="%name photo" id="rule_' . $rule . '_title" />';
		$html .= '<span class="description"> example: %name photo</span>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_override_div">';
		$html .= '<ul class="lists">';
		$html .= '<li><label class="line2" for="rule_' . $rule . '_override_alt">' . __( 'Override default image alt tag ', 'seo-friendly-images' ) . '</label>';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_override_alt" checked id="rule_' . $rule . '_override_alt" /></li>';
		$html .= '<span class="description">( recommended )</span>';
		$html .= '<li><label class="line2" for="rule_' . $rule . '_override_title">' . __( 'Override default image title tag', 'seo-friendly-images' ) . '</label>';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_override_title" id="rule_' . $rule . '_override_title" /></li>';
		$html .= '<li><label class="line2" for="rule_' . $rule . '_strip_extension_title">' . __( 'Strip extension and delimiter characters from title tag', 'seo-friendly-images' ) . '</label>';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_strip_extension_title" checked id="rule_' . $rule . '_strip_extension_title" /></li>';
		$html .= '</ul>';		
		$html .= '<br />';
		$html .= '<label class="line2" for="rule_' . $rule . '_override_max">' . __( 'Maximum processed images for <stropng>ALT</stropng> and <strong>Title</strong> tags', 'seo-friendly-images' ) . '</label>';
		$html .= '<input class="regular-text small" type="text" name="rule_' . $rule . '_override_max" id="rule_' . $rule . '_override_max" value="2" />';
		$html .= '<span class="description"> default: 2</span>';
		$html .= '</div>';
		$html .= '<br /><div id="rule_' . $rule . '_attach_internal_images_div" class="rule">';
		$html .= '<label class="line2" for="rule_' . $rule . '_attach_internal_images">' . __( 'Automatically link internal images to', 'seo-friendly-images' ) . '</label>';
		$html .= '<select name="rule_' . $rule . '_attach_internal_images" id="rule_' . $rule . '_attach_internal_images">';
		$html .= '<option value="def" selected="yes">' . __( 'Leave as it is', 'seo-friendly-images' ) . '</option>';
		$html .= '<option value="post">' . __( 'Post', 'seo-friendly-images' ) . '</option>';
		$html .= '<option value="att">' . __( 'Attachment page', 'seo-friendly-images' ) . '</option>';
		$html .= '<option value="img">' . __( 'Image file', 'seo-friendly-images' ) . '</option>';
		$html .= '</select>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_colorbox_internal_images_div" class="rule">';
		$html .= '<label class="line2" for="rule_' . $rule . '_colorbox_internal_images">' . __( 'Use Colorbox preview', 'seo-friendly-images' ) . '</label>';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_colorbox_internal_images" id="rule_' . $rule . '_colorbox_internal_images" />';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_attach_external_images_div" class="rule">';
		$html .= '<label class="line2" for="rule_' . $rule . '_attach_external_images">' . __( 'Automatically link external images to', 'seo-friendly-images' ) . '</label>';
		$html .= '<select name="rule_' . $rule . '_attach_external_images" id="rule_' . $rule . '_attach_external_images">';
		$html .= '<option value="def" selected="yes">' . __( 'Leave as it is', 'seo-friendly-images' ) . '</option>';
		$html .= '<option value="post">' . __( 'Post', 'seo-friendly-images' ) . '</option>';
		$html .= '<option value="img">' . __( 'Image file', 'seo-friendly-images' ) . '</option>';
		$html .= '</select>';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_colorbox_external_images_div" class="rule">';
		$html .= '<label class="line2" for="rule_' . $rule . '_colorbox_external_images">' . __( 'Use Colorbox preview', 'seo-friendly-images' ) . '</label>';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_colorbox_external_images" id="rule_' . $rule . '_colorbox_external_images" />';
		$html .= '</div>';
		$html .= '<div id="rule_' . $rule . '_external_links_div" class="rule">';
		$html .= '<label class="line2" for="rule_' . $rule . '_external_links">' . __( 'Do not change image links to external sites', 'seo-friendly-images' ) . '</label>';
		$html .= '<input type="checkbox" name="rule_' . $rule . '_external_links" id="rule_' . $rule . '_external_links" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		return $html;
	}
	
	function handle_settings() {
		if ( isset( $_POST['submitted'] ) ) {
			$this->global['no_frames'] = ( ! isset( $_POST['global_no_frames'] ) ? 'off' : 'on' );
			$this->global['xml_sitemap'] = ( ! isset( $_POST['global_xml_sitemap'] ) ? 'off' : 'on' );
			$this->global['xml_sitemap_num'] = ( ! isset( $_POST['global_xml_sitemap_num'] ) ? '' : $_POST['global_xml_sitemap_num'] );
			$this->rules[0]['domains'] = array( 'all' );			
			$this->rules[0]['options']['alt'] = ( ! isset( $_POST['default_alt'] ) ? '' : $_POST['default_alt'] );
			$this->rules[0]['options']['title'] = ( ! isset( $_POST['default_title'] ) ? '' : $_POST['default_title'] );
			$this->rules[0]['options']['override_alt'] = ( ! isset( $_POST['default_override_alt'] ) ? 'off' : 'on' );
			$this->rules[0]['options']['override_title'] = ( ! isset( $_POST['default_override_title'] ) ? 'off' : 'on' );
			$this->rules[0]['options']['strip_extension_title'] = ( ! isset( $_POST['default_strip_extension_title'] ) ? 'off' : 'on' );
			$this->rules[0]['options']['override_max'] = ( ! isset( $_POST['default_override_max'] ) ? 2 : $_POST['default_override_max'] );
			$this->rules[0]['options']['attach_internal_images'] = ( ! isset( $_POST['default_attach_internal_images'] ) ? '' : $_POST['default_attach_internal_images'] );
			$this->rules[0]['options']['colorbox_internal_images'] = ( ! isset( $_POST['default_colorbox_internal_images'] ) ? 'off' : 'on' );
			$this->rules[0]['options']['attach_external_images'] = ( ! isset( $_POST['default_attach_external_images'] ) ? '' : $_POST['default_attach_external_images'] );
			$this->rules[0]['options']['colorbox_external_images'] = ( ! isset( $_POST['default_colorbox_external_images'] ) ? 'off' : 'on' );			
			$this->rules[0]['options']['external_links'] = ( ! isset( $_POST['default_external_links'] ) ? 'off' : 'on' );
			$this->rules[0]['options']['enable'] = 'on';
			//$this->colorbox['theme'] = ( ! isset( $_POST['colorbox_theme'] ) ? '1' : $_POST['colorbox_theme'] );
			
			if ( $this->global['xml_sitemap'] == 'on' ) {				
				$st = $this->create_xml_sitemap();
				if ( ! $st ) {
					$site_url = get_bloginfo( 'url' );
					preg_match( '/http(s)?:\/\/([^\/]*)\/([^\/]*)/' , $site_url, $matches );
					$relative_path = $matches[3];
					echo '<br /><div class="error"><h2>Oops!</h2><p>The XML sitemap was generated successfully but the plugin was unable to save the xml to your WordPress root folder at <strong>' . $_SERVER["DOCUMENT_ROOT"] . '/' . $relative_path . '</strong>.</p><p>Please ensure that the folder has appropriate <a href="http://codex.wordpress.org/Changing_File_Permissions" target="_blank">write permissions</a>.</p><p> You can either use the chmod command in Unix or use your FTP Manager to change the permission of the folder to 0666 and then try generating the sitemap again.</p></div>';
				}
			} else {
				/*$st = $this->delete_xml_sitemap();
				if ( ! $st ) {
					$site_url = get_bloginfo( 'url' );
					preg_match( '/http(s)?:\/\/([^\/]*)\/([^\/]*)/' , $site_url, $matches );
					$relative_path = $matches[3];
					echo '<br /><div class="error"><h2>Oops!</h2><p>The plugin was unable to delete the xml sitemap in your WordPress root folder at <strong>' . $_SERVER["DOCUMENT_ROOT"] . '/' . $relative_path . '</strong>.</p><p>Please ensure that the folder has appropriate <a href="http://codex.wordpress.org/Changing_File_Permissions" target="_blank">write permissions</a>.</p><p> You can either use the chmod command in Unix or use your FTP Manager to change the permission of the folder to 0666 and then try generating the sitemap again.</p></div>';
				}*/
			}
			
			$i = 1;
			while ( isset( $_POST['rule_' . $i . '_hidden'] ) ) {
				$this->rules[$i]['domains'] = array();
				if ( isset( $_POST['rule_' . $i . '_domain_main'] ) ) {
					array_push( $this->rules[$i]['domains'], 'main' );
					$this->remove_from_domains( $i, 'home' );
					$this->remove_from_domains( $i, 'front' );
				} else {
					if ( isset( $_POST['rule_' . $i . '_domain_home'] ) ) {
						array_push( $this->rules[$i]['domains'], 'home' );
					}
					if ( isset( $_POST['rule_' . $i . '_domain_front'] ) ) {
						array_push( $this->rules[$i]['domains'], 'front' );
					}
				}
				if ( isset( $_POST['rule_' . $i . '_domain_archive'] ) ) {
					if ( ( $_POST['rule_' . $i . '_domain_category_ids'] == '' ) && ( $_POST['rule_' . $i . '_domain_tag_ids'] == '' ) && ( $_POST['rule_' . $i . '_domain_taxonomy_ids'] == '' ) && ( $_POST['rule_' . $i . '_domain_author_ids'] == '' ) ) {
						array_push( $this->rules[$i]['domains'], 'archive' );
						$this->remove_from_domains( $i, 'category' );
						$this->remove_from_domains( $i, 'tag' );
						$this->remove_from_domains( $i, 'taxonomy' );
						$this->remove_from_domains( $i, 'author' );
						$this->remove_from_domains( $i, 'date' );
						$this->remove_from_domains( $i, 'year' );
						$this->remove_from_domains( $i, 'month' );
						$this->remove_from_domains( $i, 'day' );
						$this->remove_from_domains( $i, 'time' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_category_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_category_ids'] ) ) {
							$ids = $_POST['rule_' . $i . '_domain_category_ids'];
								$array_ids = explode( ',', $ids );
								$this->rules[$i]['domains']['category'] = $array_ids;
							$this->remove_from_domains( $i, 'category' );
						} else {
							array_push( $this->rules[$i]['domains'], 'category' );
							unset( $this->rules[$i]['domains']['category'] );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_tag_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_tag_ids'] ) ) {
							$ids = $_POST['rule_' . $i . '_domain_tag_ids'];
								$array_ids = explode( ',', $ids );
								$this->rules[$i]['domains']['tag'] = $array_ids;
							$this->remove_from_domains( $i, 'tag' );
						} else {
							array_push( $this->rules[$i]['domains'], 'tag' );
							unset( $this->rules[$i]['domains']['tag'] );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_taxonomy_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_taxonomy_ids'] ) ) {
							$ids = $_POST['rule_' . $i . '_domain_taxonomy_ids'];
								$array_ids = explode( ',', $ids );
								$this->rules[$i]['domains']['taxonomy'] = $array_ids;
							$this->remove_from_domains( $i, 'taxonomy' );
						} else {
							array_push( $this->rules[$i]['domains'], 'taxonomy' );
							unset( $this->rules[$i]['domains']['taxonomy'] );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_author_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_author_ids'] ) ) {
							$ids = $_POST['rule_' . $i . '_domain_author_ids'];
								$array_ids = explode( ',', $ids );
								$this->rules[$i]['domains']['author'] = $array_ids;
							$this->remove_from_domains( $i, 'author' );
						} else {
							array_push( $this->rules[$i]['domains'], 'author' );
							unset( $this->rules[$i]['domains']['author'] );
						}
						array_push( $this->rules[$i]['domains'], 'date' );
						$this->remove_from_domains( $i, 'year' );
						$this->remove_from_domains( $i, 'month' );
						$this->remove_from_domains( $i, 'day' );
						$this->remove_from_domains( $i, 'time' );
					}
				} else {
					if ( isset( $_POST['rule_' . $i . '_domain_category_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_category_ids'] ) ) {
						$ids = $_POST['rule_' . $i . '_domain_category_ids'];
							$array_ids = explode( ',', $ids );
							$this->rules[$i]['domains']['category'] = $array_ids;
						$this->remove_from_domains( $i, 'category' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_category'] ) ) {
							array_push( $this->rules[$i]['domains'], 'category' );
						}
						unset( $this->rules[$i]['domains']['category'] );
					}
					if ( isset( $_POST['rule_' . $i . '_domain_tag_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_tag_ids'] ) ) {
						$ids = $_POST['rule_' . $i . '_domain_tag_ids'];
							$array_ids = explode( ',', $ids );
							$this->rules[$i]['domains']['tag'] = $array_ids;
						$this->remove_from_domains( $i, 'tag' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_tag'] ) ) {
							array_push( $this->rules[$i]['domains'], 'tag' );
						}
						unset( $this->rules[$i]['domains']['tag'] );
					}
					if ( isset( $_POST['rule_' . $i . '_domain_taxonomy_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_taxonomy_ids'] ) ) {
						$ids = $_POST['rule_' . $i . '_domain_taxonomy_ids'];
							$array_ids = explode( ',', $ids );
							$this->rules[$i]['domains']['taxonomy'] = $array_ids;
						$this->remove_from_domains( $i, 'taxonomy' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_taxonomy'] ) ) {
							array_push( $this->rules[$i]['domains'], 'taxonomy' );
						}
						unset( $this->rules[$i]['domains']['taxonomy'] );
					}
					if ( isset( $_POST['rule_' . $i . '_domain_author_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_author_ids'] ) ) {
						$ids = $_POST['rule_' . $i . '_domain_author_ids'];
							$array_ids = explode( ',', $ids );
							$this->rules[$i]['domains']['author'] = $array_ids;
						$this->remove_from_domains( $i, 'author' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_author'] ) ) {
							array_push( $this->rules[$i]['domains'], 'author' );
						}
						unset( $this->rules[$i]['domains']['author'] );
					}
					if ( isset( $_POST['rule_' . $i . '_domain_date'] ) ) {
						array_push( $this->rules[$i]['domains'], 'date' );
						$this->remove_from_domains( $i, 'year' );
						$this->remove_from_domains( $i, 'month' );
						$this->remove_from_domains( $i, 'day' );
						$this->remove_from_domains( $i, 'time' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_year'] ) ) {
							array_push( $this->rules[$i]['domains'], 'year' );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_month'] ) ) {
							array_push( $this->rules[$i]['domains'], 'month' );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_day'] ) ) {
							array_push( $this->rules[$i]['domains'], 'day' );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_time'] ) ) {
							array_push( $this->rules[$i]['domains'], 'time' );
						}
					}
				}
				if ( isset( $_POST['rule_' . $i . '_domain_singular'] ) ) {
					if ( ( $_POST['rule_' . $i . '_domain_post_ids'] == '' ) && ( $_POST['rule_' . $i . '_domain_page_ids'] == '' ) && ( $_POST['rule_' . $i . '_domain_attachment_ids'] == '' ) ) {
						array_push( $this->rules[$i]['domains'], 'singular' );
						$this->remove_from_domains( $i, 'post' );
						$this->remove_from_domains( $i, 'page' );
						$this->remove_from_domains( $i, 'attachment' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_post_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_post_ids'] ) ) {
							$ids = $_POST['rule_' . $i . '_domain_post_ids'];
								$array_ids = explode( ',', $ids );
								$this->rules[$i]['domains']['post'] = $array_ids;
						} else {
							array_push( $this->rules[$i]['domains'], 'post' );
							unset( $this->rules[$i]['domains']['post'] );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_page_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_page_ids'] ) ) {
							$ids = $_POST['rule_' . $i . '_domain_page_ids'];
								$array_ids = explode( ',', $ids );
								$this->rules[$i]['domains']['page'] = $array_ids;
						} else {
							array_push( $this->rules[$i]['domains'], 'page' );
							unset( $this->rules[$i]['domains']['page'] );
						}
						if ( isset( $_POST['rule_' . $i . '_domain_attachment_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_attachment_ids'] ) ) {
							$ids = $_POST['rule_' . $i . '_domain_attachment_ids'];
								$array_ids = explode( ',', $ids );
								$this->rules[$i]['domains']['attachment'] = $array_ids;
						} else {
							array_push( $this->rules[$i]['domains'], 'attachment' );
							unset( $this->rules[$i]['domains']['attachment'] );
						}
					}
				} else {
					if ( isset( $_POST['rule_' . $i . '_domain_post_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_post_ids'] ) ) {
						$ids = $_POST['rule_' . $i . '_domain_post_ids'];
							$array_ids = explode( ',', $ids );
							$this->rules[$i]['domains']['post'] = $array_ids;
						$this->remove_from_domains( $i, 'post' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_post'] ) ) {
							array_push( $this->rules[$i]['domains'], 'post' );
						}
						unset( $this->rules[$i]['domains']['post'] );
					}
					if ( isset( $_POST['rule_' . $i . '_domain_page_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_page_ids'] ) ) {
						$ids = $_POST['rule_' . $i . '_domain_page_ids'];
							$array_ids = explode( ',', $ids );
							$this->rules[$i]['domains']['page'] = $array_ids;
						$this->remove_from_domains( $i, 'page' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_page'] ) ) {
							array_push( $this->rules[$i]['domains'], 'page' );
						}
						unset( $this->rules[$i]['domains']['page'] );
					}
					if ( isset( $_POST['rule_' . $i . '_domain_attachment_ids'] ) && ( "" != $_POST['rule_' . $i . '_domain_attachment_ids'] ) ) {
						$ids = $_POST['rule_' . $i . '_domain_attachment_ids'];
							$array_ids = explode( ',', $ids );
							$this->rules[$i]['domains']['attachment'] = $array_ids;
						$this->remove_from_domains( $i, 'attachment' );
					} else {
						if ( isset( $_POST['rule_' . $i . '_domain_attachment'] ) ) {
							array_push( $this->rules[$i]['domains'], 'attachment' );
						}
						unset( $this->rules[$i]['domains']['attachment'] );
					}
				}
				
				$this->rules[$i]['options']['enable'] = ( ! isset( $_POST['rule_' . $i . '_enable'] ) ? 'off' : ( ( $_POST['rule_' . $i . '_enable'] == 'enabled' ) ? 'on' : 'off' ) );
				
				if ( $this->rules[$i]['options']['enable'] == 'on' ) {
					$this->rules[$i]['options']['alt'] = ( ! isset( $_POST['rule_' . $i . '_alt'] ) ? '' : $_POST['default_alt'] );
					$this->rules[$i]['options']['title'] = ( ! isset( $_POST['rule_' . $i . '_title'] ) ? '' : $_POST['default_title'] );
					$this->rules[$i]['options']['override_alt'] = ( ! isset( $_POST['rule_' . $i . '_override_alt'] ) ? 'off' : 'on' );
					$this->rules[$i]['options']['override_title'] = ( ! isset( $_POST['rule_' . $i . '_override_title'] ) ? 'off' : 'on' );
					$this->rules[$i]['options']['strip_extension_title'] = ( ! isset( $_POST['rule_' . $i . '_strip_extension_title'] ) ? 'off' : 'on' );
					$this->rules[$i]['options']['override_max'] = ( ! isset( $_POST['rule_' . $i . '_override_max'] ) ? 2 : $_POST['rule_' . $i . '_override_max'] );
					$this->rules[$i]['options']['attach_internal_images'] = ( ! isset( $_POST['rule_' . $i . '_attach_internal_images'] ) ? '' : $_POST['rule_' . $i . '_attach_internal_images'] );
					$this->rules[$i]['options']['colorbox_internal_images'] = ( ! isset( $_POST['rule_' . $i . '_colorbox_internal_images'] ) ? 'off' : 'on' );
					$this->rules[$i]['options']['attach_external_images'] = ( ! isset( $_POST['rule_' . $i . '_attach_external_images'] ) ? '' : $_POST['rule_' . $i . '_attach_external_images'] );
					$this->rules[$i]['options']['colorbox_external_images'] = ( ! isset( $_POST['rule_' . $i . '_colorbox_external_images'] ) ? 'off' : 'on' );			
					$this->rules[$i]['options']['external_links'] = ( ! isset( $_POST['rule_' . $i . '_external_links'] ) ? 'off' : 'on' );
				} else {
					unset( $this->rules[$i]['options']['alt'] );
					unset( $this->rules[$i]['options']['title'] );
					unset( $this->rules[$i]['options']['override_alt'] );
					unset( $this->rules[$i]['options']['override_title'] );
					unset( $this->rules[$i]['options']['strip_extension_title'] );
					unset( $this->rules[$i]['options']['override_max'] );
					unset( $this->rules[$i]['options']['attach_internal_images'] );
					unset( $this->rules[$i]['options']['colorbox_internal_images'] );
					unset( $this->rules[$i]['options']['attach_external_images'] );
					unset( $this->rules[$i]['options']['colorbox_external_images'] );			
					unset( $this->rules[$i]['options']['external_links'] );
				}  
				
				$i++;
			}
			
			for ( $j = $i; $j <= count( $this->rules ); $j++ ) {
				unset( $this->rules[$j] );
			}
			
			$options['rules'] = $this->rules;
			$options['colorbox'] = $this->colorbox;
			$options['global'] = $this->global;
			
			update_option( $this->key, $options );
			$this->tree = null;
			$this->build_tree();
			
			$msg_status = __( 'SEO Friendly Images settings saved.', 'seo-friendly-images' );
			
			// Show message
			echo '<div id="message" class="updated fade"><p>' . $msg_status . '</p></div>';
		}
		
		// Fetch code from DB
		if (isset($this->rules))
		foreach ( $this->rules as $key => $rule ) {
			$form[$key]['domains'] = $rule['domains'];
			$form[$key]['options'] = $rule['options'];
			$form[$key]['options']['enable'] = ( $rule['options']['enable'] == 'on' ) ? 'checked' : '';
			if ( $key == 0 || $rule['options']['enable'] == 'on' ) {
				$form[$key]['options']['override_alt'] = ( $rule['options']['override_alt'] == 'on' ) ? 'checked' : '';
				$form[$key]['options']['override_title'] = ( $rule['options']['override_title'] == 'on' ) ?'checked' : '';
				$form[$key]['options']['strip_extension_title'] = ( $rule['options']['strip_extension_title'] == 'on' ) ?'checked' : '';
				$form[$key]['options']['override_max'] = $rule['options']['override_max'];
				$form[$key]['options']['colorbox_internal_images'] = ( $rule['options']['colorbox_internal_images'] == 'on' ) ? 'checked' : '';
				$form[$key]['options']['colorbox_external_images'] = ( $rule['options']['colorbox_external_images'] == 'on' ) ? 'checked' : '';
				$form[$key]['options']['external_links'] = ( $rule['options']['external_links'] == 'on' ) ? 'checked' : '';
			}
		}
		$form['global']['no_frames'] = ( $this->global['no_frames'] == 'on' ) ? 'checked' : '';
		$form['global']['xml_sitemap'] = ( $this->global['xml_sitemap'] == 'on' ) ? 'checked' : '';
		$form['global']['xml_sitemap_num'] =  $this->global['xml_sitemap_num'];
		
		$imgpath = $this->plugin_url . '/i';
		$actionurl = $_SERVER['REQUEST_URI'];
		// Configuration Page
	?>
	<div class="wrap">
		<?php screen_icon(); ?>
		<h2><?php _e( 'SEO Friendly Images Business', 'seo-friendly-images' ); echo '&nbsp;' . $this->local_version; ?></h2>
		<a href="admin.php?page=sfi_settings"><?php _e( 'Settings', 'seo-friendly-images' ); ?></a> &nbsp;|&nbsp; <a href="admin.php?page=sfi_colorbox_options"><?php _e( 'Colorbox Options', 'seo-friendly-images' ); ?></a> &nbsp;|&nbsp; <a href="admin.php?page=sfi_about"><?php _e( 'About', 'seo-friendly-images' ); ?></a>
		<div id="poststuff" style="margin-top:10px;">
		
			<div id="sideblock" style="float:right;width:270px;margin-left:10px;">		  
				<iframe width=270 height=800 frameborder="0" src="http://www.prelovac.com/plugin/news.php?id=102&utm_source=plugin&utm_medium=plugin&utm_campaign=SEO%2BFriendly%2BImages%2BPro"></iframe>
			</div>
		</div>
		<div id="mainblock" class="submit">
			<div class="dbx-content">
				<form name="sfiform" action="<?php echo $actionurl; ?>" method="post">
					<input type="hidden" name="submitted" value="1" />		 
					<p><?php _e( 'SEO Friendly Images Business automatically adds ALT and Title attributes to all your images in all your posts specified by parameters below.', 'seo-friendly-images' ); ?></p>						 
					<p><strong><?php _e( 'Plugin supports several special tags:', 'seo-friendly-images' ); ?></strong></p>
					<ul>
						<li><b>%title</b> - <?php _e( 'replaces post title', 'seo-friendly-images' ); ?></li>
						<li><b>%name</b> - <?php _e( 'replaces image file name ( without extension )', 'seo-friendly-images' ); ?></li>
						<li><b>%category</b> - <?php _e( 'replaces post category', 'seo-friendly-images' ); ?></li>
						<li><b>%tags</b> - <?php _e( 'replaces post tags', 'seo-friendly-images' ); ?></li>
					</ul>
					<p>
						<strong><?php _e( 'Example:', 'seo-friendly-images' ); ?></strong>
						<?php _e( 'In a post titled Car Pictures there is a picture named Ferrari.jpg', 'seo-friendly-images' ); ?><br /><br />
						<?php _e( 'Setting alt attribute to <b>"%name %title"</b> will produce alt="Ferrari Car Pictures"', 'seo-friendly-images' ); ?><br />
						<?php _e( 'Setting title attribute to <b>"%name photo"</b> will produce title="Ferrari photo"', 'seo-friendly-images' ); ?>
					</p>
					<div id="poststuff" class="postbox holder">
					<h3 class="hndle"><span><?php _e( 'Settings', 'seo-friendly-images' ); ?></span></h3>
					<div class="inside">
					<h4 id="title_global" class="big"><?php _e( 'Global', 'seo-friendly-images' ); ?></h4>
					<div id="global_settings" style="width:710px;" class="settings">
					<ul>
					<li>
						<label class="line" for="global_no_frames"><?php _e( 'Prevent external sites from showing this site in an iframe', 'seo-friendly-images' ); ?></label>
						<input type="checkbox" id="global_no_frames" name="global_no_frames" <?php echo $form['global']['no_frames']; ?> />
					</li>
					<li>
						<label class="line" for="global_xml_sitemap"><?php _e( 'Generate Google Images Sitemap', 'seo-friendly-images' ); ?></label>
						<input type="checkbox" id="global_xml_sitemap" name="global_xml_sitemap" <?php echo $form['global']['xml_sitemap']; ?> />
						<br /><br /><label for="global_no_frames" class="line">Max. Number of posts to process for sitemap</label> <input class="regular-text small" type="text" id="global_xml_sitemap_num" name="global_xml_sitemap_num" value="<?php echo $form['global']['xml_sitemap_num']; ?>" /> <span class="description"> lower it if it takes too much time or servers times-out</span> 
						<br /><br /> Sitemap path: <a href="<?php echo get_bloginfo('url').'/sitemap-image.xml'; ?>"><?php echo get_bloginfo('url').'/sitemap-image.xml'; ?></a>
					</li>
				  </ul>
					</div>
					<h4 id="title_global" class="big"><?php _e( 'Default', 'seo-friendly-images' ); ?></h4>
					<div id="defualt_settings" style="width:710px;" class="settings">
						<div id="default_alt_div">
							<label class="line" for="default_alt"><?php _e( 'Image <b>ALT</b> attribute:', 'seo-friendly-images' ); ?></label>
							<input class="regular-text" type="text" id="default_alt" name="default_alt" value="<?php echo $form[0]['options']['alt']; ?>" />
							<span class="description"><?php _e( 'example: %name %title', 'seo-friendly-images' ); ?></span>
						</div>
						<div id="default_title_div">
							<label class="line" for="default_title"><?php _e( 'Image <b>TITLE</b> attribute:', 'seo-friendly-images' ); ?></label>
							<input class="regular-text" type="text" id="default_title" name="default_title" value="<?php echo $form[0]['options']['title']; ?>" />
							<span class="description"><?php _e( 'example: %name photo', 'seo-friendly-images' ); ?></span>
						</div>
						<div id="default_override_div">
						<ul>
						<li>
							<label class="line" for="default_override_alt"><?php _e( 'Override default image alt tag', 'seo-friendly-images' ); ?></label>
							<input type="checkbox" id="default_override_alt" name="default_override_alt" <?php echo $form[0]['options']['override_alt']; ?> />
							<?php _e( '<span class="description">( recommended )</span>', 'seo-friendly-images' ); ?>
						</li>
						<li>
						<label class="line" for="default_override_title"><?php _e( 'Override default image title tag', 'seo-friendly-images' ); ?></label>
						<input type="checkbox" id="default_override_title" name="default_override_title" <?php echo $form[0]['options']['override_title']; ?> />
						</li>
						<li>
					<label class="line" for="default_strip_extension_title"><?php _e( 'Strip extension and delimiter characters from title tag', 'seo-friendly-images' ); ?></label>
					<input type="checkbox" id="default_strip_extension_title" name="default_strip_extension_title" <?php echo $form[0]['options']['strip_extension_title']; ?> />
						</li>
						<li>						
							<label class="line" for="default_override_max"><?php _e( 'Maximum processed images for <strong>ALT</strong> and <strong>Title</strong> tags', 'seo-friendly-images' ); ?></label>
							 <input class="regular-text small" type="text" id="default_override_max" name="default_override_max" value="<?php echo ( isset( $form[0]['options']['override_max'] ) ? $form[0]['options']['override_max'] : 2 ); ?>" />
							<span class="description"> default: 2</span>
						</li>
						</ul>
						</div>
						<div id="default_attach_internal_images_div">
							<label class="line" for="default_attach_internal_images"><?php _e( 'Automatically link internal images to', 'seo-friendly-images' ); ?></label>
							<select id="default_attach_internal_images" name="default_attach_internal_images">
								<option value="def" <?php echo ( ( $form[0]['options']['attach_internal_images']=='def' ) ? 'selected="yes"' : '' ); ?>><?php _e( 'Leave as it is', 'seo-friendly-images' ); ?></option>
								<option value="post" <?php echo ( ( $form[0]['options']['attach_internal_images']=='post' ) ? 'selected="yes"' : '' ); ?>><?php _e( 'Post', 'seo-friendly-images' ); ?></option>
								<option value="att" <?php echo ( ( $form[0]['options']['attach_internal_images']=='att' ) ? 'selected="yes"' : '' ); ?>><?php _e( 'Attachment page', 'seo-friendly-images' ); ?></option>
								<option value="img" <?php echo ( ( $form[0]['options']['attach_internal_images']=='img' ) ? 'selected="yes"' : '' ); ?>><?php _e( 'Image file', 'seo-friendly-images' ); ?></option>					
							</select>
						</div>
						<div id="default_colorbox_internal_images_div">
							<label class="line" for="default_colorbox_internal_images"><?php _e( 'Use Colorbox preview', 'seo-friendly-images' ); ?></label>
							<input type="checkbox" id="default_colorbox_internal_images" name="default_colorbox_internal_images" <?php echo $form[0]['options']['colorbox_internal_images']; ?> />

						</div>
						<div id="default_attach_external_images_div">
							<label class="line" for="default_attach_external_images"><?php _e( 'Automatically link external images to', 'seo-friendly-images' ); ?></label>
							<select id="default_attach_external_images" name="default_attach_external_images">
								<option value="def" <?php echo ( ( $form[0]['options']['attach_external_images']=='def' ) ? 'selected="yes"' : '' ); ?>><?php _e( 'Leave as it is', 'seo-friendly-images' ); ?></option>
								<option value="post" <?php echo ( ( $form[0]['options']['attach_external_images']=='post' ) ? 'selected="yes"' : '' ); ?>><?php _e( 'Post', 'seo-friendly-images' ); ?></option>
								<option value="img" <?php echo ( ( $form[0]['options']['attach_external_images']=='img' ) ? 'selected="yes"' : '' ); ?>><?php _e( 'Image file', 'seo-friendly-images' ); ?></option>					
							</select>
						</div>
						<div id="default_colorbox_external_images_div">
							<label class="line" for="default_colorbox_external_images"><?php _e( 'Use Colorbox preview', 'seo-friendly-images' ); ?></label>
							<input type="checkbox" id="default_colorbox_external_images" name="default_colorbox_external_images" <?php echo $form[0]['options']['colorbox_external_images']; ?> />	
						</div>
						<div id="default_external_links_div">
							<label class="line" for="default_external_links"><?php _e( 'Retain links to external sites', 'seo-friendly-images' ); ?></label>
							<input type="checkbox" id="default_external_links" name="default_external_links" <?php echo $form[0]['options']['external_links']; ?> />
						</div>
					</div>
				</div>
				</div>
					<?php for ( $i = 1; $i < count( $form ) - 1; $i++ ): ?>
					<script type="text/javascript">
					jQuery( document ).ready( function( $){
						load_js(<?php echo $i; ?>, true );
					});
					</script>
					<div id="post-box" class="postbox holder">
					<div class="inside">
					<h4 class="big" id="title_rule_<?php echo $i; ?>"><?php echo __( 'Rule', 'seo-friendly-images' ) . ' ' . $i; ?></h4>
					<div id="rule_<?php echo $i; ?>_settings_div" style="width:710px;" class="settings">
						<div id="rule_<?php echo $i; ?>_domains_div" class="settings">
							<div id="rule_<?php echo $i; ?>_domain_main_all_div" class="settings settingstop">
								<div id="rule_<?php echo $i; ?>_domain_main_div" class="rew">
									<input type="checkbox" id="rule_<?php echo $i; ?>_domain_main" name="rule_<?php echo $i; ?>_domain_main" <?php echo ( in_array( 'main', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> />
									<label for="rule_<?php echo $i; ?>_domain_main"><?php _e( 'Main Pages', 'seo-friendly-images' ); ?></label>
								</div>
								<div id="rule_<?php echo $i; ?>_subdomains_main_div">
									<input type="checkbox" id="rule_<?php echo $i; ?>_domain_home" name="rule_<?php echo $i; ?>_domain_home" <?php echo ( in_array( 'main', $form[$i]['domains'] ) || in_array( 'home', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'main', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
									<label for="rule_<?php echo $i; ?>_domain_home"><?php _e( 'Home Pages', 'seo-friendly-images' ); ?></label>
									<br />
									<input type="checkbox" id="rule_<?php echo $i; ?>_domain_front" name="rule_<?php echo $i; ?>_domain_front" <?php echo ( in_array( 'main', $form[$i]['domains'] ) || in_array( 'front', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'main', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
									<label for="rule_<?php echo $i; ?>_domain_front"><?php _e( 'Front Pages', 'seo-friendly-images' ); ?></label>
								</div>
								<div style="clear:both">
								</div>
							</div>
							<div id="rule_<?php echo $i; ?>_domain_archive_all_div" class="settings settingssec">
								<div id="rule_<?php echo $i; ?>_domain_archive_div">
									<input type="checkbox" id="rule_<?php echo $i; ?>_domain_archive" name="rule_<?php echo $i; ?>_domain_archive" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> />
									<label for="rule_<?php echo $i; ?>_domain_archive"><?php _e( 'Archive Pages', 'seo-friendly-images' ); ?></label>
								</div>
								<br />
								<div id="rule_<?php echo $i; ?>_subdomains_archive_div">
									<div id="rule_<?php echo $i; ?>_domain_category_all_div" class="settings">
										<input type="checkbox" id="rule_<?php echo $i; ?>_domain_category" name="rule_<?php echo $i; ?>_domain_category" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'category', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
										<label for="rule_<?php echo $i; ?>_domain_category"><?php _e( 'All Categories', 'seo-friendly-images' ); ?></label>
										<br />
										<div id="rule_<?php echo $i; ?>_domain_category_ids_div">
											<?php _e( 'or specify by IDs:', 'seo-friendly-images' ); ?>
											<br />
											<input class="regular-text smaller" type="text" id="rule_<?php echo $i; ?>_domain_category_ids" name="rule_<?php echo $i; ?>_domain_category_ids" value="<?php echo ( isset( $form[$i]['domains']['category'] ) ) ? implode( ',', $form[$i]['domains']['category'] ) : ''; ?>" />
										</div>
									</div>
									<div id="rule_<?php echo $i; ?>_domain_tag_all_div" class="settings">
										<input type="checkbox" id="rule_<?php echo $i; ?>_domain_tag" name="rule_<?php echo $i; ?>_domain_tag" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'tag', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
										<label for="rule_<?php echo $i; ?>_domain_tag"><?php _e( 'All Tags', 'seo-friendly-images' ); ?></label>
										<br />
										<div id="rule_<?php echo $i; ?>_domain_tag_ids_div">
											<?php _e( 'or specify by IDs:', 'seo-friendly-images' ); ?>
											<br />
											<input class="regular-text smaller" type="text" id="rule_<?php echo $i; ?>_domain_tag_ids" name="rule_<?php echo $i; ?>_domain_tag_ids" value="<?php echo ( isset( $form[$i]['domains']['tag'] ) ) ? implode( ',', $form[$i]['domains']['tag'] ) : ''; ?>" />
										</div>
									</div>
									<div id="rule_<?php echo $i; ?>_domain_taxonomy_all_div" class="settings">
										<input type="checkbox" id="rule_<?php echo $i; ?>_domain_taxonomy" name="rule_<?php echo $i; ?>_domain_taxonomy" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'taxonomy', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
										<label for="rule_<?php echo $i; ?>_domain_taxonomy"><?php _e( 'All Taxonomies', 'seo-friendly-images' ); ?></label>
										<br />
										<div id="rule_<?php echo $i; ?>_domain_taxonomy_ids_div">
											<?php _e( 'or specify by IDs:', 'seo-friendly-images' ); ?>
											<br />
											<input class="regular-text smaller" type="text" id="rule_<?php echo $i; ?>_domain_taxonomy_ids" name="rule_<?php echo $i; ?>_domain_taxonomy_ids"  value="<?php echo ( isset( $form[$i]['domains']['taxonomy'] ) ) ? implode( ',', $form[$i]['domains']['taxonomy'] ) : ''; ?>" />
										</div>
									</div>
									<div id="rule_<?php echo $i; ?>_domain_author_all_div" class="settings">
										<input type="checkbox" id="rule_<?php echo $i; ?>_domain_author" name="rule_<?php echo $i; ?>_domain_author" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'author', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
										<label for="rule_<?php echo $i; ?>_domain_Author"><?php _e( 'All Authors', 'seo-friendly-images' ); ?></label>
										<br />
										<div id="rule_<?php echo $i; ?>_domain_author_ids_div">
											<?php _e( 'or specify by IDs:', 'seo-friendly-images' ); ?>
											<br />
											<input class="regular-text smaller" type="text" id="rule_<?php echo $i; ?>_domain_author_ids" name="rule_<?php echo $i; ?>_domain_author_ids"  value="<?php echo ( isset( $form[$i]['domains']['author'] ) ) ? implode( ',', $form[$i]['domains']['author'] ) : ''; ?>" />
										</div>
									</div>
									<div id="rule_<?php echo $i; ?>_domain_date_all_div" class="settings settingssec">
										<div id="rule_<?php echo $i; ?>_subdomains_date_div">
											<input type="checkbox" id="rule_<?php echo $i; ?>_domain_date" name="rule_<?php echo $i; ?>_domain_date" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
											<label for="rule_<?php echo $i; ?>_domain_date"><?php _e( 'Date Pages', 'seo-friendly-images' ); ?></label>
										</div>
										<div id="rule_<?php echo $i; ?>_subdomains_date_div" class="settings">
											<input type="checkbox" id="rule_<?php echo $i; ?>_domain_year" name="rule_<?php echo $i; ?>_domain_year" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) || in_array( 'year', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
											<label for="rule_<?php echo $i; ?>_domain_year"><?php _e( 'Year Pages', 'seo-friendly-images' ); ?></label>
											<br />
											<input type="checkbox" id="rule_<?php echo $i; ?>_domain_month" name="rule_<?php echo $i; ?>_domain_month" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) || in_array( 'month', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
											<label for="rule_<?php echo $i; ?>_domain_month"><?php _e( 'Month Pages', 'seo-friendly-images' ); ?></label>
											<br />
											<input type="checkbox" id="rule_<?php echo $i; ?>_domain_day" name="rule_<?php echo $i; ?>_domain_day" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) || in_array( 'day', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
											<label for="rule_<?php echo $i; ?>_domain_day"><?php _e( 'Day Pages', 'seo-friendly-images' ); ?></label>
											<br />
											<input type="checkbox" id="rule_<?php echo $i; ?>_domain_time" name="rule_<?php echo $i; ?>_domain_time" <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) || in_array( 'time', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> <?php echo ( in_array( 'archive', $form[$i]['domains'] ) || in_array( 'date', $form[$i]['domains'] ) ) ? 'disabled' : ''; ?> />
											<label for="rule_<?php echo $i; ?>_domain_time"><?php _e( 'Time Pages', 'seo-friendly-images' ); ?></label>
										</div>
										<div style="clear:both">
										</div>
									</div>
								</div>
								<div style="clear:both">
								</div>
							</div>
							<div id="rule_<?php echo $i; ?>_domain_singular_all_div" class="settings settingssec">
								<div id="rule_<?php echo $i; ?>_domain_singular_div">
									<input type="checkbox" id="rule_<?php echo $i; ?>_domain_singular" name="rule_<?php echo $i; ?>_domain_singular" <?php echo ( in_array( 'singular', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> />
									<label for="rule_<?php echo $i; ?>_domain_singular"><?php _e( 'Singular Pages', 'seo-friendly-images' ); ?></label>
								</div>
								<br />
								<div id="rule_<?php echo $i; ?>_subdomains_singular_div">
									<div id="rule_<?php echo $i; ?>_domain_post_all_div" class="settings settingssec">
										<input type="checkbox" id="rule_<?php echo $i; ?>_domain_post" name="rule_<?php echo $i; ?>_domain_post" <?php echo ( in_array( 'singular', $form[$i]['domains'] ) || in_array( 'post', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> />
										<label for="rule_<?php echo $i; ?>_domain_post"><?php _e( 'All Posts', 'seo-friendly-images' ); ?></label>
										<br />
										<div id="rule_<?php echo $i; ?>_domain_post_ids_div">
											<?php _e( 'or specify by IDs:', 'seo-friendly-images' ); ?>
											<br />
											<input class="regular-text smaller" type="text" id="rule_<?php echo $i; ?>_domain_post_ids" name="rule_<?php echo $i; ?>_domain_post_ids"  value="<?php echo ( isset( $form[$i]['domains']['post'] ) ) ? implode( ',', $form[$i]['domains']['post'] ) : ''; ?>" />
										</div>
									</div>
									<div id="rule_<?php echo $i; ?>_domain_page_all_div" class="settings settingssec">
										<input type="checkbox" id="rule_<?php echo $i; ?>_domain_page" name="rule_<?php echo $i; ?>_domain_page" <?php echo ( in_array( 'singular', $form[$i]['domains'] ) || in_array( 'page', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> />
										<label for="rule_<?php echo $i; ?>_domain_page"><?php _e( 'All Pages', 'seo-friendly-images' ); ?></label>
										<br />
										<div id="rule_<?php echo $i; ?>_domain_page_ids_div">
											<?php _e( 'or specify by IDs:', 'seo-friendly-images' ); ?>
											<br />
											<input class="regular-text smaller" type="text" id="rule_<?php echo $i; ?>_domain_page_ids" name="rule_<?php echo $i; ?>_domain_page_ids" value="<?php echo ( isset( $form[$i]['domains']['page'] ) ) ? implode( ',', $form[$i]['domains']['page'] ) : ''; ?>" />
										</div>
									</div>
									<div id="rule_<?php echo $i; ?>_domain_attachment_all_div" class="settings settingssec">
										<input type="checkbox" id="rule_<?php echo $i; ?>_domain_attachment" name="rule_<?php echo $i; ?>_domain_attachment" <?php echo ( in_array( 'singular', $form[$i]['domains'] ) || in_array( 'attachment', $form[$i]['domains'] ) ) ? 'checked' : ''; ?> />
										<label for="rule_<?php echo $i; ?>_domain_attachment"><?php _e( 'All Attachments', 'seo-friendly-images' ); ?></label>
										<br />
										<div id="rule_<?php echo $i; ?>_domain_attachment_ids_div">
											<?php _e( 'or specify by IDs:', 'seo-friendly-images' ); ?>
											<br />
											<input class="regular-text smaller" type="text" id="rule_<?php echo $i; ?>_domain_attachment_ids" name="rule_<?php echo $i; ?>_domain_attachment_ids" value="<?php echo ( isset( $form[$i]['domains']['attachment'] ) ) ? implode( ',', $form[$i]['domains']['attachment'] ) : ''; ?>" />
										</div>
									</div>
								</div>
								<div style="clear:both">
								</div>
							</div>
						</div>
						<input type="hidden" id="rule_<?php echo $i; ?>_hidden" name="rule_<?php echo $i; ?>_hidden" value="1" />
						<br />
					   <ul class="radios">
					   <li>
						<input type="radio" id="rule_<?php echo $i; ?>_enable" name="rule_<?php echo $i; ?>_enable" value="enabled" <?php echo $form[$i]['options']['enable']; ?> /> <label>Enable plugin for the above rules</label>
						</li>
						<li>
						<input type="radio" id="rule_<?php echo $i; ?>_enable" name="rule_<?php echo $i; ?>_enable" value="disabled" <?php echo ( ( $form[$i]['options']['enable'] == "" ) ? "checked" : "" ); ?> /> <label>Disable plugin for the above rules</label></li>
						</ul>
						<br />
						
						<div id="rule_<?php echo $i; ?>_rules_div" <?php echo ( ( $form[$i]['options']['enable'] == 'checked' ) ? "" : "style='display:none;'" ); ?> >
							<div id="rule_<?php echo $i; ?>_alt_div" class="rule">
								<label class="line2" for="rule_<?php echo $i; ?>_alt"><?php _e( 'Image <b>ALT</b> attribute:', 'seo-friendly-images' ); ?></label>
								<input class="regular-text" type="text" id="rule_<?php echo $i; ?>_alt" name="rule_<?php echo $i; ?>_alt" value="<?php echo ( isset( $form[$i]['options']['alt'] ) ) ? $form[$i]['options']['alt'] : '%name %title'; ?>" />
								<span class="description"><?php _e( 'example: %name %title', 'seo-friendly-images' ); ?></span>
							</div>
							<div id="rule_<?php echo $i; ?>_title_div" class="rule">
								<label class="line2" for="rule_<?php echo $i; ?>_title"><?php _e( 'Image <b>TITLE</b> attribute:', 'seo-friendly-images' ); ?></label>
								<input class="regular-text" type="text" id="rule_<?php echo $i; ?>_title" name="rule_<?php echo $i; ?>_title" value="<?php echo ( isset( $form[$i]['options']['title'] ) ) ? $form[$i]['options']['title'] : '%name photo'; ?>" />
								<span class="description"><?php _e( 'example: %name photo', 'seo-friendly-images' ); ?></span>
							</div>
							<div id="rule_<?php echo $i; ?>_override_div" class="rule">
							<ul class="lists">
							<li>
								<label class="line2" for="rule_<?php echo $i; ?>_override_alt"><?php _e( 'Override default image alt tag', 'seo-friendly-images' ); ?></label>
								<input type="checkbox" id="rule_<?php echo $i; ?>_override_alt" name="rule_<?php echo $i; ?>_override_alt" <?php echo ( isset( $form[$i]['options']['override_alt'] ) ) ? $form[$i]['options']['override_alt'] : 'checked'; ?> />
								<span class="description"><?php _e( '( recommended )', 'seo-friendly-images' ); ?></span>
							</li>
							<li>
								<label class="line2" for="rule_<?php echo $i; ?>_override_title"><?php _e( 'Override default image title tag', 'seo-friendly-images' ); ?></label>
								<input type="checkbox" name="rule_<?php echo $i; ?>_override_title" id="rule_<?php echo $i; ?>_override_title" <?php echo ( isset( $form[$i]['options']['override_title'] ) ? $form[$i]['options']['override_title'] : '' ); ?> />
							</li>
							<li>
								<label class="line2" for="rule_<?php echo $i; ?>_strip_extension_title"><?php _e( 'Strip extension and delimiter characters from title tag', 'seo-friendly-images' ); ?></label>
								<input type="checkbox" name="rule_<?php echo $i; ?>_strip_extension_title" id="rule_<?php echo $i; ?>_strip_extension_title" <?php echo ( isset( $form[$i]['options']['strip_extension_title'] ) ? $form[$i]['options']['strip_extension_title'] : '' ); ?> />

							</li>
							<li>
								<label class="line2" for="rule_<?php echo $i; ?>_override_max"><?php _e( 'Maximum processed images for <strong>ALT</strong> and <strong>Title</strong> tags:', 'seo-friendly-images' ); ?></label>
								<input class="regular-text small" type="text" name="rule_<?php echo $i; ?>_override_max" id="rule_<?php echo $i; ?>_override_max" value="<?php echo ( isset( $form[$i]['options']['override_max'] ) ? $form[$i]['options']['override_max'] : 2 ); ?>" />
								<span class="description"><?php _e( 'default: 2', 'seo-friendly-images' ); ?></span>
							</li>
							</ul>								
							</div>
							<br />
							<div id="rule_<?php echo $i; ?>_attach_internal_images_div" class="rule">
								<label class="line2" for="rule_<?php echo $i; ?>_attach_internal_images"><?php _e( 'Automatically link images to', 'seo-friendly-images' ); ?></label>
								<select id="rule_<?php echo $i; ?>_attach_internal_images" name="rule_<?php echo $i; ?>_attach_internal_images">
									<option value="def" <?php echo ( ! isset( $form[$i]['options']['attach_internal_images'] ) ? 'selected="yes"' : ( ( $form[$i]['options']['attach_internal_images']=='def' ) ? 'selected="yes"' : '' ) ); ?>><?php _e( 'Leave as it is', 'seo-friendly-images' ); ?></option>
									<option value="post" <?php echo ( ! isset( $form[$i]['options']['attach_internal_images'] ) ? '' : ( ( $form[$i]['options']['attach_internal_images']=='post' ) ? 'selected="yes"' : '' ) ); ?>><?php _e( 'Post', 'seo-friendly-images' ); ?></option>
									<option value="att" <?php echo ( ! isset( $form[$i]['options']['attach_internal_images'] ) ? '' : ( ( $form[$i]['options']['attach_internal_images']=='att' ) ? 'selected="yes"' : '' ) ); ?>><?php _e( 'Attachment page', 'seo-friendly-images' ); ?></option>
									<option value="img" <?php echo ( ! isset( $form[$i]['options']['attach_internal_images'] ) ? '' : ( ( $form[$i]['options']['attach_internal_images']=='img' ) ? 'selected="yes"' : '' ) ); ?>><?php _e( 'Image file', 'seo-friendly-images' ); ?></option>
								</select>
							</div>
							<div id="rule_<?php echo $i; ?>_colorbox_internal_images_div" class="rule">
								<label class="line2" for="rule_<?php echo $i; ?>_colorbox_internal_images"><?php _e( 'Use Colorbox preview', 'seo-friendly-images' ); ?></label>
								<input type="checkbox" id="rule_<?php echo $i; ?>_colorbox_internal_images" name="rule_<?php echo $i; ?>_colorbox_internal_images" <?php echo $form[$i]['options']['colorbox_internal_images']; ?> />
							</div>
							<div id="rule_<?php echo $i; ?>_attach_external_images_div" class="rule">
								<label class="line2" for="rule_<?php echo $i; ?>_attach_external_images"><?php _e( 'Automatically link external images to', 'seo-friendly-images' ); ?></label>
								<select id="rule_<?php echo $i; ?>_attach_external_images" name="rule_<?php echo $i; ?>_attach_external_images">
									<option value="def" <?php echo ( ! isset( $form[$i]['options']['attach_external_images'] ) ? 'selected="yes"' : ( ( $form[$i]['options']['attach_external_images']=='def' ) ? 'selected="yes"' : '' ) ); ?>><?php _e( 'Leave as it is', 'seo-friendly-images' ); ?></option>
									<option value="post" <?php echo ( ! isset( $form[$i]['options']['attach_external_images'] ) ? '' : ( ( $form[$i]['options']['attach_external_images']=='post' ) ? 'selected="yes"' : '' ) ); ?>><?php _e( 'Post', 'seo-friendly-images' ); ?></option>
									<option value="img" <?php echo ( ! isset( $form[$i]['options']['attach_external_images'] ) ? '' : ( ( $form[$i]['options']['attach_external_images']=='img' ) ? 'selected="yes"' : '' ) ); ?>><?php _e( 'Image file', 'seo-friendly-images' ); ?></option>
								</select>
							</div>
							<div id="rule_<?php echo $i; ?>_colorbox_external_images_div" class="rule">
								<label class="line2" for="rule_<?php echo $i; ?>_colorbox_external_images"><?php _e( 'Use Colorbox preview', 'seo-friendly-images' ); ?></label>
								<input type="checkbox" id="rule_<?php echo $i; ?>_colorbox_external_images" name="rule_<?php echo $i; ?>_colorbox_external_images" <?php echo $form[$i]['options']['colorbox_external_images']; ?> />

							</div>
							<div id="rule_<?php echo $i; ?>_external_links_div" class="rule">
								<label class="line2" for="rule_<?php echo $i; ?>_external_links"><?php _e( 'Retain links to external sites', 'seo-friendly-images' ); ?></label>
								<input type="checkbox" id="rule_<?php echo $i; ?>_external_links" name="rule_<?php echo $i; ?>_external_links" <?php echo ( isset( $form[$i]['options']['external_links'] ) ? $form[$i]['options']['external_links'] : '' ); ?> />

							</div>
						</div>
					</div>
				</div>
				</div>
					<?php endfor; ?>
					<div id="rule_buttons">
						<input type="button" id="add_rule" name="add_rule" value="<?php _e( 'Add Rule', 'seo-friendly-images' ); ?>" />
						<input type="button" id="remove_rule" name="remove_rule" value="<?php _e( 'Remove Rule', 'seo-friendly-images' ); ?>" />
					</div>
					<div style="padding: 1.5em 0;margin: 5px 0;">
						<input type="submit" name="Submit" value="<?php _e( 'Update options', 'seo-friendly-images' ); ?>" />
					</div>
				</form>
				<div id="rule_copy" style="display:none;"><?php echo $this->create_rule_html( 'number' ); ?></div>
			</div> 
		</div>
		<h5><?php _e( 'Another fine WordPress plugin by', 'seo-friendly-images' ); ?> <a href="http://www.prelovac.com/vladimir/">Vladimir Prelovac</a></h5>
	</div>
	<?php	
	}
	
	function handle_colorbox_options() {
		if ( isset( $_POST['submitted'] ) )  {
			$this->colorbox['theme'] = ( ! isset( $_POST['colorbox_theme'] ) ? '1' : $_POST['colorbox_theme'] );
			
			$options['rules'] = $this->rules;
			$options['global'] = $this->global;
			$options['colorbox'] = $this->colorbox;
			
			update_option( $this->key, $options );
			
			$msg_status = __( 'SEO Friendly Images colorbox options saved.', 'seo-friendly-images' );
			
			// Show message
			echo '<div id="message" class="updated fade"><p>' . $msg_status . '</p></div>';
		}
		
		$imgpath = $this->plugin_url . '/i';
		$actionurl = $_SERVER['REQUEST_URI'];
		
	?>
	<div class="wrap">
		<?php screen_icon(); ?>
		<h2><?php _e( 'SEO Friendly Images Business', 'seo-friendly-images' ); echo '&nbsp;' . $this->local_version; ?></h2>
		<a href="admin.php?page=sfi_settings"><?php _e( 'Settings', 'seo-friendly-images' ); ?></a> &nbsp;|&nbsp; <a href="admin.php?page=sfi_colorbox_options"><?php _e( 'Colorbox Options', 'seo-friendly-images' ); ?></a> &nbsp;|&nbsp; <a href="admin.php?page=sfi_about"><?php _e( 'About', 'seo-friendly-images' ); ?></a>
		<div id="poststuff" style="margin-top:10px;">
		
			<div id="sideblock" style="float:right;width:270px;margin-left:10px;">		  
				<iframe width=270 height=800 frameborder="0" src="http://www.prelovac.com/plugin/news.php?id=2&utm_source=plugin&utm_medium=plugin&utm_campaign=SEO%2BFriendly%2BImages%2BPRO"></iframe>
			</div>
		</div>
		<div id="mainblock" class="submit">
			<div class="dbx-content">
				<h2><?php _e( 'Colorbox Options', 'seo-friendly-images' ); ?></h2>
				<br />
				<form name="sfiform" action="<?php echo $actionurl; ?>" method="post">
					<input type="hidden" name="submitted" value="1" />
					<div>
						<label for="colorbox_theme"><?php _e( 'Choose ColorBox theme:', 'seo-friendly-images' ); ?></label>
						<select id="colorbox_theme" name="colorbox_theme">
							<?php for( $i = 1; $i <= 11; $i++ ): ?>
								<option value="<?php echo $i; ?>" <?php echo ( $this->colorbox['theme']==$i ) ? 'selected="yes"' : ''; ?>><?php echo __( 'Theme ', 'seo-friendly-images' ) . $i; ?></option>
							<?php endfor; ?>
						</select>
					</div>
					<div>
						  
						<br />
						<div id="screenshot_image">
							<img src="<?php echo $this->plugin_url . '/screenshots/screenshot-' . $this->colorbox['theme'] . '.jpg'; ?>" />
						</div>
					</div>
					<div style="padding: 1.5em 0;margin: 5px 0;">
						<input type="submit" name="Submit" value="<?php _e( 'Update options', 'seo-friendly-images' ); ?>" />
					</div>
				</form>
			</div> 
		</div>
		<h5><?php _e( 'Another fine WordPress plugin by', 'seo-friendly-images' ); ?> <a href="http://www.prelovac.com/vladimir/">Vladimir Prelovac</a></h5>
	</div>
	<?php		
	}
	
	function handle_about() {
		global $wp_version;
		
		$upd_msg = "";
		
		$actionurl = $_SERVER['REQUEST_URI'];
		$nonce = wp_create_nonce( 'seo-smart-links' );
		
		$imgpath = $this->plugin_url . '/i';
		$lic_msg = '<p>Welcome to ' . $this->name . '.</p>';
	?>
	<div class="wrap">
		<?php screen_icon(); ?>
		<h2><?php _e( 'SEO Friendly Images Business', 'seo-friendly-images' ); echo '&nbsp;' . $this->local_version; ?></h2>
		<a href="admin.php?page=sfi_settings"><?php _e( 'Settings', 'seo-friendly-images' ); ?></a> &nbsp;|&nbsp; <a href="admin.php?page=sfi_colorbox_options"><?php _e( 'Colorbox Options', 'seo-friendly-images' ); ?></a> &nbsp;|&nbsp; <a href="admin.php?page=sfi_about"><?php _e( 'About', 'seo-friendly-images' ); ?></a>
		<div id="poststuff" style="margin-top:10px;">
		
			<div id="sideblock" style="float:right;width:270px;margin-left:10px;">		  
				<iframe width=270 height=800 frameborder="0" src="http://www.prelovac.com/plugin/news.php?id=2&utm_source=plugin&utm_medium=plugin&utm_campaign=SEO%2BFriendly%2BImages%2BPRO"></iframe>
			</div>
		</div>
		<div id="mainblock" class="submit">
			<div class="dbx-content">				
				<h2><?php _e( 'About', 'seo-friendly-images' ); ?></h2>
				<br />
				<form name="SEOLinks_about" action="$actionurl" method="post">
					<input type="hidden" id="_wpnonce" name="_wpnonce" value="$nonce" />
					<input type="hidden" name="submitted" value="1" /> 			
					<?php echo $lic_msg; ?>
					<?php echo __( 'Version:', 'seo-friendly-images' ) . $this->local_version; ?> <?php echo $upd_msg; ?>
				</form>
			</div>   
		</div>
		<h5><?php _e( 'Another fine WordPress plugin by', 'seo-friendly-images' ); ?> <a href="http://www.prelovac.com/vladimir/">Vladimir Prelovac</a></h5>
	</div>
	<?php
	}
	
	function remove_extension( $name ) {
		return preg_replace( '/(.+)\..*$/', '$1', $name );
	}
	
	function seo_friendly_images_process( $matches ) {
		global $post;
		
		$alttext_rep = $this->process_parameters["alt"];
		$titletext_rep = $this->process_parameters["title"];
		$override_alt = $this->process_parameters["override_alt"];
		$override_title = $this->process_parameters["override_title"];
		$strip_extension_title = $this->process_parameters["strip_extension_title"];
		
		$title = $post->post_title;
		
		# take care of unusual endings
		$matches[0] = preg_replace( '|([\'"])[/ ]*$|', '\1 /', $matches[0] );
		
		### Normalize spacing around attributes.
		$matches[0] = preg_replace( '/\s*=\s*/', '=', substr( $matches[0], 0, strlen( $matches[0] ) - 2 ) );
		### Get source.
		
		preg_match( '/src\s*=\s*([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/', $matches[0], $source );
		
		$saved = $source[2];
		
		### Swap with file's base name.
		preg_match( '%[^/]+(?=\.[a-z]{3}(\z|(?=\?)))%', $source[2], $source );
		### Separate URL by attributes.
		$pieces = preg_split( '/(\w+=)/', $matches[0], -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY );
		### Add missing pieces.
		
		$tags = "";
		if ( strrpos( $alttext_rep, "%tags" ) !== false || strrpos( $titletext_rep, "%tags" ) !== false ) {
			$posttags = get_the_tags();		
			
			if ( $posttags ) {
				$i = 0;
				foreach ( $posttags as $tag ) {
					if ( $i == 0 ) {
						$tags = $tag->name . $tags;
					} else {
						$tags = $tag->name . ' ' . $tags;
					}
					++$i;
				}
			}
		}
		
		$cats = "";
		if ( strrpos( $alttext_rep, "%category" ) !== false || strrpos( $titletext_rep, "%category" ) !== false ) {
			$categories = get_the_category();
			
			if ( $categories ) {
				$i = 0;
				foreach ( $categories as $cat ) {
					if ( $i == 0 ) {
						$cats = $cat->slug . $cats;
					} else {
						$cats = $cat->slug . ' ' . $cats;
					}
					++$i;
				}
			}
		}
		
		if ( $override_title == "on" ) {
			$titletext_rep = str_replace("%title", $post->post_title, $titletext_rep );
			$titletext_rep = str_replace("%name", $source[0], $titletext_rep );
			$titletext_rep = str_replace("%category", $cats, $titletext_rep );
			$titletext_rep = str_replace("%tags", $tags, $titletext_rep );
			
			if ( $strip_extension_title == "on" ) {
				$titletext_rep = str_replace( '"', '', $titletext_rep );
				$titletext_rep = str_replace("'", "", $titletext_rep );			
				$titletext_rep = str_replace("_", " ", $titletext_rep );
				$titletext_rep = str_replace("-", " ", $titletext_rep );
			}
			
			//$titletext_rep = ucwords( strtolower( $titletext_rep ) );
			if ( ! in_array( 'title=', $pieces ) ) {
				array_push( $pieces, ' title="' . $titletext_rep . '"' );
			} else {
				$index			  = array_search( 'title=', $pieces );
				$pieces[$index + 1] = '"' . $titletext_rep . '" ';
			}
		}
		if ( $override_alt == "on" ) {
			$alttext_rep = str_replace("%title", $post->post_title, $alttext_rep );
			$alttext_rep = str_replace("%name", $source[0], $alttext_rep );
			$alttext_rep = str_replace("%category", $cats, $alttext_rep );
			$alttext_rep = str_replace("%tags", $tags, $alttext_rep );
			$alttext_rep = str_replace("\"", "", $alttext_rep );
			$alttext_rep = str_replace("'", "", $alttext_rep );
			
			$alttext_rep = ( str_replace("-", " ", $alttext_rep ) );
			$alttext_rep = ( str_replace("_", " ", $alttext_rep ) );
			
			if ( ! in_array( 'alt=', $pieces ) ) {
				array_push( $pieces, ' alt="' . $alttext_rep . '"' );
			} else {
				$index			  = array_search( 'alt=', $pieces );
				$pieces[$index + 1] = '"' . $alttext_rep . '" ';
			}
		}
		
		return implode( '', $pieces ) . ' /';
	}   
	
	function get_proper_options() {
		$options = null;
		
		if ( is_home() ) {
			if ( $this->tree["main"]["home"]["options"] != null ) {
				$options = $this->tree["main"]["home"]["options"];
			} elseif ( $this->tree["main"]["options"] != null ) {
				$options = $this->tree["main"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_front_page() ) {
			if ( $this->tree["main"]["front"]["options"] != null ) {
				$options = $this->tree["main"]["front"]["options"];
			} elseif ( $this->tree["main"]["options"] != null ) {
				$options = $this->tree["main"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_category() ) {
			$cur_category_id = get_cat_id( single_cat_title("",false ) );
			$found = false;
			$found_group = null;
			foreach ( $this->tree["archive"]["category"] as $key => $group ) {
				if ( $key != 'options' ) {
					$found = $found || in_array( $cur_category_id, $group["ids"] );
					if ( in_array( $cur_category_id, $group["ids"] ) ) {
						$found_group = $key;
					}
				}
			}
			if ( $found ) {
				$options = $this->tree["archive"]["category"][$found_group]["options"];
			} elseif ( $this->tree["archive"]["category"]["options"] != null ) {
				$options = $this->tree["archive"]["category"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_tag() ) {
			$cur_tag_title = single_tag_title("",false );
			$tag = get_term_by( 'name', $cur_tag_title, 'post_tag' );
			if ( $tag ) {
				$cur_tag_id = $tag->term_id;
			} else {
				$cur_tag_id = 0;
			}
			$found = false;
			$found_group = null;
			foreach ( $this->tree["archive"]["tag"] as $key => $group ) {
				if ( $key != 'options' ) {
					$found = $found || in_array( $cur_tag_id, $group["ids"] );
					if ( in_array( $cur_tag_id, $group["ids"] ) ) {
						$found_group = $key;
					}
				}
			}
			if ( $found ) {
				$options = $this->tree["archive"]["tag"][$found_group]["options"];
			} elseif ( $this->tree["archive"]["tag"]["options"] != null ) {
				$options = $this->tree["archive"]["tag"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_tax() ) {
			$term = get_queried_object();
			$cur_taxonomy_id = $term->term_id;
			$found = false;
			$found_group = null;
			foreach ( $this->tree["archive"]["taxonomy"] as $key => $group ) {
				if ( $key != 'options' ) {
					$found = $found || in_array( $cur_taxonomy_id, $group["ids"] );
					if ( in_array( $cur_taxonomy_id, $group["ids"] ) ) {
						$found_group = $key;
					}
				}
			}
			if ( $found ) {
				$options = $this->tree["archive"]["taxonomy"][$found_group]["options"];
			} elseif ( $this->tree["archive"]["taxonomy"]["options"] != null ) {
				$options = $this->tree["archive"]["taxonomy"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_author() ) {
			$term = get_queried_object();
			$cur_author_id = $term->ID;
			$found = false;
			$found_group = null;
			foreach ( $this->tree["archive"]["author"] as $key => $group ) {
				if ( $key != 'options' ) {
					$found = $found || in_array( $cur_author_id, $group["ids"] );
					if ( in_array( $cur_author_id, $group["ids"] ) ) {
						$found_group = $key;
					}
				}
			}
			if ( $found ) {
				$options = $this->tree["archive"]["author"][$found_group]["options"];
			} elseif ( $this->tree["archive"]["author"]["options"] != null ) {
				$options = $this->tree["archive"]["author"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_year() ) {
			if ( $this->tree["archive"]["date"]["year"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["year"]["options"];
			} elseif ( $this->tree["archive"]["date"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_month() ) {
			if ( $this->tree["archive"]["date"]["month"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["month"]["options"];
			} elseif ( $this->tree["archive"]["date"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_day() ) {
			if ( $this->tree["archive"]["date"]["day"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["day"]["options"];
			} elseif ( $this->tree["archive"]["date"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_time() ) {
			if ( $this->tree["archive"]["date"]["time"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["time"]["options"];
			} elseif ( $this->tree["archive"]["date"]["options"] != null ) {
				$options = $this->tree["archive"]["date"]["options"];
			} elseif ( $this->tree["archive"]["options"] != null ) {
				$options = $this->tree["archive"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_attachment() ) {
			global $post;
			$cur_attachment_id = $post->ID;
			$found = false;
			$found_group = null;
			foreach ( $this->tree["singular"]["attachment"] as $key => $group ) {
				if ( $key != 'options' ) {
					$found = $found || in_array( $cur_attachment_id, $group["ids"] );
					if ( in_array( $cur_attachment_id, $group["ids"] ) ) {
						$found_group = $key;
					}
				}
			}
			if ( $found ) {
				$options = $this->tree["singular"]["attachment"][$found_group]["options"];
			} elseif ( $this->tree["singular"]["attachment"]["options"] != null ) {
				$options = $this->tree["singular"]["attachment"]["options"];
			} elseif ( $this->tree["singular"]["options"] != null ) {
				$options = $this->tree["singular"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		} elseif ( is_page() ) {
			global $post;
			$cur_page_id = $post->ID;
			$found = false;
			$found_group = null;
			foreach ( $this->tree["singular"]["page"] as $key => $group ) {
				if ( $key != 'options' ) {
					$found = $found || in_array( $cur_page_id, $group["ids"] );
					if ( in_array( $cur_page_id, $group["ids"] ) ) {
						$found_group = $key;
					}
				}
			}
			if ( $found ) {
				$options = $this->tree["singular"]["page"][$found_group]["options"];
			} elseif ( $this->tree["singular"]["page"]["options"] != null ) {
				$options = $this->tree["singular"]["page"]["options"];
			} elseif ( $this->tree["singular"]["options"] != null ) {
				$options = $this->tree["singular"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		}
		elseif ( is_single() ) {
			global $post;
			$cur_post_id = $post->ID;
			$found = false;
			$found_group = null;
			foreach ( $this->tree["singular"]["post"] as $key => $group ) {
				if ( $key != 'options' ) {
					$found = $found || in_array( $cur_post_id, $group["ids"] );
					if ( in_array( $cur_post_id, $group["ids"] ) ) {
						$found_group = $key;
					}
				}
			}
			if ( $found ) {
				$options = $this->tree["singular"]["post"][$found_group]["options"];
			} elseif ( $this->tree["singular"]["post"]["options"] != null ) {
				$options = $this->tree["singular"]["post"]["options"];
			} elseif ( $this->tree["singular"]["options"] != null ) {
				$options = $this->tree["singular"]["options"];
			} else {
				$options = $this->tree["all"]["options"];
			}
		}
		
		return $options;
	}
	
	function seo_friendly_images( $content ) {
		$options = $this->get_proper_options();
		
		if ( $options["enable"] == 'on' ) {
			$this->process_parameters['alt'] = $options['alt'];
			$this->process_parameters['title'] = $options['title'];
			$this->process_parameters['override_alt'] = $options['override_alt'];
			$this->process_parameters['override_title'] = $options['override_title'];
			$this->process_parameters['strip_extension_title'] = $options['strip_extension_title'];
			$replaced = preg_replace_callback( '/<img[^>]+/', array( $this, 'seo_friendly_images_process' ), $content, $options['override_max'] );
			if ( ( $options['attach_internal_images'] != 'def' ) || ( $options['attach_external_images'] != 'def' ) ) {
				return $this->replace_image_with_attachment( $replaced, $options['attach_internal_images'], $options['colorbox_internal_images'], $options['attach_external_images'], $options['colorbox_external_images'], $options['external_links'] );
			}
			return $replaced;
		}
		
		return $content;
	}
	
	function seo_friendly_images_featured( $html ) {
		$options = $this->get_proper_options();
		
		if ( $options["enable"] == 'on' ) {
			$this->process_parameters['alt'] = $options['alt'];
			$this->process_parameters['title'] = $options['title'];
			$this->process_parameters['override_alt'] = $options['override_alt'];
			$this->process_parameters['override_title'] = $options['override_title'];
			$this->process_parameters['strip_extension_title'] = $options['strip_extension_title'];
			$replaced = preg_replace_callback( '/<img[^>]+/', array( $this, 'seo_friendly_images_process' ), $html );
			return $replaced;
		}
		
		return $html;
	}
	
	//filter handler
	function replace_image_with_attachment( $content, $attach, $colorbox, $attach_ext, $colorbox_ext, $external ) {
		global $post;
		
		//get all images uploaded as "attachments" on this post
		$allimages =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
		
		//regex - to get all images from posts like <a><img/></a> OR <img/>
		preg_match_all( '/(<a.*rel="([^"]+)".*href="([^"]+)"[^>]*>)?\s*<img.*src="([^"]+)"[^>]*>/i', $content, $matches1, PREG_PATTERN_ORDER );
		preg_match_all( '/(<a.*href="([^"]+)".*rel="([^"]+)"[^>]*>)?\s*<img.*src="([^"]+)"[^>]*>/i', $content, $matches2, PREG_PATTERN_ORDER );
		
		$tmp = $matches2[2];
		$matches2[2] = $matches2[3];
		$matches2[3] = $tmp;
		
		$matches = array_merge( $matches1, $matches2 );
		
		$links = array_map( array( $this, 'fix_img_url' ), $matches[4] );
		
		$index = 0;
		$post_ids = array();
		$last_id = 0;
		foreach ( $links as $link )  {
			$isthere = false;
			$img = null;
			
			foreach ( $allimages as $image ) {
				$isthere = $isthere || ( $image->guid == $link );
				
				if ( $isthere ) {
					$img = $image;
					break;
				}
			}
			
			if ( ( $external == 'off' ) && ( $link != $matches[3][$index] ) ) {
				$content = str_replace( 'href="' . $matches[3][$index] . '"', 'href="' . $link . '"', $content );
				$content = str_replace( 'href=\'' . $matches[3][$index] . '\'', 'href="' . $link . '"', $content );
			}
			
			$cbox = 'rel="cbox_' . get_the_ID() . '" ';
			
			$current_id = get_the_ID();
			
			if ( $last_id != $current_id ) {
				array_push( $post_ids, $current_id );
			}
			
			$last_id = $current_id;
			
			if ( $isthere ) {
				if ( $attach != 'def' ) {
					$img_src = ( $attach == 'post' ) ? get_permalink( $post->ID ) : ( $attach == 'att' ? get_attachment_link( $img->ID ) : $img->guid );				
					if ( $matches[1][$index] ) {
						$content = str_replace( 'href="' . $img->guid . '"', 'href="' . $img_src . '"', $content );
						$content = str_replace( 'href=\'' . $img->guid . '\'', 'href="' . $img_src . '"', $content );
						$content = str_replace( 'href="' . get_attachment_link( $img->ID ) . '"', 'href="' . $img_src . '"', $content );
						$content = str_replace( 'href=\'' . get_attachment_link( $img->ID ) . '\'', 'href="' . $img_src . '"', $content );
					} else {
						$content = str_replace( $matches[0][$index], '<a ' . ( ( $attach == 'img' && $colorbox == 'on' ) ? $cbox : '' ) . 'href="' . $img_src . '">' . $matches[0][$index] . '</a>', $content );						
					}
				}
			} else {
				if ( $attach_ext != 'def' ) {
					$img_src = ( $attach_ext == 'post' ) ? get_permalink( $post->ID ) : $link;
					if ( $matches[1][$index] )  {
						$content = str_replace( 'href="' . $matches[3][$index] . '"', ( ( $attach_ext == 'img' && $colorbox_ext == 'on' ) ? $cbox : '' ) . 'href="' . $img_src . '"', $content );
						$content = str_replace( 'href=\'' . $matches[3][$index] . '\'', ( ( $attach_ext == 'img' && $colorbox_ext == 'on' ) ? $cbox : '' ) . 'href="' . $img_src . '"', $content );
					} else {
						$content = str_replace( $matches[0][$index], '<a ' . ( ( $attach_ext == 'img' && $colorbox_ext == 'on' ) ? $cbox : '' ) . 'href="' . $img_src . '">' . $matches[0][$index] . '</a>', $content );
					}
				}
			}
			
			if ( $matches[2][$index] ) {
				$content = str_replace( 'rel="' . $matches[2][$index] . '"', ( ( $attach == 'img' && $colorbox == 'on' ) ? $cbox : 'rel="' . $matches[2][$index] . '"' ), $content );
				$content = str_replace( 'rel=\'' . $matches[2][$index] . '\'', ( ( $attach == 'img' && $colorbox == 'on' ) ? $cbox : 'rel=\'' . $matches[2][$index] . '\'' ), $content );
			}
			
			$index++;
		}
		
		if ( count( $post_ids ) > 0 ) {
			$content .= '<script>
			jQuery( document ).ready( function( $){';
			
			foreach ( $post_ids as $post_id ) {
				$content .= '$("a[rel=\'cbox_' . $post_id . '\']" ).colorbox()';
			}
			
			$content .= '});
			</script>';
		}
		
		return $content;
	}
	
	//this function removes 640x480 like dimension information from image URLs which is added by wordpress when generating multiple images for the uploaded one 
	function fix_img_url( $url ) {
		$url = preg_replace( '/-([0-9]{1,5})x([0-9]{1,5})\./i', '.', $url );
		return $url;
	}
	
	function no_frames() {	
		if ( is_admin() || current_user_can( 'level_10' ) )
			return; 
			
		$options = $this->get_options();
		
		if ($options['global']['no_frames'] == 'off')
			return;
			   
?>
		<script type="text/javascript">
		<!--
			if (top.location != self.location) {
				top.location = self.location.href
			}
		//-->
		</script>
<?php
	}
	
	
	// Credits to http://www.labnol.org/internet/google-image-sitemap-for-wordpress/14125/
 
	function is_sitemap_writable( $filename ) {
		if( ! is_writable( $filename ) ) {
			if( ! @chmod( $filename, 0666 ) ) {
				$pathtofilename = dirname( $filename );
				if( ! is_writable( $pathtofilename ) ) {
					if( ! @chmod( $pathtoffilename, 0666 ) ) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/*
	 * @author Arne Brachhold
	 * @return string The full path to the blog directory
	*/
	function GetHomePath() {
		
		$res="";
		//Check if we are in the admin area -> get_home_path() is avaiable
		if(function_exists("get_home_path")) {
			$res = get_home_path();
		} else {
			//get_home_path() is not available, but we can't include the admin
			//libraries because many plugins check for the "check_admin_referer"
			//function to detect if you are on an admin page. So we have to copy
			//the get_home_path function in our own...
			$home = get_option( 'home' );
			if ( $home != '' && $home != get_option( 'url' ) ) {
				$home_path = parse_url( $home );
				$home_path = $home_path['path'];
				$root = str_replace( $_SERVER["PHP_SELF"], '', $_SERVER["SCRIPT_FILENAME"] );
				$home_path = trailingslashit( $root.$home_path );
			} else {
				$home_path = ABSPATH;
			}

			$res = $home_path;
		}
		return $res;
	}
	
	function create_xml_sitemap() {
		global $wpdb;
		$limit=	$this->global['xml_sitemap_num'];
		
		if ($limit=='' ||	$limit<0)
			$limit=1000;
		
		$posts = $wpdb->get_results ("SELECT ID, post_content FROM $wpdb->posts 
			WHERE post_status = 'publish'
			AND ( post_type = 'post' OR post_type = 'page' )
			ORDER BY post_date LIMIT $limit" );
		
		if ( empty ( $posts ) ) {
			return false;
		} else {
			$xml   = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";	
			$xml .= '<!-- Generated-on="' . date("F j, Y, g:i a" ) .'" -->' . "\n";			   
			$xml  .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">' . "\n";
			
			foreach ( $posts as $post ) { 
			 
				$content = $post->post_content;
				//$content = apply_filters('the_content', $post->post_content);
				$has_featured = false;
				if ( has_post_thumbnail( $post->ID ) ) {
					$has_featured = true;
					
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) );
					
				}
				
				if ( preg_match_all ("/[\'\"](http:\/\/.[^\'\"]+\.(?:jpe?g|png|gif))[\'\"]/ui", 
					$content , $matches, PREG_SET_ORDER ) || $has_featured) {
	
					$permalink = get_permalink( $post->ID ); 
					$xml .= "<url>\n";
					$xml .= " <loc>$permalink</loc>\n";
	
					foreach ( $matches as $match ) {
						$xml .= " <image:image>\n";
						$xml .= " <image:loc>$match[1]</image:loc>\n";
						$xml .= " </image:image>\n";								   
					}
					
					if ( $has_featured ) {
						$xml .= " <image:image>\n";
						$xml .= " <image:loc>$image[0]</image:loc>\n";
						$xml .= " </image:image>\n";
					}
					$xml .= "</url>\n";
				}
			}
			$xml .= "\n</urlset>";
		}
		
		/*$site_url = get_bloginfo( 'url' );
		preg_match( '/http(s)?:\/\/([^\/]*)\/([^\/]*)/' , $site_url, $matches );
		$relative_path = $matches[3];
		$image_sitemap_url = $_SERVER["DOCUMENT_ROOT"] . "/" . $relative_path . '/sitemap-image.xml';*/
		$image_sitemap_url=$this->GetHomePath().'sitemap-image.xml';		
		
		if ( $this->is_sitemap_writable( $image_sitemap_url ) ) {				
			
			if ( file_put_contents( $image_sitemap_url, $xml ) ) {
				return true;
			}
		}
		return false;
	}
	
	function delete_xml_sitemap() {
		$site_url = get_bloginfo( 'url' );
		preg_match( '/http(s)?:\/\/([^\/]*)\/([^\/]*)/' , $site_url, $matches );
		$relative_path = $matches[3];
		$image_sitemap_url = $_SERVER["DOCUMENT_ROOT"] . "/" . $relative_path . '/sitemap-image.xml';
		if ( $this->is_sitemap_writable( $_SERVER["DOCUMENT_ROOT"] . "/" . $relative_path ) || $this->is_sitemap_writable( $image_sitemap_url ) ) {
			if ( unlink( $image_sitemap_url ) ) {
				return true;
			}
		} elseif ( ! file_exists( $image_sitemap_url ) ) {
			return true;
		}
		return false;
	}
	
	function build_tree() {
		$this->tree["all"]["options"] = null;
		$this->tree["main"]["options"] = null;
		$this->tree["main"]["home"]["options"] = null;
		$this->tree["main"]["front"]["options"] = null;
		$this->tree["archive"]["options"] = null;
		$this->tree["archive"]["category"]["options"] = null;
		$this->tree["archive"]["tag"]["options"] = null;
		$this->tree["archive"]["taxonomy"]["options"] = null;
		$this->tree["archive"]["author"]["options"] = null;
		$this->tree["archive"]["date"]["options"] = null;
		$this->tree["archive"]["date"]["year"]["options"] = null;
		$this->tree["archive"]["date"]["month"]["options"] = null;
		$this->tree["archive"]["date"]["day"]["options"] = null;
		$this->tree["archive"]["date"]["time"]["options"] = null;
		$this->tree["archive"]["search"]["options"] = null;
		$this->tree["singular"]["options"] = null;
		$this->tree["singular"]["post"]["options"] = null;
		$this->tree["singular"]["page"]["options"] = null;
		$this->tree["singular"]["attachment"]["options"] = null;			 
		if (isset($this->rules))
		foreach( $this->rules as $rule ) {
			if ( isset( $rule["domains"] ) && is_array( $rule["domains"] ) )
				foreach( $rule["domains"] as $key => $domain ) {
					if( ! is_int( $key ) ) {
						switch( $key ) {
						case "category":
							$num = count( $this->tree["archive"]["category"] );
							$this->tree["archive"]["category"]["group_" . $num]["ids"] = $domain;
							$this->tree["archive"]["category"]["group_" . $num]["options"] = $rule["options"];
							break;
						case "tag":
							$num = count( $this->tree["archive"]["tag"] );
							$this->tree["archive"]["tag"]["group_" . $num]["ids"] = $domain;
							$this->tree["archive"]["tag"]["group_" . $num]["options"] = $rule["options"];
							break;
						case "taxonomy":
							$num = count( $this->tree["archive"]["taxonomy"] );
							$this->tree["archive"]["taxonomy"]["group_" . $num]["ids"] = $domain;
							$this->tree["archive"]["taxonomy"]["group_" . $num]["options"] = $rule["options"];
							break;
						case "author":
							$num = count( $this->tree["archive"]["author"] );
							$this->tree["archive"]["author"]["group_" . $num]["ids"] = $domain;
							$this->tree["archive"]["author"]["group_" . $num]["options"] = $rule["options"];
	
							break;
						case "post":
							$num = count( $this->tree["singular"]["post"] );
							$this->tree["singular"]["post"]["group_" . $num]["ids"] = $domain;
							$this->tree["singular"]["post"]["group_" . $num]["options"] = $rule["options"];
							break;
						case "page":
							$num = count( $this->tree["singular"]["page"] );
							$this->tree["singular"]["page"]["group_" . $num]["ids"] = $domain;
							$this->tree["singular"]["page"]["group_" . $num]["options"] = $rule["options"];
							break;
						case "attachment":
							$num = count( $this->tree["singular"]["attachment"] );
							$this->tree["singular"]["attachment"]["group_" . $num]["ids"] = $domain;
							$this->tree["singular"]["attachment"]["group_" . $num]["options"] = $rule["options"];
							break;
						}
					} else {
						switch( $domain )
						{
						case "all":
						case "main":
						case "archive":
						case "singular":
							if( $this->tree[$domain]["options"] == null )
							{
								$this->tree[$domain]["options"] = $rule["options"];
							}
							break;
						case "home":
						case "front":
							if( $this->tree["main"][$domain]["options"] == null )
							{
								$this->tree["main"][$domain]["options"] = $rule["options"];
							}
							break;
						case "category":
						case "tag":
						case "taxonomy":
						case "author":
						case "date":
						case "search":
							if( $this->tree["archive"][$domain]["options"] == null )
							{
								$this->tree["archive"][$domain]["options"] = $rule["options"];
							}
							break;
						case "year":
						case "month":
						case "day":
						case "time":
							if( $this->tree["archive"]["date"][$domain]["options"] == null )
							{
								$this->tree["archive"]["date"][$domain]["options"] = $rule["options"];
							}
							break;
						case "post":
						case "page":
						case "attachment":
							if( $this->tree["singular"][$domain]["options"] == null ) {
								$this->tree["singular"][$domain]["options"] = $rule["options"];
							}
							break;
						}
					}
				}
		}
	}
	
	function get_options() {
		$options = array(
			'global' => array(
				'no_frames' => 'off',
				'xml_sitemap' => 'off',
				'xml_sitemap_num' => 1000
			),
			'rules' => array(
				0 => array(
					'domains' => array(
						'all'
					),
					'options' => array(
						'alt' => '%name %title',
						'title' => '%name photo',
						'override_alt' => 'on',
						'override_title' => 'off',
						'strip_extension_title' => 'on',
						'override_max' => 2,
						'attach_internal_images' => 'def',
						'colorbox_internal_images' => 'off',
						'attach_external_images' => 'def',
						'colorbox_external_images' => 'off',
						'external_links' => 'on',
						'enable' => true
					)
				)
			),
			'colorbox' => array(
				'theme' => '1'
			)
		);
		
		$saved = get_option( $this->key );
		
		if (!empty($saved)) {
			foreach ($options['global'] as $key => $option)
				if (!isset($saved['global'][$key]))
					$saved['global'][$key] = $option;
		}
		                        			
		if ( $saved != $options ) {
			update_option( $this->key, $saved );
		}
		
		return $saved;
	}
}
?>