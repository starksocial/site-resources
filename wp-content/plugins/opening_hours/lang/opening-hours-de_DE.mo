��            )   �      �     �     �     �     �     �     �     �  '     H   9  e   �     �     �  
             +  '   C  %   k  �   �             �   2     �     �     �               %     <     ?  �  T     5     A     ^     j     �     �  *   �  1   �  D   	  r   X	     �	      �	     �	     
  &   $
  4   K
  )   �
  �   �
     Z     p  �   ~      E     f     m  	   }      �     �     �     �                     
                                                                        	                                            Add Close business manually Closed Closed Image Configuration saved! Configure Opening Hours Custom Closed Text Custom closed text for the main widget. Displays "closed image" manually (independent from your business hours). Displays your configured business hours. (Widget can be configured under "Plugins" > "Opening Hours") Main Settings Oops, something went wrong. Open Image Opening Hours Please add a Time Frame Please configure the plugin appearance. Please configure your business hours. Please specify the images which will be used in the “We´re Open” Widget. Leave blank if you want to use the pre-made images. Save Changes Set Widget Images Shows an configured or predefined image based on if your business is currently either open or closed. (Images can be set under "Plugins" > "Opening Hours") The divider between two times Title: URL of the image. Upload Will be shown if closed. Will be shown if open. to “Time”-separator Project-Id-Version: Opening Hours
POT-Creation-Date: 2013-02-11 02:14+0100
PO-Revision-Date: 2013-02-11 02:20+0100
Last-Translator: 
Language-Team: Harbor <support@hrbor.com>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;_e;__
X-Poedit-Basepath: /Users/buzzrocket/opening hours/out/opening_hours
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Hinzufügen Geschäft manuell schließen Geschlossen “Geschlossen”-Bild Konfiguration gespeichert. Öffnungszeiten verwalten Benutzerdefinierter “Geschlossen”-Text “Geschlossen”-Text für das “Main”-Widget Zeigt manuell "Geschlossen"-Bild an (unabhängig v. Öffnungszeiten) Zeigt Ihre konfigurierten Geschäftszeiten an. (Widget kann unter "Plugins" > "Opening Hours" konfiguriert werden) Haupteinstellungen Oops, etwas ist schief gelaufen. “Geöffnet”-Bild Öffnungszeiten Bitte fügen Sie ein Zeitfenster hinzu Bitte nehmen Sie alle notwendigen Einstellungen vor. Bitte geben Sie Ihre Öffnungszeiten ein. Bitte geben Sie die Bilder an, welche im “We´re Open”-Widget verwendet werden sollen. Sie können die Felder leer lassen, wenn Sie die Standart-Bilder verwenden möchten. Änderungen speichern Widget Bilder Zeigt ein konfiguriertes oder vordefiniertes Bild an, basierend darauf ob Ihr Geschäft derzeit geschlossen oder geöffnet ist. (Bilder können eingestellt werden unter  "Plugins" > "Opening Hours") Der Trennet zwischen zwei Zeiten Titel: URL des Bildes. Hochladen Wird angezeigt wenn geschlossen. Wird angezeigt wenn offen. bis “Zeit”-Separator 