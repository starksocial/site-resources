jQuery(document).ready(function($) {

	//make entries sortable
	 $( ".oh_row ul" ).sortable({
	 
	 items: ".oh_entry",
	 cursor: 'pointer'
	 
	 });

	//activate tab function for the plugin page (jquery UI)
	$( "#oh_wrap" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );

	//assign a time picker to each input within an entry and make this input only accessible via time picker whenever "load_timepicker" is triggered.
	$('.oh_entry input').live('load_timepicker', function() {
	
		$(this).timepicker().attr('readonly', true);
		
	});
	
	//trigger the load_timepicker event
	$('.oh_entry input').trigger("load_timepicker");
    
    //Adds new time window to the database
    $('.oh_add').live("click", function() {
    
    	//store clicked trigger => oh_add
    	var clicked_trigger = $(this);
    	
    	//get current day of the week
    	var day_of_week = $(this).parent('ul').attr('id');
    
    	//send data to the processing file
    	$.post(
   			ajaxurl, 
   			{
     			'action': 'oh_add',
      			'oh_day': day_of_week
   			}, 
   			function(response){ 

				//check if .oh_closed exists
				if($(clicked_trigger).parent().children('.oh_closed').length) {
				
					//hide .oh_closed for current day of the week
					$(clicked_trigger).parent().children('.oh_closed').fadeOut('fast', function() {
					
						//add the new entry (generated from the processor) to the row
						$(clicked_trigger).before(response);
						//trigger load_timepicker to make the new entry accessible
						$('.oh_entry input').trigger("load_timepicker");
					
					});
				
				} else {
					
					//add the new entry (generated from the processor) to the row				
					$(clicked_trigger).before(response);
					//trigger load_timepicker to make the new entry accessible
					$('.oh_entry input').trigger("load_timepicker");
					
				}
				
				

   			}
		);
    
    });
    
	//Deletes parent time window from database
	$('.oh_delete').live("click", function() {
	
		//store clicked trigger => oh_delete
	   	var clicked_trigger = $(this);
	   	//get the id of the entry that will be deleted
    	var oh_id = $(this).parent().attr('id');
    	
    	//send data to the processing file
    	$.post(
   			ajaxurl, 
   			{
     			'action': 'oh_delete',
      			'oh_id': oh_id
   			}, 
   			function(response){
   				
   				//hide element of deleted entry
   				$(clicked_trigger).parent().fadeOut('fast', function() {
   					
   					//check if response = 0; 0 means there no entries left for the week day of the row
   					if(response == 0) {
   					
   						//check if an .oh_closed element already exists in this row. If not generate one.
   						if($(clicked_trigger).parent().parent().children('.oh_closed').length) {
   						
   							//display .oh_closed
   							$(clicked_trigger).parent().parent().children('.oh_closed').fadeIn('fast');
   						
   						} else {
   						
   							//generate and display .oh_closed
   							$(clicked_trigger).parent().parent().children('.oh_add').before('<li class="oh_closed"><p><span class="oh_closed">Closed</span> - Please add a Time Frame</p></li>')
   						
   						}
   					
   					}
   					
   					//destroy element of deleted entry
   					$(this).remove();
   					
   				});
   				
			}
	
		);
    		
	});
	
	//Shows success message
	function dosuccess() {

		//show save button in the original form
		$('#oh_update').removeAttr("disabled").fadeTo(50, 1);
		//show success message
		$('#oh_success_message').slideDown(250).delay(500).fadeOut(250, function() {
		
			//make save button accessible again
			$('#oh_update').bind("click", oh_update);	
		
		});
			
	}
	
	//Shows error message
	function doerror() {

		//show save button in the original form
		$('#oh_update').removeAttr("disabled").fadeTo(50, 1);
		
		//check if there is an error given. If yes, show an error message.
		if(!$('p.error').length)
		$('#oh_error_message').slideDown(250).delay(500).fadeOut(250, function() {
		
			//make save button accessible again
			$('#oh_update').bind("click", oh_update);	
		
		});
		return;	
	
	}
	
	//Saves and updates all settings (Trigger: Save Button -> See below this function)
	var oh_update = function() {
		
		//Deactivate save button
		$('#oh_update').unbind('click', oh_update).fadeTo(50, 0.65);
	
		//Reset errors
		var oh_error = "";
		
		//Get New Config Variables
		var oh_title = $('input[name="oh_title"]').val(); 
		var oh_separator = $('input[name="oh_separator"]').val(); 
		var oh_man_closed = $('input[name="oh_toggle_closed"]').is(':checked'); 
		var oh_custom_closed = $('input[name="oh_closed"]').val(); 
		var oh_open_image = $('input[name="oh_open_image"]').val(); 
		var oh_closed_image = $('input[name="oh_closed_image"]').val(); 
			
		//Validate time frames
		$('.oh_entry input[type=text]').each(function() {
		
			if( !$(this).val() || !$(this).val().match(/\d{2}:\d{2}/)) {
			
				$(this).addClass('error');
				oh_error = 1;
			
			}
			
		});
		
		//Check if there is an error
		if(!oh_error) {
		
			//Send all settings to the processing file
			$.post(
   				ajaxurl, 
   				{
     				'action': 'oh_config_update',
      				'oh_title': oh_title,
      				'oh_separator': oh_separator,
      				'oh_custom_closed': oh_custom_closed,
      				'oh_open_image': oh_open_image,
      				'oh_closed_image': oh_closed_image,
      				'oh_man_closed': oh_man_closed
   				}
			); 
			
			//Get every entry and loop them
			$('.oh_entry').each(function() {
		
				//Get the ID of the current entry
				var oh_id = $(this).attr('id');
				
				//Get the time frame of the current entry
				var item_index = $(this).index();
				var time_from = $(this).children('.time_from').val();
				var time_to = $(this).children('.time_to').val();
					
				//Send data of the current entry to the processing file to update it
				$.post(
   					ajaxurl, 
   					{
     					'action': 'oh_update',
      					'oh_id': oh_id,
      					'time_from': time_from,
      					'item_index': item_index,
      					'time_to': time_to
   					},
   					function(response) {
   					
   						//Check if the response contains an error message. If yes, run the error function to give out an error message
   						if(response != "error") {
   						
   							//If there is no error remove the class ".error" from all elements -> just if there are elements if this class
   							$('*').removeClass('error');
   						
   						} else {
   						
   							//show error message
   							doerror();
   							return false;
   						
   						}
   					
   					}
	
				);

			}).promise().done( function() {
			
				//If everything could be successfully updated show a success message
				dosuccess();
			
			});
			
		} else {
		
			//show error message
			doerror();
		
		}
	
	}
	
	//Assign the update function to the save button
	$('#oh_update').bind("click", oh_update);
	
	//Wordpress Media Upload
	//Show on click on an upload button the media uploader
	$('.oh_upload_button').click(function() {
		
		//get the form field sibling of the triggered button
 		formfield = $(this).prev();
 		//show the media uploader
 		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
 		return false;
 		
	});
	
	//Overwrite the "send_to_editor" function - Send uploaded file to the form field
	window.send_to_editor = function(html) {
	
		//get the URL of the uploaded file
 		imgurl = jQuery('img',html).attr('src');
 		//set the value of the form field to the image URL
 		$(formfield).val(imgurl);
 		//hide the media uploader
 		tb_remove();
	}
	
	//Toggle Open and Closed (Button)
	$('#oh_toggle_closed').click(function() {
	
		//toggle Class
		$(this).toggleClass('active');
		
		//if buttons has class "active" toggle message and set property of sibling
		if($(this).hasClass('active')) {
		
			$(this).val('Your business is closed');
			$('#oh_toggle_closed_checkbox').prop('checked', 'checked');
		
		} else {
		
			$(this).val('Your business is open');
			$('#oh_toggle_closed_checkbox').prop('checked', '');
		
		}
	
	});
    
});