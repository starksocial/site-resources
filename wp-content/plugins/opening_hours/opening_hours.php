<?php
/*
Plugin Name: Opening Hours
Plugin URI: http://themeforest.net/user/harbor
Description: Opening Hours is an easy-to-use wordpress plugin for displaying your business hours.
Version: 1.4.1
Author: Harbor
Author URI: http://hrbor.com
License: Regular Licence (http://themeforest.net/licenses/regular_extended)
*/

//Make variables accessible for other functions/files
global $wpdb;
global $table_name;
global $oh_current_time;
global $dow_int;
global $days_of_week;

//The Blogtime
$oh_current_time = current_time('mysql'); 

//Helper for We're Open Widget - Required for selecting the correct weekday based on date(w, …)
$dow_int = array(

	'0' => 'su',
	'1' => 'mo', 
	'2' => 'tu', 
	'3' => 'we', 
	'4' => 'th', 
	'5' => 'fr', 
	'6' => 'sa' 
		
);	
	
//Define prefix for Wordpress MySQL Database
$table_name = $wpdb->prefix . "opening_hours";

//Include update helper
require_once('wp-updates-plugin.php');
new WPUpdatesPluginUpdater_120( 'http://wp-updates.com/api/2/plugin', plugin_basename(__FILE__));

//SECTION Install & Uninstall

//defines the function that will be triggered on the activation of the plugin
register_activation_hook( __FILE__, 'oh_install' );

//defines the function that will be triggered on the deactivation of the plugin
register_deactivation_hook( __FILE__, 'oh_uninstall' );

//install & activation
function oh_install() {
	
	global $table_name;
	global $wpdb; 

	//create a new table in the database for the business hours
	$sql = "CREATE TABLE  `$table_name` (
				`id` INT NOT NULL AUTO_INCREMENT ,
				`time_from` VARCHAR( 124 ) NOT NULL ,
				`time_to` VARCHAR( 124 ) NOT NULL ,
				`day` VARCHAR( 124 ) NOT NULL ,
				`item_index` INT( 124 ) NOT NULL ,	
				PRIMARY KEY (  `id` )
			);";
		
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);

	//set and predefine all necessary settings for the plugin
	update_option('oh_custom_closed', '');
	update_option('oh_separator', '-');
	update_option('oh_man_closed', false);
	update_option('oh_open_image', '');
	update_option('oh_closed_image', '');
			
}
	
//uninstall & deactivation
function oh_uninstall() {

	global $table_name;
	global $wpdb; 
	
	//delete table from the database
	$wpdb->query("DROP TABLE IF EXISTS $table_name;");

}

//SECTION admin initation

//defines the function which will be triggered when all plugins are loaded > initation of the plugin
add_action('init', 'oh_init');
//defines the function which will be triggered when the "wpadmin" panel will be initiated > initation of plugin page
add_action( 'admin_init', 'oh_admin_init' );

//initation
function oh_init() {

	global $days_of_week;
  
 	//load translation files from the /lang folder
  	load_plugin_textdomain( 'opening-hours', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/'); 
 
	//Used in the widgets and the shortcode and keeps the translations of the week days
	$days_of_week = array(
	
		'mo' => __('Monday'), 
		'tu'=> __('Tuesday'), 
		'we'=> __('Wednesday'), 
		'th'=> __('Thursday'), 
		'fr'=> __('Friday'), 
		'sa'=> __('Saturday'), 
		'su'=> __('Sunday')
			
	);	
	
}

//admin initation
function oh_admin_init() {

	//register all required files (javascript files + admin.css) for the plugin page
	wp_register_style( 'oh_admin_style', plugins_url('/css/admin.css', __FILE__) );   
	
	wp_register_script( 'oh_helper_js', plugins_url('/javascript/helper.js', __FILE__) );   
	wp_register_script( 'oh_timepicker', plugins_url('/javascript/jquery-ui-timepicker-addon.js', __FILE__) ); 
	
	
	//check if wordpress is version 3.2, if yes register custom jquery ui (solves a compatibility bug with the time picker add-on)
	if(substr(get_bloginfo('version'), 0, 3) == '3.2') {
		wp_register_script( 'oh_custom_ui', plugins_url('/javascript/jquery-ui-custom.js', __FILE__) ); 	
	}

		
}

//SECTION widgets

//Initiliaze widgets
add_action( 'widgets_init', 'oh_widgets_init');

//Opening Hours Widget -> Shows business hours
class oh_main_widget extends WP_Widget {

	//meta and initialization
	public function __construct() {
		
		parent::__construct(
	 		'opening_hours', // Base ID
			__('Opening Hours', 'opening-hours'), // Name
			array( 'description' => __( 'Displays your configured business hours. (Widget can be configured under "Plugins" > "Opening Hours")', 'opening-hours')) // Args
		);
		
	}
	
	//renders the widget
	public function widget( $args, $instance ) {
	
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
   		//widget content
   		echo $before_widget;
   		
   		//get and display the configured widget title > can be set on the plugin page
   		echo $before_title . $title . $after_title;
   
   		//include the widget file from the /view folder
   		include("view/widget/opening_hours_main.php");
   
   		echo $after_widget;
   		//widget content end		
	
	}
	
	//updates the options of the widgets (title)
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		
		//updates title of the instance
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
		
	}
	
	//renders the form for the widget
	public function form( $instance ) {
	
		//if title is defined for this instance use defined title, otherwise use default title
		if ( isset( $instance[ 'title' ] ) ) {
			
				$title = $instance[ 'title' ]; //gets title
				
		} else {
		
			$title =  __('Opening Hours', 'opening-hours'); //the default title
			
		}
		
			//the form
		?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
		<?php 
	}
}

//We're Open Widget -> Shows image if business is either open or closed
class oh_were_open_widget extends WP_Widget {

	//meta and initialization 
	public function __construct() {
		
		parent::__construct(
	 		'oh_were_open_widget', // Base ID
			'We´re Open', // Name
			array( 'description' => __( 'Shows an configured or predefined image based on if your business is currently either open or closed. (Images can be set under "Plugins" > "Opening Hours")', 'opening-hours')) // Args
		);
		
	}
	
	//renders the widget
	public function widget( $args, $instance ) {
	
   		extract($args);
   
   		//widget content
   		echo $before_widget;
   
   		//include the widget file from the /view folder
   		include("view/widget/opening_hours_wo.php");
   
   		echo $after_widget;
   		//widget content end	
	
	}
	
}

//registers widgets of the plugin whenever wordpress initializes it's widgets
function oh_widgets_init() {

	register_widget( "oh_main_widget" );
	register_widget( "oh_were_open_widget" );

}

//SUBSECTION widget functions

//returns Open Image to the "We're Open" widget
function oh_open() {

	//check if a custom image is defined > if not return predefined image
	if(get_option('oh_open_image')) {
		return '<img src="' .get_option('oh_open_image'). '" class="oh_image oh_open oh-custom_image" style="box-shadow: none; border: none;" alt="We´re Open" />';
	} else {
		return '<img src="' .plugin_dir_url(__FILE__). '/images/open.png" class="oh_image oh_open" style="box-shadow: none; border: none;" alt="We´re Open" />';
	}

}
	
//returns Closed Image to the "We're Open" widget
function oh_closed() {

	//check if a custom image is defined > if not return predefined image
	if(get_option('oh_closed_image')) {
		return '<img src="' .get_option('oh_closed_image'). '" class="oh_image oh_closed oh_custom_image" style="box-shadow: none; border: none;" alt="We´re Closed" />';
	} else {
		return '<img src="' .plugin_dir_url(__FILE__). '/images/closed.png" class="oh_image oh_closed" style="box-shadow: none; border: none;" alt="We´re Closed" />';
	}


}

//converts string from the database into a date string in the from wordpress given format
function oh_convertime($time) {

	//get time format from the options
	$time_format = 	get_option('time_format');

	//convert string from the database into a timestamp
	$time = strtotime(strip_tags($time));
	
	//if the converted timestamp is not 0 return the timestamp as a date string
	if($time) {
		return date($time_format, $time);
	} else {
		return 0;
	}

}

//SECTION shortcodes

//declares function which will be triggered if user uses shortcode
function oh_shortcode( $atts ) {

	//extracts attributes from shortcode -> day="{days)"
	extract( shortcode_atts( array(
		'days' => 'su, mo, tu, we, th, fr, sa'
	), $atts ) );
	
	//cuts attribute into array
	$days = explode(", ", $days);
	
	//start output buffer
	ob_start();
	
	//render shortcode
	include("view/shortcode/opening_hours_shortcode.php");
	
	//save rendered shortcode to variable
	$out = ob_get_contents();
	
	//delete buffer
	ob_clean();
	
	//return rendered shortcode to wordpress
	return $out;
	
}

//adds shortcode to wordpress
add_shortcode( 'opening_hours', 'oh_shortcode' );


//SECTION plugin page (wpadmin)

//defines the function for the plugin page
add_action('admin_menu', 'oh_menu_page');
	
//defines all required scripts which wille be loaded into the plugin page
function oh_load_helper_files() {

		//check if wordpress version = 3.5, if not load scripts for the media upload
		if(substr(get_bloginfo('version'), 0, -1) != '3.5-beta') {
		
			wp_enqueue_script( 'media-upload' );
			wp_enqueue_script( 'thickbox' );

		}
		
		wp_enqueue_script( 'json2' );	
		wp_enqueue_script( 'jquery' );
		
		
		//check if wordpress version is 3.2, if yes load custom jquery ui (solves a compatibility bug with the time picker add-on)
		if(substr(get_bloginfo('version'), 0, 3) != '3.2') {	
		
			wp_enqueue_script( 'jquery-ui-core' );	
			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_enqueue_script( 'jquery-ui-tabs' );	
			wp_enqueue_script( 'jquery-ui-mouse' );	
			wp_enqueue_script( 'jquery-ui-widget' );	
			wp_enqueue_script( 'jquery-ui-sortable' );	
			wp_enqueue_script( 'jquery-ui-slider' );
						
		} else {
		
			wp_enqueue_script( 'oh_custom_ui' );	
		
		}
	
		wp_enqueue_script( 'oh_timepicker' );	
		wp_enqueue_script( 'oh_helper_js' );
	   
}

//defines all required styles which wille be loaded into the plugin page
function oh_load_style_files() {

	wp_enqueue_style( 'oh_admin_style' );
	
	//check if wordpress version = 3.5, if not load style for the media upload
	if(substr(get_bloginfo('version'), 0, -1) != '3.5-beta') {
			wp_enqueue_style('thickbox');
	}

}
	
//registers, outputs and prepares plugin page
function oh_menu_page() {

	//register new plugin page and define output function
	$page = add_plugins_page(__('Opening Hours', 'opening-hours'), __('Opening Hours', 'opening-hours'), 'administrator', 'opening-hours', 'oh_menu_page_output');
	
	//load all required files into the plugin page
	add_action('admin_print_scripts-' . $page, 'oh_load_helper_files');
	add_action('admin_print_styles-' . $page, 'oh_load_style_files');	
}

//plugin page output
function oh_menu_page_output() {

	//includes administration panel from folder /view
	include("view/admin/opening_hours_admin.php");

}

//SECTION AJAX Form processing (required for the plugin page > see section above)
	
//make sure requesting user is administrator to trigger the following actions
if(is_admin()) {

	//define all functions which will be required for the form processing (see javascript/helper.js for details)
	 add_action('wp_ajax_oh_add', 'oh_ajax_add');
	 add_action('wp_ajax_oh_delete', 'oh_ajax_delete');
	 add_action('wp_ajax_oh_update', 'oh_ajax_update');
	 add_action('wp_ajax_oh_config_update', 'oh_ajax_config_update');
	 	 	 
}
	
//adds new time frame to the database (trigger: add+ button)
function oh_ajax_add() {

	global $table_name;
	global $wpdb; 

	//create new entry in the data, oh_day = week day
	$wpdb->insert(
		$table_name, 
		array( 
			'time_from' => "00:00",
			'time_to' => "00:00",			
			'day' => $_POST['oh_day'],
			'item_index' => 99
		)
	);
	
	//return new entry in a template to the plugin page
	echo '<li class="oh_entry" id="' . $_POST['oh_day'] . '_'.$wpdb->insert_id.'">
			<input type="text" name="time_from" value="00:00" class="oh_timepicker time_from" />
			<span class="oh_separator">to</span>
			<input type="text" name="time_to" value="00:00" class="oh_timepicker time_to" />
			<a href="#" class="oh_delete">X</a>
		</li>';
	
	die();

}

//deletes existing time frames from the database (trigger: cross-button)
function oh_ajax_delete() {

	global $table_name;
	global $wpdb;
	
	//prepare values for sql query and delete oh_ from the strings
	$id = substr($_POST['oh_id'], 3);
	$day_of_week = substr($_POST['oh_id'], 0, 2);

	//delete entry from the database
	$wpdb->query( $wpdb->prepare( 
		"DELETE FROM $table_name WHERE id = %s", 
        $id
	));
	
	//count all entries for a week day and return it to the plugin page (required for displaying "closed - …")
	$row_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $table_name WHERE day = %s;", $day_of_week) );
	
	echo $row_count;
	
	die();

}

//updates existing time frame from the database (trigger: "save" button)
function oh_ajax_update() {

	global $table_name;
	global $wpdb;

	//prepare values for sql query and delete oh_ from the strings	
	$id = substr($_POST['oh_id'], 3);
	$day_of_week = substr($_POST['oh_id'], 0, 2);

	//check if the value is in the correct format (0:00) > if not return an error
	if(preg_match("/\d{2}:\d{2}/", $_POST['time_from']) && preg_match("/\d{2}:\d{2}/", $_POST['time_to']) && is_numeric($_POST['item_index'])) {
	
		//updates entry
		$wpdb->update( 
			$table_name, 
			array( 
				'time_from' => $_POST['time_from'],
				'time_to' => $_POST['time_to'],
				'item_index' => $_POST['item_index'],
			), 
			array( 
				'ID' => $id
			) 
		);	
	
	} else {
	
		echo "error";
	
	}

	die();

}

//updates settings (trigger: "save" button)
function oh_ajax_config_update() {

	//if oh_man_closed (manually closed) is false, reset option
	if($_POST['oh_man_closed'] == "false") { 
		$oh_man_closed = '';
	} else {
		 $oh_man_closed = $_POST['oh_man_closed'];
	}

	//update the following settings: Widget Title, "Time"-Separator, Open-Image, Closed-Image, Custom Closed Text, Manually Closed Trigger
	update_option('oh_separator', $_POST['oh_separator']);
	update_option('oh_open_image', $_POST['oh_open_image']);
	update_option('oh_closed_image', $_POST['oh_closed_image']);
	update_option('oh_custom_closed', $_POST['oh_custom_closed']);
	update_option('oh_man_closed', $oh_man_closed);
		
	die();

}

?>