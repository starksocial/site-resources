<div class="oh_wo_wrap" style="text-align:center;">
<?php
	//Widget file for the We're Open widget
	
	//Get all necessary predefined variables from opening_hours.php
	global $wpdb;
	global $oh_current_time;
	global $dow_int;
	global $table_name;	
	
	//Convert blog time into a timestamp
	$oh_curren_time_ut = strtotime($oh_current_time);
	
	//split blog time into pieces
	list( $today_year, $today_month, $today_day, $hour, $minute, $second ) = split( '([^0-9])', $oh_current_time );	
	
	//use blog time timestamp to choose the current week day
	$day_of_week = $dow_int[date('w', $oh_curren_time_ut)];	
	
	//get all time frames from the database for the current week day
	$oh_times = $wpdb->get_results($wpdb->prepare( 
		"SELECT time_to, time_from 
		FROM $table_name
		WHERE day = '%s'", 
		$day_of_week
	));
	
	//check if there are results for the current week day
	if($oh_times) {
	
		//go through all given results
		foreach($oh_times as $oh_time) {
		
			//convert times into a time stamp
			$time_from =  strtotime($today_year . "-" . $today_month . "-" . $today_day . " " . $oh_time->time_from . ":00");
			$time_to =  strtotime($today_year . "-" . $today_month . "-" . $today_day . " " . $oh_time->time_to . ":00");
			
			//check if the current time is between a time frame
			if($time_from < $oh_curren_time_ut && $time_to > $oh_curren_time_ut) {
			
				//if a result is found set $oh_open to 1
				$oh_open = 1;
				break;
			
			}
					
		}
	
	}
	
	//check if oh_open is given (oh_open == 1)
	if(!empty($oh_open) && !get_option('oh_man_closed')) {
	
		//get Open Image from the oh_open function (opening_hours.php) and display it
		echo oh_open();
	
	} else {

		//get Closed Image from the oh_closed function (opening_hours.php) and display it
		echo oh_closed();
	
	}
	
?>
</div>
