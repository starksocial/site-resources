<?php 

	//get all required variables from opening_hours.php
	global $table_name; 
	global $wpdb;
	global $days_of_week;
	
?>

<table class="opening_hours" width="100%">

	<tbody>
	
		<?php 
		
			//loop days of the week
			foreach($days_of_week as $day=>$day_full) { 
			
		?>
		
			<tr class="oh_row">
			
				<td class="oh_day" valign="top" width="40%">
				
					<strong class="oh_day_label"><?php echo $day_full; ?></strong>
				
				</td>
				
				<td valign="top" class="oh_entries_row">
				
					<ul style="list-style:none;" class="oh_entries">
					
						<?php 	
							//get entries for the current week day
							$oh_days = $wpdb->get_results($wpdb->prepare(
									
									"SELECT id, time_from, time_to
									 FROM $table_name
									 WHERE day = %s 
									 ORDER by item_index ASC",
									 $day
								
								 ));
								
							//if there are entries for the current week day, display the results	 
							if($oh_days) {
									 
								//loop given entries, otherwise display "closed" message
								foreach($oh_days as $oh_day) {
								
								//show the entry
						?>
						
								<li class="oh_entry">
								
									<span class="oh_time oh_from"><?php echo oh_convertime($oh_day->time_from); ?></span>
									<span class="oh_separator"><?php echo get_option('oh_separator'); ?></span>
									<span class="oh_time oh_to"><?php echo oh_convertime($oh_day->time_to); ?></span>
								
								</li>
								
						<?php		
								}
								
							} else {	
							
							//display "closed" message
						?>
						
							<li class="oh_entry">
							
								<span class="oh_closed">
								
									<?php	
										//check if custom closed text is set, if not use translated or predefined closed text
										if(get_option('oh_custom_closed') != '') {
											echo get_option('oh_custom_closed');
										} else {
										
											echo _e('Closed', 'opening-hours');
										
										}  
										
									?>
								
								</span>
							
							</li>
						
						<?php } ?>
					
					</ul>
				
				</td>
			
			</tr>
		
		<?php } ?>
	
	</tbody>

</table>