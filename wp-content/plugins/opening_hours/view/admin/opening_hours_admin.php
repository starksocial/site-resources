<?php 

	//Template for the plugin page

	//Get all required variables from opening_hours.php
	global $table_name; 
	global $wpdb;
	

	//Get translations for the week days
	$days_of_week = array(

		'mo' => __('Monday'), 
		'tu'=> __('Tuesday'), 
		'we'=> __('Wednesday'), 
		'th'=> __('Thursday'), 
		'fr'=> __('Friday'), 
		'sa'=> __('Saturday'), 
		'su'=> __('Sunday')
		
	);
	
	//Define "closed" template
	$closed = '<li class="oh_closed"><p><span class="oh_closed">' . __('Closed', 'opening-hours') . '</span> - ' . __('Please add a Time Frame', 'opening-hours') . '</p></li>';
	
	//Define template for the "add"-button
	$add =  '<li class="oh_add">' . __('Add', 'opening-hours') . '+</li>';

?>

<div class="wrap">

<div id="oh_wrap">
	
	<!-- OH_Sidebar -->
	<section id="oh_sidebar">
		
		<!-- Logo -->
		<header>
			
			<div id="oh_logo">Opening Hours</div>
			
		</header>
			
		<!-- Links for the tabs -->
		<nav>
			
			<ul id="oh_tab_links">
				
				<li><a href="#oh_main_settings" id="oh_main_settings_link"><span>&nbsp;</span><?php echo _e('Main Settings', 'opening-hours'); ?></a></li>
				<li><a href="#oh_opening_hours" id="oh_opening_hours_link"><span>&nbsp;</span><?php echo _e('Opening Hours', 'opening-hours'); ?></a></li>
				<li><a href="#oh_widget_images" id="oh_widget_images_link"><span>&nbsp;</span><?php echo _e('Set Widget Images', 'opening-hours'); ?></a></li>		
																	
			</ul>
			
		</nav>
		
	</section>
	<!-- OH_Sidebar END -->	
	
	<!-- OH_Content -->
	<section id="oh_content">
	
		<!-- Topbar in the content area with save button -->
		<section id="oh_topbar">
		
				<input type="button" name="oh_update" id="oh_update" value="<?php echo _e('Save Changes', 'opening-hours'); ?>" />
		
		</section>
		
		<!-- OH_Tabs -->
		<section id="oh_tabs">

			<!-- Main Settings -->
			<div id="oh_main_settings" class="oh_tab">
			
				<h2><?php echo _e('Main Settings', 'opening-hours'); ?></h2>
				<p class="oh_caption"><?php echo _e('Please configure the plugin appearance.', 'opening-hours'); ?></p>

				<div class="oh_form_index">
				
					<h3><?php echo _e('Close business manually', 'opening-hours'); ?></h3>
					<input type="checkbox" name="oh_toggle_closed" id="oh_toggle_closed_checkbox" <?php if(get_option('oh_man_closed')) { ?>checked="checked"<?php } ?> />
					<input type="button" name="oh_toggle_closed_button" class="oh_toggle_button <?php if(get_option('oh_man_closed')) { ?>active<?php } ?>" id="oh_toggle_closed" value="Your business is <?php if(get_option('oh_man_closed')) { ?>closed<?php } else { ?>open<?php } ?>" /> <p class="oh_desc"><?php echo _e('Displays "closed image" manually (independent from your business hours).', 'opening-hours'); ?></p>
				
				</div>
					
				<div class="oh_form_index">
				
					<h3><?php echo _e('“Time”-separator', 'opening-hours'); ?></h3>
					
					<input type="text" name="oh_separator" value="<?php echo get_option('oh_separator'); ?>" id="oh_separator" /> <p class="oh_desc"><?php echo _e('The divider between two times', 'opening-hours'); ?> (ex. "-")</p>
				
				</div>
					
				<div class="oh_form_index">
				
					<h3><?php echo _e('Custom Closed Text', 'opening-hours'); ?></h3>
					<input type="text" name="oh_closed" value="<?php echo get_option('oh_custom_closed'); ?>" id="oh_closed" /> <p class="oh_desc"><?php echo _e('Custom closed text for the main widget.', 'opening-hours'); ?> (optional)</p>
				
				</div>
					
			</div>
		
			<!-- Business Hours/Opening Hours -->
			<div id="oh_opening_hours" class="oh_tab">
		
				<h2><?php echo _e('Configure Opening Hours', 'opening-hours'); ?></h2>
				<p class="oh_caption"><?php echo _e('Please configure your business hours.', 'opening-hours'); ?></p>
			
				<table id="oh_opening_hours_config">
			
					<?php	//loop each day of the week
						foreach($days_of_week as $day=>$day_full) { ?>
						<tr>
				
							<td class="oh_day"><?php echo $day_full; ?></td>
							<td class="oh_row">
					
								<ul id="<?php echo $day; ?>">
								
									<?php 	
										//get entries for the current week day (defined in $day_full)
										$oh_days = $wpdb->get_results($wpdb->prepare(
											"SELECT id, time_from, time_to
										 	FROM $table_name
										 	WHERE day = '%s' ORDER by item_index ASC",
										 	$day
										 ) );
									 
									 	//if there are entries for the current day show them otherwise give out the "closed" template
										if($oh_days) {
									 
									 		//loop for the given entries
									 		foreach($oh_days as $oh_day) {
									 		
									 		//show the entry
									?>
								
										<li class="oh_entry" id="<?php echo $day; ?>_<?php echo $oh_day->id; ?>">
							
											<input type="text" name="time_from" value="<?php echo $oh_day->time_from; ?>" class="oh_timepicker time_from" />
											<span class="oh_separator"><?php echo _e('to', 'opening-hours'); ?></span>
											<input type="text" name="time_to" value="<?php echo $oh_day->time_to; ?>" class="oh_timepicker time_to" />
									
											<a href="#" class="oh_delete">X</a>
							
										</li>
					
									<?php 
										} } else { echo $closed; } 
										
										//display the add button
										echo $add;
									?>
								
								</ul>
					
							</td>
				
						</tr>
					<?php } ?>
					
				</table>
		
			</div>
			
			<!-- We're Open Widget / Widget Images -->
			<div id="oh_widget_images" class="oh_tab">
			
				<h2><?php echo _e('Set Widget Images', 'opening-hours'); ?></h2>
				<p class="oh_caption"><?php echo _e('Please specify the images which will be used in the “We´re Open” Widget. Leave blank if you want to use the pre-made images.', 'opening-hours'); ?></p>
									
				<div class="oh_form_index">
					
					<!-- Open Image -->
					<h3><?php echo _e('Open Image', 'opening-hours'); ?></h3>
					
					<input type="text" name="oh_open_image" value="<?php echo get_option('oh_open_image'); ?>" id="oh_open_image" <?php if(substr(get_bloginfo('version'), 0, -1) == '3.5-beta') echo 'style="max-width:400px !important"'; ?> /> 
					
					<?php if(substr(get_bloginfo('version'), 0, -1) != '3.5-beta') { ?>
						<input type="button" name="oh_open_image_upload" id="oh_open_image_upload" class="oh_upload_button" value="<?php echo _e('Upload'); ?>"  />
					<?php } ?>
					
					<p class="oh_desc"><?php echo _e('URL of the image.', 'opening-hours'); ?> <?php echo _e('Will be shown if open.', 'opening-hours'); ?></p>
				
				</div>
									
				<div class="oh_form_index">
				
					<!-- Closed Image -->
					<h3><?php echo _e('Closed Image', 'opening-hours'); ?></h3>
					
					<input type="text" name="oh_closed_image" value="<?php echo get_option('oh_closed_image'); ?>" id="oh_closed_image" <?php if(substr(get_bloginfo('version'), 0, -1) == '3.5-beta') echo 'style="max-width:400px !important"'; ?> /> 
					
					<?php if(substr(get_bloginfo('version'), 0, -1) != '3.5-beta') { ?>
						<input type="button" name="oh_closed_image_upload" id="oh_close_image_upload" class="oh_upload_button" value="<?php echo _e('Upload'); ?>" />
					<?php } ?>
					
					<p class="oh_desc"><?php echo _e('URL of the image.', 'opening-hours'); ?> <?php echo _e('Will be shown if closed.', 'opening-hours'); ?></p>
				
				</div>
				
			</div>
		
		</section>
		<!-- OH_Tabs END -->
		
		<!-- Messages -->
		<div id="oh_error_message" class="oh_message"><p><?php echo _e('Oops, something went wrong.', 'opening-hours'); ?></p></div>
		<div id="oh_success_message" class="oh_message"><p><?php echo _e('Configuration saved!', 'opening-hours'); ?></p></div>
		<!-- Messages END -->

	</section>
	<!-- OH_Content END -->
		
</div>

</div>

