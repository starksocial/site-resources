<?php
echo "
<h2>How to get started with Geo Calendar</h2>

<p>
	<b>Step 1:</b> Go to <b>\"myEventON > Settings > Sorting and Filtering\"</b> and select <b>\"Event Location\"</b>. This will add event location as an option for filtering events on the front-end. You can always turn this off if you dont want users to select event locations.
</p>
<p>
	<b>Step 2:</b> Add the shortcode.<br/>Go to the page where you have the eventON shortcode on. OR go to a page when you want to add a new calendar. Click the EventON shortcode button from WYSIWYG text editor on this page. From the popup shortcode box select <b>\"Calendar with set Event Location\"</b>. <br/>Type in the address you want as default for the calendar event location exactly, within the shortcode.
</p>
<p>
	<b>Step 3:</b> Save changes and enjoy the new geo calendar.
</p>
<br/>
<p>
	<b>Other Notes:</b><br/>
	If you enable Event Location to show as a filter option, then the location filter will also show up in the eventON widget.
</p>
<br/>
<p><b>Requirements:</b> EventON version 2.1.14 or higher</p>
";
?>