<?php
/*
 * Plugin Name: EventON - Geo Calendar  
 * Plugin URI: http://www.myeventon.com/geo-calendar
 * Description: Extends EventON by allowing location based event searches. 
 * Version: 0.2
 * Author: AshanJay
 * Author URI: http://www.ashanjay.com
 * Requires at least: 3.5
 * Tested up to: 3.6
 */


 
class EventON_geo_cal{
	
	public $slug;
	public $plugin_slug ;	
	public $plugin_url ;	
	public $version='0.2';
	
	private $localtion_value ='';
	private $geo_cal_location_filter_on = false;
	private $is_google_maps_on = false;
	
	/*
	 * Construct
	 */
	public function __construct(){
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) , 15);		
		
		add_filter('eventon_shortcode_default_values',array( $this, 'geocal_shortcode_support' ), 10,1);
		add_filter('eventon_shortcode_argument_update',array( $this, 'geocal_shortcode_arguments' ), 10,1);
		add_filter('eventon_shortcode_options',array($this, 'shortcode_options'), 10, 1);
		add_filter('eventon_settings_tab1_arr_content',array($this, 'eventon_settings_tab1_content'), 10, 1);
		add_filter('eventon_settings_lang_tab_content',array($this, 'eventon_settings_lang_tab_content'), 10, 1);
		
		add_action('eventon_sorting_filters',array( $this, 'content_below_sortbar' ), 5);
		add_action('eventon_save_meta', array($this,'geocal_save_meta_values'),10,2);
	
		
		// BUILD initial value checks
		$evcal_val1= get_option('evcal_options_evcal_1');		
		$filtering_options = (!empty($evcal_val1['evcal_filter_options']))?$evcal_val1['evcal_filter_options']:array();
		
		// Google gmaps API
		if( !empty($evcal_val1['evcal_cal_gmap_api']) && ($evcal_val1['evcal_cal_gmap_api']=='yes') 	){
			if(!empty($evcal_val1['evcal_gmap_disable_section']) && $evcal_val1['evcal_gmap_disable_section']=='complete'){
				$this->is_google_maps_on= false;
			}else{
				$this->is_google_maps_on= true;				
			}			
		}else {
			$this->is_google_maps_on= true;		
		}
				
		
		$this->geo_cal_location_filter_on = (in_array('event_location',$filtering_options))? true:false;

		// get plugin slug
		$this->plugin_slug = plugin_basename(__FILE__);
		list ($t1, $t2) = explode('/', $this->plugin_slug);
        $this->slug = $t1;
		
		$this->add_to_eventon_addons_list();	
		
		// Deactivation
		register_deactivation_hook( __FILE__, array($this,'deactivate'));
		
		
	}
	
	
	/*
		remove this plugin from myEventON Addons list
	*/
	function deactivate(){
		$eventon_addons_opt = get_option('eventon_addons');
		
		if(is_array($eventon_addons_opt) && array_key_exists($this->slug, $eventon_addons_opt)){
			foreach($eventon_addons_opt as $addon_name=>$addon_ar){
				
				if($addon_name==$this->slug){
					unset($eventon_addons_opt[$addon_name]);
				}
			}
		}
		
		update_option('eventon_addons',$eventon_addons_opt);
	}
	
	
	
	
	/** Add this extension's information to EventON addons tab **/
	function add_to_eventon_addons_list(){
		$eventon_addons_opt = get_option('eventon_addons');
		
		$this->plugin_url = path_join(WP_PLUGIN_URL, basename(dirname(__FILE__)));
		$plugin_path = dirname( __FILE__ );
		
		$plugin_details = array(
			'name'=> 		'Geo Calendar for EventON',
			'details'=> 	'Extends EventON by allowing location based event searches.',
			'version'=> 	$this->version,			
			'slug'=>		$this->slug,
			'guide_file'=>		( file_exists($plugin_path.'/guide.php') )? 
				$this->plugin_url.'/guide.php':null,
			'type'=>'extension'
		);
		
		$eventon_addons_ar[$this->slug]=$plugin_details;
		if(is_array($eventon_addons_opt)){
			$eventon_addons_new_ar = array_merge($eventon_addons_opt, $eventon_addons_ar );
		}else{
			$eventon_addons_new_ar = $eventon_addons_ar;
		}
		
		update_option('eventon_addons',$eventon_addons_new_ar);
		
		//print_r(get_option('eventon_addons'));
	}
	
	
	/*
	 * Content for the geo calendar filtering bar
	 */
	function content_below_sortbar(){
		
		// get required saved setting values
		$evcal_val2= get_option('evcal_options_evcal_2');
				
		$content='';
		
		// check if event location is selection is selected as sorting option
		if($this->geo_cal_location_filter_on && $this->is_google_maps_on){			
			
			$event_locations = new WP_Query(
				array('post_type'=>'ajde_events','posts_per_page'=>-1)
			);
			
			if($event_locations->have_posts()):
				
				// INITIAL variable values
				$default_cal_location = (!empty($this->localtion_value))?$this->localtion_value:'All';
				$text_location = (!empty($evcal_val2['evcal_lang_evlocation']))?$evcal_val2['evcal_lang_evlocation']:'Location';	
				
				$locations_list = array();
				
				$content.="
						<div class='eventon_filter eventon_geocal_filter' filter_field='evcal_location' filter_val='{$this->localtion_value}' filter_type='meta'>
							<div class='eventon_sf_field'><p>{$text_location}:</p></div>				
						
							<div class='eventon_filter_selection eventon_geo_cal_filter'>
								<p class='filtering_set_val' opts='evs4_in'>{$default_cal_location}</p>
								<div class='eventon_filter_dropdown' style='display:none'>";
									$content.="<p filter_val='all'>All</p>";
									
									// WHILE loop for locations
									while($event_locations->have_posts()): $event_locations->the_post();		
										
										$loc_id = get_the_ID();
										
										$location = get_post_meta($loc_id,'evcal_location',true);
										
										
										if(!empty($location) ){
											
											$location_slug_exist = get_post_meta($loc_id,'evcal_location_slug',true);
											$location_slug = (!empty($location_slug_exist) )? 
												$location_slug_exist 
												:sanitize_title($location);
											
											if( !in_array($location_slug, $locations_list) ){
												
												// add to locations list
												$locations_list[] = $location_slug;
											
												$content.="<p filter_val='".$location."' filter_slug='".$location_slug."'>".$location."</p>";
											}
										}
									endwhile;										
										
							$content.="</div>
							</div><div class='clear'></div>
						</div>";
				
				
				$content.="<div class='eventon_global_gmap'></div>";
				
			endif; // end if events exist
			wp_reset_query();
		}
		
		echo $content;
	}
	
	/**
	 *	Add the event location translations to language tab
	 */
	function eventon_settings_lang_tab_content($array){
		$new_array = $array;
		
		$addition = array(
			'label'=>'Location',
			'name'=>'evcal_lang_evlocation',
			'legend'=>''
		);
				
		array_push($new_array,$addition);
		
		return $new_array;
	}
	
	/**
	 *	Add the event location as an option for sorting and filtering in myEventON Settings page
	 */
	function eventon_settings_tab1_content($array){
		
		$new_array = $array;
		
		foreach($array as $field=>$settings){
			if($settings['id']=='evcal_001a'){
				
				foreach($settings['fields'] as $f=>$v){
					if($v['id']=='evcal_filter_options'){
						
						// build the location field to add to checkbox options
						$location_option_ar = array(
							'event_location'=>'Event Location'
						);
						$this->array_push_associative($v['options'],$location_option_ar);
						
						$replace = array(0=>$location_option_ar);
						unset($settings['fields'][1]);
						
						array_push($settings['fields'],$v);
						
					}
										
				}
				
				/*
				// code to add to fields list
				$settings_ar=array(
					'id'=>'evcal_filter_options', 
					'type'=>'checkboxes','name'=>'Something'
				);
				array_push($settings['fields'], $settings_ar);
				*/
			}
			
			$new_array[$field]=$settings;
			
			
		}
		
		//print_r($new_array);
		
		return $new_array;
	}
	
	
	/**
	 *	Save the location slug when event data is saved
	 */	
	public function geocal_save_meta_values($fields, $post_id){
		global $post;
		
		if(!empty($_POST[ 'evcal_location']) ){
			$location_slug = sanitize_title($_POST[ 'evcal_location' ]);
			update_post_meta( $post->ID, 'evcal_location_slug', $location_slug);
		}else{
			delete_post_meta( $post_id, 'evcal_location_slug');
		}
		
	}
	
	
	
	
	/**
	 *	Styles for the tab page
	 */	
	public function frontend_scripts(){
		
		wp_register_style( 'eventon_geocal_styles',$this->plugin_url.'/style.css');
		wp_register_script('eventon_geocal_script',$this->plugin_url.'/script.js', array('jquery'), 1.0, true );
		// add js file if event location is selected to show in filtering bar
		
		
		if(class_exists('eventon_sin_event') ){
			global $eventon_sin_event;
			
			if(!$eventon_sin_event->is_single_event){
				wp_enqueue_script('eventon_geocal_script');
				wp_enqueue_style( 'eventon_geocal_styles');
			}
		}else if ( $this->geo_cal_location_filter_on  ) {
			
			wp_enqueue_script('eventon_geocal_script');
			wp_enqueue_style( 'eventon_geocal_styles');
		}
	}
	
	
	
	/**
	 *	Add the new event location shortcode to eventon shortcode popup panel
	 */
	function shortcode_options($shortcode_btns){
		$shortcode_btns['Calendar with set Event Location']='[add_eventon cal_id="1" event_location="{ **type exact location address here }"]';
		return $shortcode_btns;
	}
	
	
	/*
	 * Add the event location argument to the shortcodes
	 */
	function geocal_shortcode_arguments($args){
		if(!empty($args['event_location'])){
			$cal_location['filters'][]= array(
				'filter_type'=>'meta',
				'filter_name'=>'evcal_location',
				'filter_val'=>$args['event_location']
			);	

			$this->localtion_value =$args['event_location'];
			
			$args = array_merge($args,$cal_location);
			return $args;
		}else{
			return $args;
		}
	}
	
	/*
	 * add support for event_location new variable for shortcode
	 */
	function geocal_shortcode_support($supported_defaults){
		$shortcode_array_add = array(
			'event_location'=>''
		);
		
		return $supported_defaults = array_merge($supported_defaults,$shortcode_array_add);
	}
	
	// Append associative array elements
	function array_push_associative(&$arr) {
	   $ret='';
	   $args = func_get_args();
	   foreach ($args as $arg) {
		   if (is_array($arg)) {
			   foreach ($arg as $key => $value) {
				   $arr[$key] = $value;
				   $ret++;
			   }
		   }else{
			   $arr[$arg] = "";
		   }
	   }
	   return $ret;
	}
}
// Initiate this addon within the plugin
$GLOBALS['eventon_geo_cal'] = new EventON_geo_cal();