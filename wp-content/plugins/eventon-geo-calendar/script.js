/*
	Javascript code for geo cal
	version: 0.1
*/
jQuery(document).ready(function($){
	
		
	initial_geo_cal_setup();
	
	function initial_geo_cal_setup(){
		$('.ajde_evcal_calendar').each(function(){
			var location_val = $(this).find('.eventon_geo_cal_filter p').html();
			
			if(location_val!='All'){			
				var cal_id = $(this).attr('id');
				var mapformat = $(this).attr('mapformat');
				var geo_map_id = cal_id+'map';
				$(this).find('.eventon_global_gmap').attr({'id':geo_map_id}).fadeIn();
				var zoom = $(this).attr('mapzoom');
				var zoomlevel = (typeof zoom !== 'undefined' && zoom !== false)? parseInt(zoom):12;
				
				//run gmap
				initialize(geo_map_id, location_val, mapformat, zoomlevel);
				
				// disable gmaps on invidual events
				$(this).find('.desc_trig').attr({'gmap_status':'null'});
				$(this).find('.evcal_gmaps').hide();
			}
		});
	}
	
	
	$('.eventon_geo_cal_filter').find('.eventon_filter_dropdown p').click(function(){
		var new_location = $(this).attr('filter_val');
		var cal = $(this).closest('.ajde_evcal_calendar');
		
		// new location is an actual address
		if(new_location!='all'){			
			var cal_id = cal.attr('id');
			var mapformat = cal.attr('mapformat');
			var geo_map_id = cal_id+'map';
			$(this).closest('.eventon_filter_line').find('.eventon_global_gmap').attr({'id':geo_map_id}).fadeIn();
			
			var zoom = cal.attr('mapzoom');
			var zoomlevel = (typeof zoom !== 'undefined' && zoom !== false)? parseInt(zoom):12;
			
			
			//run gmap
			initialize(geo_map_id, new_location,mapformat,zoomlevel);
			
			// disable gmaps on invidual events
			$(document).ajaxComplete(function() {
				cal.find('.evcal_list_a').attr({'gmap_status':'null'});
				cal.find('.evcal_gmaps').hide();
			});
				
		}else{
		// new location is selected as ALL
			
			$(this).closest('.eventon_filter_line').find('.eventon_global_gmap').fadeOut();
			
			// enable gmaps on invidual events
			$(document).ajaxComplete(function() {
				cal.find('.desc_trig').attr({'gmap_status':''});
				cal.find('.evcal_gmaps').show();
			});
		}
	});
	
});