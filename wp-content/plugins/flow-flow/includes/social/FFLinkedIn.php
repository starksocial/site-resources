<?php if ( ! defined( 'WPINC' ) ) die;
/**
 * FlowFlow.
 *
 * @package   FlowFlow
 * @author    Looks Awesome <email@looks-awesome.com>
 *
 * @link      http://looks-awesome.com
 * @copyright 2014-2015 Looks Awesome
 */

class FFLinkedIn extends FFHttpRequestFeed {
	private $company;
	private $profileUrl;
	private $profileImageUrl;

	public function __construct( $type = null ) {
		if (is_null($type)) $type = 'linkedin';
		parent::__construct( $type );
	}

	/**
	 * Search company.
	 * http://stackoverflow.com.80bola.com/questions/17860616/search-company-api-linkedin
	 *
	 *
	 * @param FFGeneralSettings $options
	 * @param FFStreamSettings $stream
	 * @param $feed
	 */
	public function init( $options, $stream, $feed ) {
		parent::init( $options, $stream, $feed );
		$original = $options->original();
		$token = $original['linkedin_access_token'];
		$start = 0;
		$num = $this->getCount();
		$this->company = $feed->content;
		$event_type = '';
		if (isset($feed->{'event-type'}) && $feed->{'event-type'} != 'any'){
			$event_type = '&event-type=' . $feed->{'event-type'};
		}
		$this->url = "https://api.linkedin.com/v1/companies/{$this->company}/updates?oauth2_access_token={$token}{$event_type}&start={$start}&count={$num}&format=json&format=json-get";
		$this->profileUrl = "https://api.linkedin.com/v1/companies/{$this->company}:(id,name,logo-url,square-logo-url)?oauth2_access_token={$token}&format=json";
	}

	protected function beforeProcess() {
		parent::beforeProcess();
		$data = FFFeedUtils::getFeedData($this->profileUrl);
		if ( sizeof( $data['errors'] ) > 0 ) {
			$this->errors[] = array(
				'type'    => $this->getType(),
				'message' => $data['errors'],
				'url' => $this->getUrl()
			);
		}
		if (isset($data['response'])){
			$profile = json_decode($data['response']);
			if (isset($profile->squareLogoUrl) && !empty($profile->squareLogoUrl)){
				$this->profileImageUrl = $profile->squareLogoUrl;
			}
			else $this->profileImageUrl = $profile->logoUrl;
		}
	}

	protected function items( $request ) {
		$pxml = json_decode($request);
		if (isset($pxml->values)) { return $pxml->values; }
		return array();
	}

	protected function getId( $item ) {
		if (isset($item->updateContent->companyStatusUpdate)){
			return $item->updateContent->companyStatusUpdate->share->id;
		}
		elseif (isset($item->updateContent->companyJobUpdate)){
			return $item->updateContent->companyJobUpdate->job->id;
		}
	}

	protected function getHeader( $item ) {
		if (isset($item->updateContent->companyStatusUpdate)){
			return $item->updateContent->companyStatusUpdate->share->content->title;
		}
		elseif (isset($item->updateContent->companyJobUpdate)){
			return $item->updateContent->companyJobUpdate->job->position->title;
		}
	}

	protected function getScreenName( $item ) {
		return $item->updateContent->company->name;
	}

	protected function getProfileImage( $item ) {
		return $this->profileImageUrl;
	}

	protected function getSystemDate( $item ) {
		return (int) floor($item->timestamp/1000);
	}

	protected function getContent( $item ) {
		if (isset($item->updateContent->companyStatusUpdate)){
			return $item->updateContent->companyStatusUpdate->share->content->description;
		}
		elseif (isset($item->updateContent->companyJobUpdate)){
			$location = $item->updateContent->companyJobUpdate->job->locationDescription;
			return $location . '<br>' . $item->updateContent->companyJobUpdate->job->description;
		}
	}

	protected function getUserlink( $item ) {
		return 'https://www.linkedin.com/company/' . $this->company;
	}

	protected function getPermalink( $item ) {
		if (isset($item->updateContent->companyStatusUpdate)){
			return $item->updateContent->companyStatusUpdate->share->content->shortenedUrl;
		}
		elseif (isset($item->updateContent->companyJobUpdate)){
			return $item->updateContent->companyJobUpdate->job->siteJobRequest->url;
		}
	}

	protected function showImage( $item ) {
		return isset($item->updateContent->companyStatusUpdate);
	}

	protected function getImage( $item ) {
		$image_url = $item->updateContent->companyStatusUpdate->share->content->submittedImageUrl;
		return $this->createImage($image_url);
	}

	protected function getMedia( $item ) {
		$image_url = $item->updateContent->companyStatusUpdate->share->content->submittedImageUrl;
		return $this->createMedia($image_url);
	}
}