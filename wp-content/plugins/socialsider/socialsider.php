<?php  
/* 
Plugin Name: Socialsider
Plugin URI: http://codecanyon.net/item/socialsider-universal-social-sidebar/7042612?ref=dczerwonski
Version: 1.1
Author: Dawid Czerwoński
Author URI: http://www.czerwonski.pl
Description: Universal Social Sidebar
*/  

/* Function: socialsider_return_list */
/* Description: Function returns  array of social icons available */
function socialsider_return_list() {
	$SocialsiderSocialList[] = array('facebook','Facebook');
	$SocialsiderSocialList[] = array('pinterest','Pinterest');
	$SocialsiderSocialList[] = array('vimeo','Vimeo');
	$SocialsiderSocialList[] = array('windows','Windows');
	$SocialsiderSocialList[] = array('skype','Skype');
	$SocialsiderSocialList[] = array('twitter','Twitter');
	$SocialsiderSocialList[] = array('google','Google');
	$SocialsiderSocialList[] = array('foursquare','Foursquare');
	$SocialsiderSocialList[] = array('yahoo','Yahoo');
	$SocialsiderSocialList[] = array('yelp','Yelp');
	$SocialsiderSocialList[] = array('feedburner','Feedburner');
	$SocialsiderSocialList[] = array('linkedin','Linkedin');
	$SocialsiderSocialList[] = array('viadeo','Viadeo');
	$SocialsiderSocialList[] = array('xing','Xing');
	$SocialsiderSocialList[] = array('myspace','MySpace');
	$SocialsiderSocialList[] = array('soundcloud','Soundcloud');
	$SocialsiderSocialList[] = array('spotify','Spotify');
	$SocialsiderSocialList[] = array('grooveshark','Grooveshark');
	$SocialsiderSocialList[] = array('lastfm','Last.fm');
	$SocialsiderSocialList[] = array('youtube','YouTube');
	$SocialsiderSocialList[] = array('dailymotion','Dailymotion');
	$SocialsiderSocialList[] = array('vine','Vine');
	$SocialsiderSocialList[] = array('flickr','Flickr');
	$SocialsiderSocialList[] = array('500px','500px');
	$SocialsiderSocialList[] = array('instagram','Instagram');
	$SocialsiderSocialList[] = array('wordpress','Wordpress');
	$SocialsiderSocialList[] = array('tumblr','Tumblr');
	$SocialsiderSocialList[] = array('blogger','Blogger');
	$SocialsiderSocialList[] = array('technorati','Technorati');
	$SocialsiderSocialList[] = array('reddit','Reddit');
	$SocialsiderSocialList[] = array('dribbble','Dribble');
	$SocialsiderSocialList[] = array('stumbleupon','Stumbleupon');
	$SocialsiderSocialList[] = array('digg','Digg');
	$SocialsiderSocialList[] = array('envato','Envato');
	$SocialsiderSocialList[] = array('behance','Behance');
	$SocialsiderSocialList[] = array('delicious','Delicious');
	$SocialsiderSocialList[] = array('deviantart','Deviantart');
	$SocialsiderSocialList[] = array('forrst','Forrst');
	$SocialsiderSocialList[] = array('play','Play');
	$SocialsiderSocialList[] = array('zerply','Zerply');
	$SocialsiderSocialList[] = array('wikipedia','Wikipedia');
	$SocialsiderSocialList[] = array('apple','Apple');
	$SocialsiderSocialList[] = array('flattr','Flattr');
	$SocialsiderSocialList[] = array('github','GitHub');
	$SocialsiderSocialList[] = array('chimein','ChimeIn');
	$SocialsiderSocialList[] = array('friendfeed','Friendfeed');
	$SocialsiderSocialList[] = array('newsvine','Newsvine');
	$SocialsiderSocialList[] = array('identica','Identica');
	$SocialsiderSocialList[] = array('bebo','Bebo');
	$SocialsiderSocialList[] = array('zynga','Zynga');
	$SocialsiderSocialList[] = array('steam','Steam');
	$SocialsiderSocialList[] = array('xbox','xBox');
	$SocialsiderSocialList[] = array('outlook','Outlook');
	$SocialsiderSocialList[] = array('coderwall','Coderwall');
	$SocialsiderSocialList[] = array('tripadvisor','TripAdvisor');
	$SocialsiderSocialList[] = array('netcodes','NetCodes');
	$SocialsiderSocialList[] = array('easid','Easid');
	$SocialsiderSocialList[] = array('lanyrd','Lanyrd');
	$SocialsiderSocialList[] = array('slideshare','Slideshare');
	$SocialsiderSocialList[] = array('buffer','Buffer');
	$SocialsiderSocialList[] = array('rss','RSS');
	$SocialsiderSocialList[] = array('vkontakte','vKontakte');
	return $SocialsiderSocialList;
}

/* Backend */

/* Function: socialsider_plugin_settings */
/* Description: Function adds position to admin menu */
function socialsider_plugin_settings() {
	add_menu_page('Socialsider', 'Socialsider', 'administrator', 'socialsider_settings', 'socialsider_display_settings');
}


/* Function: socialsider_display_settings */
/* Description: Function displays admin panel */
function socialsider_display_settings() {
	$SocialsiderSocialList = socialsider_return_list();

	get_option('socialsider_hidden');
	get_option('socialsider_opacity');
	get_option('socialsider_spacer');
	get_option('socialsider_radius');
	get_option('socialsider_colorscheme');
	get_option('socialsider_attachment');
	get_option('socialsider_position');

  $return .= '<h1>Socialsider</h1><form action="options.php" method="post" name="options">' . wp_nonce_field('update-options') . '<h2>Display options</h2>
  <ul><li><label style="width:150px; display:inline-block;">Position:</label>
				<select name="socialsider_position">
					<option '.selected( get_option('socialsider_position'), 'socialsider_left_top', false ).' value="socialsider_left_top">Left Top</option>
					<option '.selected( get_option('socialsider_position'), 'socialsider_left_middle', false ).' value="socialsider_left_middle">Left Middle</option>
					<option '.selected( get_option('socialsider_position'), 'socialsider_left_bottom', false ).' value="socialsider_left_bottom">Left Bottom</option>
					<option '.selected( get_option('socialsider_position'), 'socialsider_right_top', false ).' value="socialsider_right_top">Right top</option>
					<option '.selected( get_option('socialsider_position'), 'socialsider_right_middle', false ).' value="socialsider_right_middle">Right Middle</option>
					<option '.selected( get_option('socialsider_position'), 'socialsider_right_bottom', false ).' value="socialsider_right_bottom">Right Bottom</option>
				</select>
				<li><label style="width:150px; display:inline-block;">Attachment:</label>
				<select name="socialsider_attachment">
					<option '.selected( get_option('socialsider_attachment'), 'socialsider_fixed', false ).' value="socialsider_fixed">Fixed (beware too many elements)</option>
					<option '.selected( get_option('socialsider_attachment'), 'socialsider_absolute', false ).' value="socialsider_absolute">Absolute</option>
				</select>
				
				<li><label style="width:150px; display:inline-block;">Colors:</label>
				<select name="socialsider_colorscheme">
					<option '.selected( get_option('socialsider_colorscheme'), 'socialsider_bgdark_white', false ).' value="socialsider_bgdark_white">Dark background, white font</option>
					<option '.selected( get_option('socialsider_colorscheme'), 'socialsider_bgdark_white socialsider_colorize', false ).' value="socialsider_bgdark_white socialsider_colorize">Dark background, white font + Color hover</option>
					<option '.selected( get_option('socialsider_colorscheme'), 'socialsider_bgwhite_dark', false ).' value="socialsider_bgwhite_dark">White background, dark font</option>
					<option '.selected( get_option('socialsider_colorscheme'), 'socialsider_bgwhite_dark socialsider_colorize', false ).' value="socialsider_bgwhite_dark socialsider_colorize">White background, dark font + Color hover</option>
					<option '.selected( get_option('socialsider_colorscheme'), 'socialsider_bgwhite_color', false ).' value="socialsider_bgwhite_color">White background, color font</option>
					<option '.selected( get_option('socialsider_colorscheme'), 'socialsider_bgcolor_white', false ).' value="socialsider_bgcolor_white">Color background, white font</option>
				</select>
				
				<li><label style="width:150px; display:inline-block;">Edge radius:</label>
				<select name="socialsider_radius">
					<option '.selected( get_option('socialsider_radius'), 'socialsider_radius', false ).' value="socialsider_radius">Yes</option>
					<option '.selected( get_option('socialsider_radius'), '', false ).' value="">No</option>
				</select>
				
				<li><label style="width:150px; display:inline-block;">1px spacer:</label>
				<select name="socialsider_spacer">
					<option '.selected( get_option('socialsider_spacer'), 'socialsider_spacer', false ).' value="socialsider_spacer">Yes</option>
					<option '.selected( get_option('socialsider_spacer'), '', false ).' value="">No</option>
				</select>
				
				<li><label style="width:150px; display:inline-block;">Shadow:</label>
				<select name="socialsider_opacity">
					<option '.selected( get_option('socialsider_opacity'), 'socialsider_shadow', false ).' value="socialsider_shadow">Yes</option>
					<option '.selected( get_option('socialsider_opacity'), '', false ).' value="">No</option>
				</select>
				
				<li><label style="width:150px; display:inline-block;">Opacity on non-hover:</label>
				<select name="socialsider_opacity">
					<option '.selected( get_option('socialsider_opacity'), 'socialsider_opacity', false ).' value="socialsider_opacity">Yes</option>
					<option '.selected( get_option('socialsider_opacity'), '', false ).' value="">No</option>
				</select>
				
				<li><label style="width:150px; display:inline-block;">Half - hidden:</label>
				<select name="socialsider_hidden">
					<option '.selected( get_option('socialsider_hidden'), 'socialsider_hidden', false ).' value="socialsider_hidden">Yes</option>
					<option '.selected( get_option('socialsider_hidden'), '', false ).' value="">No</option>
				</select>
				</li>
				<li><label style="width:150px; display:inline-block;">Open links in new window?:</label>
				<select name="socialsider_targetblank">
					<option '.selected( get_option('socialsider_targetblank'), '1', false ).' value="1">Yes</option>
					<option '.selected( get_option('socialsider_targetblank'), '0', false ).' value="0">No</option>
				</select>
				</li></ul>
  <h2>Your social links</h2>
<div style="overflow:hidden;">';

  foreach($SocialsiderSocialList as $Social) {
	$SocialsiderSocialListComma .= 'socialsider_' . $Social[0] . ',';
	$return .= '<div style="float:left;"><label style="text-align:right; width:150px; display:inline-block;">'.$Social[1].': </label><input type="text" name="socialsider_'.$Social[0].'" value="'.get_option('socialsider_'.$Social[0]).'" /></div>';
  }
  $SocialsiderSocialListComma .= 'socialsider_targetblank,socialsider_hidden,socialsider_opacity,socialsider_opacity,socialsider_spacer,socialsider_radius,socialsider_colorscheme,socialsider_attachment,socialsider_position';
  
  $return .= '</div><input type="hidden" name="action" value="update" /><input type="hidden" name="page_options" value="'.$SocialsiderSocialListComma.'" /><input type="submit" name="Submit" value="Update" /></form></div>';
  
  print $return;
}


/* Function: socialsider_enqueue_styles */
/* Description: Enqueue's CSS file */
function socialsider_enqueue_styles() {
	wp_register_style('socialsider', plugins_url('socialsider-v1.0/_css/socialsider-v1.0.css',__FILE__ ));
	wp_enqueue_style('socialsider');
}

/* Function: socialsiderCode */
/* Description: Plugins' frontend code */
function socialsiderCode() {
	$SocialsiderSocialList = socialsider_return_list();

	print '<div class="socialsider '.
	get_option('socialsider_hidden').' '.
	get_option('socialsider_opacity').' '.
	get_option('socialsider_spacer').' '.
	get_option('socialsider_radius').' '.
	get_option('socialsider_colorscheme').' '.
	get_option('socialsider_attachment').' '.
	get_option('socialsider_position').'"><ul>';
	
	foreach($SocialsiderSocialList as $Social) {
		if (strlen(get_option('socialsider_'.$Social[0]))) { 
				$Url = get_option('socialsider_'.$Social[0]);
				$Url = str_replace("[PAGE_URL]", "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], $Url);
				print '<a ';
				if (get_option('socialsider_targetblank') == "1") { print 'target="_blank" '; } 
				print 'data-socialsider="'.$Social[0].'" href="'.$Url.'" title="'.$Social[1].'"></a>'; 
			}
	}
	print '</ul></div>';
}

/* Hooks */
add_action('wp_footer','socialsiderCode');
add_action('admin_menu', 'socialsider_plugin_settings');
add_action('wp_enqueue_scripts', 'socialsider_enqueue_styles' ); 
