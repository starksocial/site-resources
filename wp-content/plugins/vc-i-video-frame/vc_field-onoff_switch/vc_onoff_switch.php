<?php
/*
|--------------------------------------------------------------------------
| Include styling for onoff switch field
|--------------------------------------------------------------------------
|
*/
if(!function_exists('vc_onoff_switch_admin_enqueue')) {
    function vc_onoff_switch_admin_enqueue() {
        wp_enqueue_style('vc_onoff_switch', plugins_url( 'vc_onoff_switch.css', __FILE__ ));
    }
    add_action( 'admin_enqueue_scripts', 'vc_onoff_switch_admin_enqueue' );
}



/*
|--------------------------------------------------------------------------
| The onoff switch field for visual composer
|--------------------------------------------------------------------------
|
| By default the switch is set to ON
|
*/
if(!function_exists('vc_onoff_switch')) {
    function vc_onoff_switch($settings, $value) {
        $dependency = vc_generate_dependencies_attributes($settings);
        //switch id
        $id = str_shuffle("abcdefghijk");
        //switch default value
        if($value == '') $value = '1';
        //set switch position
        $sel_1 = ($value == '1')? 'selected' : '';
        $switch_on = ($value == '1')? 'switch_on' : '';
        $sel_0 = ($value == '0')? 'selected' : '';
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('#<?php echo $id ?>').on('click', function() {
                    var t = $(this);
                    t.toggleClass('switch_on');
                    if(t.hasClass('switch_on')) {
                        $('#sel_<?php echo $id ?>').val('1');
                    } else {
                        $('#sel_<?php echo $id ?>').val('0');
                    }
                });
            });
        </script>
        <?php
        $switch_select = '<select id="sel_'.$id.'" name="'.$settings['param_name'].'" class="vc_onoff_switch_sel ivf_options wpb_vc_param_value '.$settings['param_name'].' '.$settings['type'].'_field">'
                            .'<option  value="1" '.$sel_1.'>1</option>'
                            .'<option  value="0" '.$sel_0.'>0</option>'
                        .'</select>';
        $switch = '<div id="'.$id.'" class="vc_onoff_switch '.$switch_on.'">'
                    .'<span></span>'
                .'</div>';

        return  $switch_select.$switch;
    }
}