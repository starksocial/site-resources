<?php
/*
Plugin Name: Visual Composer iVideoFrame
Plugin URI: http://code.capital/
Description: Visual Composer element that provides beautiful Apple device screen frame for your video, image or slideshow.
Version: 1.2
Author: Konrad Węgrzyniak
Author URI: http://code.capital/
Copyright: Code Capital
*/





/*
|--------------------------------------------------------------------------
| Plugin update checker
|--------------------------------------------------------------------------
|
*/
//http://w-shadow.com/blog/2010/09/02/automatic-updates-for-any-plugin/
if(!class_exists( "PluginUpdateChecker_1_6" )) {
    require('plugin-updates/plugin-update-checker.php');
}
$boxit_update = new PluginUpdateChecker_1_6(
    'http://code.capital/updates/plugin_upgrade/vc-i-video-frame/info.json',
    __FILE__,
    'vc-i-video-frame' //plugin slug
);





/*
|--------------------------------------------------------------------------
| Include required scripts & styles
|--------------------------------------------------------------------------
|
*/
function ivf_enqueue() {
    wp_enqueue_script('ivf-js', plugins_url( 'js/ivf.js', __FILE__ ), array('jquery'), false, true);
    wp_enqueue_style('ivf-css', plugins_url( 'css/ivf.css', __FILE__ ));
}
add_action( 'wp_enqueue_scripts', 'ivf_enqueue' );

function ivf_admin_enqueue() {
    wp_enqueue_style('ivf-admin-css', plugins_url( 'css/ivf-admin.css', __FILE__ ));
    //wp_enqueue_script('ivf-admin-js', plugins_url( 'js/ivf-admin.js', __FILE__ ), array('jquery'), false, true);
}
add_action( 'admin_enqueue_scripts', 'ivf_admin_enqueue' );





//include the switch field
require('vc_field-onoff_switch/vc_onoff_switch.php');




/*
|--------------------------------------------------------------------------
| Sets up the Video Frame element (after theme setup)
|--------------------------------------------------------------------------
|
*/
function ivf_setup() {
    if(function_exists('add_shortcode_param') && function_exists('vc_map')) {
        // load frame switcher field
        add_shortcode_param('ivc_frames', 'ivc_frame_select');
        // load icon switcher field
        add_shortcode_param('ivc_play_icons', 'ivc_play_icon_select');
        // load onoff switch field
        add_shortcode_param('onoff_switch', 'vc_onoff_switch');
        // VC element settings
        $video_frame_settings = array(
            "name" => 'iVideoFrame',
            "base" => "ivf",
            "class" => "",
            "description" => "Apple device screen frame for video player or image",
            "category" => '',
            "icon" => 'ivf',
            "params" => array(
                array(
                    "heading"       => "Video URL",
                    "type"          => "textfield",
                    "holder"        => "div",
                    "param_name"    => "video_url",
                    "value"         => "",
                    "description"   => ""
                ),
                array(
                    "heading"       => 'Video frame',
                    "type"          => "ivc_frames",
                    "holder"        => "div",
                    "param_name"    => "frame",
                    "value"         => "",
                    "description"   => ""
                ),
                array(
                    "heading"            => 'Device orientation',
                    "type"               => "dropdown",
                    "param_name"         => "orientation",
                    "edit_field_class"   => "frame_advanced frame_advanced_orientation vc_col-sm-12 vc_column",
                    "admin_label"        => false,
                    "value"              => array(
                        'Vertical'       => 'ver',
                        'Horisontal'     => 'hor'
                    )
                ),
                /*
                B - black (ipad + iphone)
                W - white (ipad + iphone)
                G - gold
                S - silver
                 */
                array(
                    "heading"            => 'iPhone color',
                    "type"               => "dropdown",
                    "param_name"         => "iphone_color",
                    "edit_field_class"   => "frame_advanced frame_advanced_iphone_color vc_col-sm-12 vc_column",
                    "admin_label"        => false,
                    "value"              => array(
                        'Silver'         => 'S',
                        'Space Gray'     => 'B',
                        'Gold'           => 'G'
                    )
                ),
                array(
                    "heading"            => 'iPad color',
                    "type"               => "dropdown",
                    "param_name"         => "ipad_color",
                    "edit_field_class"   => "frame_advanced frame_advanced_ipad_color vc_col-sm-12 vc_column",
                    "admin_label"        => false,
                    "value"              => array(
                        'Space Gray'     => 'B',
                        'Silver'         => 'W'
                    )
                ),
                array(
                    "heading"       => "Video frame screen background",
                    "type"          => "attach_images",
                    "holder"        => "div",
                    "param_name"    => "bgd",
                    "admin_label"   => false,
                    "value"         => ""
                ),
                array(
                    "heading"       => 'Slideshow auto play',
                    "type"          => "onoff_switch",
                    "param_name"    => "slider_autoplay",
                    "edit_field_class"   => "ivf_slider_options vc_col-sm-12 vc_column",
                    "admin_label"   => false,
                    "value"         => "1",
                ),
                array(
                    "heading"       => 'Pause slideshow on hover',
                    "type"          => "onoff_switch",
                    "param_name"    => "slider_pause_on_hover",
                    "edit_field_class"   => "ivf_slider_options vc_col-sm-12 vc_column",
                    "admin_label"   => false,
                    "value"         => "1",
                ),
                array(
                    "heading"            => 'Change slide every',
                    "type"               => "dropdown",
                    "param_name"         => "slider_onstage",
                    "edit_field_class"   => "ivf_slider_options onstage_dropdown vc_col-sm-12 vc_column",
                    "admin_label"        => false,
                    "value"              => array(
                        '4 seconds'          => '4000',
                        '6 seconds'          => '6000',
                        '8 seconds'          => '8000',
                        '10 seconds'         => '10000',
                        '12 seconds'         => '12000',
                        '14 seconds'         => '14000',
                        '16 seconds'         => '16000',
                        '18 seconds'         => '18000',
                        '20 seconds'         => '20000'
                    )
                ),
                array(
                    "heading"       => 'Always show slideshow navigation (shows only on hover when set to "off")',
                    "type"          => "onoff_switch",
                    "param_name"    => "slider_arrows",
                    "edit_field_class"   => "ivf_slider_options vc_col-sm-12 vc_column",
                    "admin_label"   => false,
                    "value"         => "0",
                ),
                array(
                    "heading"       => 'Show slideshow progress bar',
                    "type"          => "onoff_switch",
                    "param_name"    => "slider_progress",
                    "edit_field_class"   => "ivf_slider_options vc_col-sm-12 vc_column",
                    "admin_label"   => false,
                    "value"         => "1",
                ),
                array(
                    "heading"       => 'Play button icon',
                    "type"          => "ivc_play_icons",
                    "holder"        => "div",
                    "param_name"    => "icon",
                    "admin_label"   => false,
                    "value"         => ""
                ),
                array(
                    "heading"       => 'Play button color',
                    "type"          => "colorpicker",
                    "holder"        => "div",
                    "param_name"    => "icon_color",
                    "admin_label"   => false,
                    "value"         => "#FFFFFF"
                ),
                array(
                    "heading"       => 'Play button animation',
                    "type"          => "onoff_switch",
                    "param_name"    => "play_animation",
                    "admin_label"   => false,
                    "value"         => "1",
                ),
                array(
                    "heading"       => 'Play button shadow',
                    "type"          => "onoff_switch",
                    "param_name"    => "play_shadow",
                    "admin_label"   => false,
                    "value"         => "1",
                ),
                array(
                    "heading"            => 'Play button size',
                    "type"               => "dropdown",
                    "param_name"         => "play_size",
                    "edit_field_class"   => "play_size_sel vc_col-sm-12 vc_column",
                    "admin_label"        => false,
                    "value"              => array(
                        'Large'          => 'play_large',
                        'Small'          => 'play_small'
                    )
                )
            )
        );
        vc_map($video_frame_settings);
    } else {
        //if "add_shortcode_param" or "vc_map" function is not available  display warning message
        add_action( 'admin_notices', 'ivf_error' );
    }
}
add_action('after_setup_theme', 'ivf_setup');






/*
|--------------------------------------------------------------------------
| Optional error reporting when Visual Composer is not available
|--------------------------------------------------------------------------
|
*/
function ivf_error() {
    $message = 'i Video Frame: the Visual Composer function "vc_map" OR "add_shortcode_param" could not be found.';
    ?>
    <div class="error">
        <p><?php echo $message ?></p>
    </div>
    <?php
}





/*
|--------------------------------------------------------------------------
| Settings field: DEVICE FRAME
|--------------------------------------------------------------------------
|
| displays the settings for selecting the drvice for the video frame
|
*/
function ivc_frame_select($settings, $value) {
    $dependency = vc_generate_dependencies_attributes($settings);
    $frames = array('mbp','mba','imac','cd','iphone','ipad');
    if($value == '') $value = $frames[0];
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            // show hidded device settings if iphone || ipad is selected
            if($('.ivf_option[data-frame="iphone"]').hasClass('sel') || $('.ivf_option[data-frame="ipad"]').hasClass('sel')) {
                $('.frame_advanced_orientation').slideDown(200);
            }
            if($('.ivf_option[data-frame="iphone"]').hasClass('sel')) {
                $('.frame_advanced_iphone_color').slideDown(200);
            }
            if($('.ivf_option[data-frame="ipad"]').hasClass('sel')) {
                $('.frame_advanced_ipad_color').slideDown(200);
            }
            //on device change ...
            $('.ivf_option').on('click', function() {
                $('.ivf_option').removeClass('sel');
                $(this).addClass('sel');
                var f = $(this).attr('data-frame');
                $('.ivf_frames .ivf_options').val(f);

                //for iphone/ipad show/hide orientation options
                if(f == 'iphone' || f == 'ipad') {
                    $('.frame_advanced_orientation').slideDown(200);
                } else {
                    $('.frame_advanced_orientation').slideUp(200);
                }
                //for iphone/ipad show/hide color options
                if(f == 'iphone') {
                    $('.frame_advanced_iphone_color').slideDown(200);
                } else {
                    $('.frame_advanced_iphone_color').slideUp(200);
                }
                if(f == 'ipad') {
                    $('.frame_advanced_ipad_color').slideDown(200);
                } else {
                    $('.frame_advanced_ipad_color').slideUp(200);
                }
            });

            //this bit of JS checks if slider options should be displayed
            var screen_background_input = $('input.bgd.attach_images')
            show_hide_slider_options(screen_background_input);
            screen_background_input.on('change', function() {
                show_hide_slider_options($(this));
            });
            function show_hide_slider_options(input) {
                if(input.val().indexOf(",") > -1) {
                    //show slider settings
                    $('.ivf_slider_options').slideDown(200);
                } else {
                    //hide slider settings
                    $('.ivf_slider_options').slideUp(200);
                }
            }

        });
    </script>
    <?php
    $f = '<div class="ivf_frames">';
        foreach ($frames as $frame) {
            $sel = ($value == $frame)? 'sel' : '';
            $f .= '<div class="ivf_option ivf_'.$frame.' '.$sel.'" data-frame="'.$frame.'">';
                switch ($frame) {
                    case 'mbp': $f .= '<span>MacBook Pro</span>'; break;
                    case 'mba': $f .= '<span>MacBook Air</span>'; break;
                    case 'imac': $f .= '<span>iMac</span>'; break;
                    case 'cd': $f .= '<span>Cinema Display</span>'; break;
                    case 'iphone': $f .= '<span>iPhone 6</span>'; break;
                    case 'ipad': $f .= '<span>iPad Air</span>'; break;
                }
            $f .= '</div>';
        }
        $f .= '<select name="'.$settings['param_name'].'" class="ivf_options wpb_vc_param_value '.$settings['param_name'].' '.$settings['type'].'_field">';
            foreach ($frames as $frame) {
                $sel = ($value == $frame)? 'selected' : '';
                $f .= '<option  value="'.$frame.'" '.$sel.'>'.$frame.'</option>';
            }
        $f .= '</select>';
    $f .= '</div>';//end .ivf_frames
    return  $f;
}







/*
|--------------------------------------------------------------------------
| Settings field: FRAME PLAY ICON
|--------------------------------------------------------------------------
|
| displays the settings for selecting the play icon for the video frame
|
*/
function ivc_play_icon_select($settings, $value) {
    $dependency = vc_generate_dependencies_attributes($settings);
    $icons = array('arrow75','key9','play43','play6','small31','youtube12');
    if($value == '') $value = $icons[0];
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.ivf_icon_option').on('click', function() {
                $('.ivf_icon_option').removeClass('sel');
                $(this).addClass('sel');
                var f = $(this).attr('data-icon');
                $('.ivf_icons .ivf_options').val(f);
            });
        });
    </script>
    <?php
    $i = '<div class="ivf_icons">';
        foreach ($icons as $icon) {
            $sel = ($value == $icon)? 'sel' : '';
            $i .= '<div class="ivf_icon_option ivf_'.$icon.' '.$sel.'" data-icon="'.$icon.'">';
                $i .= '<i class="ivf_play-'.$icon.'"></i>';
            $i .= '</div>';
        }
        $i .= '<select name="'.$settings['param_name'].'" class="ivf_options wpb_vc_param_value '.$settings['param_name'].' '.$settings['type'].'_field">';
            foreach ($icons as $icon) {
                $sel = ($value == $icon)? 'selected' : '';
                $i .= '<option  value="'.$icon.'" '.$sel.'>'.$icon.'</option>';
            }
        $i .= '</select>';
    $i .= '</div>';//end .ivf_icons
    return  $i;
}







/*
|--------------------------------------------------------------------------
| VIDEO FRAME SHORTCODE
|--------------------------------------------------------------------------
|
| Main function that creates the video frame based on the shortcode settings
|
*/
function ivf_shortcode( $atts, $content = null ) {
    $video = (isset($atts['video_url']))? $atts['video_url'] : false;
    $player = ivf_video($video);
    $frame = (isset($atts['frame']))? $atts['frame'] : 'mbp';
    $orientation = (isset($atts['orientation']))? $atts['orientation'] : 'ver';
    $iphone_color = (isset($atts['iphone_color']))? $atts['iphone_color'] : 'G';
    $ipad_color = (isset($atts['ipad_color']))? $atts['ipad_color'] : 'W';
    $icon = (isset($atts['icon']))? $atts['icon'] : 'arrow75';
    $icon_color = (isset($atts['icon_color']))? $atts['icon_color'] : '#FFFFFF';
    $play_animation = (isset($atts['play_animation']))? $atts['play_animation'] : '1';
    $animation = ($play_animation == '1')? 'ivf_play_animation' :'';
    $play_shadow = (isset($atts['play_shadow']))? $atts['play_shadow'] : '1';
    $shadow = ($play_shadow == '1')? 'ivf_play_shadow' :'';
    $play_size = (isset($atts['play_size']))? $atts['play_size'] : 'play_large';

    $slider = '';
    if(isset($atts['bgd'])) {
        //explode the background image IDs string (connected with ",") to se if we have single or multiple images
        $bgd = explode(',',$atts['bgd']);
        if(count($bgd) == 1) {
            //single image
            $bgd = wp_get_attachment_image_src($bgd[0], 'full');
            $bgd = $bgd[0]; //(wp_get_attachment_image_src() returns array - we want only the "0" - URL)
        } else {
            //multiple images - prepare slider
            $autoplay = (isset($atts['slider_autoplay']))? $atts['slider_autoplay'] : '1';
            $pause_on_hover = (isset($atts['slider_pause_on_hover']))? $atts['slider_pause_on_hover'] : '1';
            $on_stage = (isset($atts['slider_onstage']))? $atts['slider_onstage'] : '4000';
            $arrows = (isset($atts['slider_arrows']))? $atts['slider_arrows'] : '0';
            $progress = (isset($atts['slider_progress']))? $atts['slider_progress'] : '1';

            $slider = '<div class="ivf_slider" data-autoplay="'.$autoplay.'" data-pause_on_hover="'.$pause_on_hover.'" data-on_stage="'.$on_stage.'" data-progress="'.$progress.'">';
            foreach ($bgd as $img_id) {
                $img = wp_get_attachment_image_src($img_id, 'full');
                $img_src = (isset($img[0]))? $img[0] : plugins_url( 'img/bgd.png', __FILE__ );
                $slider .= '<div class="ivf_slider_img" style="background: url('.$img_src.') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"></div>';
            }
            $slider .= '</div>';
            //slider arrows (if enabled)
            $always_show = ($arrows == '1')? 'always_show' : '';
            $slider .= '<i class="ivf_sa '.$always_show.' ivf_sa_left ivf_sa-'.$icon.' '.$shadow.'" style="color:'.$icon_color.'"></i>';
            $slider .= '<i class="ivf_sa '.$always_show.' ivf_sa_right ivf_sa-'.$icon.' '.$shadow.'" style="color:'.$icon_color.'"></i>';
            //progress bar
            $bar_vis = ($progress == '1')? '' : 'style="display:none"';
            $slider .= '<div class="slide_progres spoh_'.$pause_on_hover.'" '.$bar_vis.' data-val="0"></div>';
        }
    } else {
        // no background in settings - use default image
        $bgd = plugins_url( 'img/bgd.png', __FILE__ );
    }
    //if we have the slider - disregard teh video url (if provided)
    if($slider != '') $player = false;

    $frame_image = $frame;
    $wrapper_class = $frame;

    //if video could not be loaded or no video URL provided...
    $no_video = ($player === false)? 'no_video' : '';
    //$simple_image = ($player === false && $slider === '')? '<img class="ivf_image" src="'.$bgd.'">' : '';
    $simple_image = ($player === false && $slider === '')? '<div class="ivf_image" style="background: url('.$bgd.') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"></div>' : '';

    //this part is only for iphone / ipad frame
    if($frame == 'iphone') {
        if($orientation == 'ver') {
            $frame_image = 'iphone'.$iphone_color;
        } else {
            $frame_image = 'iphone'.$iphone_color.'_hor';
            $wrapper_class = $frame.'_hor';
        }
    }
    if($frame == 'ipad') {
        if($orientation == 'ver') {
            $frame_image = 'ipad'.$ipad_color;
        } else {
            $frame_image = 'ipad'.$ipad_color.'_hor';
            $wrapper_class = $frame.'_hor';
        }
    }

    //the video frame
    $ivf = '<div class="ivf_container">'
            .'<div class="i_video_player '.$wrapper_class.' '.$no_video.'">'
                .'<img class="ivf_frame" src="'.plugins_url( 'img/_'.$frame_image.'.png', __FILE__ ).'">'
                .'<div class="player_wrapper '.$wrapper_class.'" style="background: url('.$bgd.') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">'
                    .$player
                    .$simple_image
                    .$slider
                    .'<i class="ivf_play-'.$icon.' '.$shadow.' '.$animation.' play_btn '.$play_size.'" style="color:'.$icon_color.'"></i>'
                .'</div>'
            .'</div>'
        .'</div>';
    return $ivf;
}
add_shortcode( 'ivf', 'ivf_shortcode' );







/*
|--------------------------------------------------------------------------
| Creates the video player iframe for vimeo/youtube videos
|--------------------------------------------------------------------------
|
*/
function ivf_video($url,$width='100%',$height='100%') {
    $video_id = ivf_video_id($url);
    if(strpos($url, 'vimeo') !== false) {
        return '<!--<iframe src="http://player.vimeo.com/video/'.$video_id.'?autoplay=1" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>-->';
    } else if(strpos($url, 'youtube') !== false) {
        if(preg_match('/(?<=v=)\S+/', $url, $ids) == 1) $video_id = $ids[0];
        $video_id = substr($video_id, 0, 11);
        return '<!--<iframe width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$video_id.'?autoplay=1&color=white&theme=light&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>-->';
    } else {
        return false; //'<p style="color:#fff; padding:15px; background:#f00;">Video URL could not be processed</p>';
    }
}






/*
|--------------------------------------------------------------------------
| Retrives the video url based on the video URL
|--------------------------------------------------------------------------
|
| Function used by ivf_video() function
|
*/
function ivf_video_id($url) {
    if(strpos($url, 'vimeo') !== false) {
        //vimeo
        if(preg_match('/(?<=com\/)\w+/', $url, $ids) == 1) $video_id = $ids[0];
        return $video_id;
    } else if(strpos($url, 'youtube') !== false) {
        //youtube
        if(preg_match('/(?<=v=)\S+/', $url, $ids) == 1) $video_id = $ids[0];
        $video_id = substr($video_id, 0, 11);
        return $video_id;
    } else {
        return false;
    }
}
