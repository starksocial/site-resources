<?php
class ulp_front_class {
	function __construct() {
		if (is_admin()) {
		} else {
			$late_init = get_option('ulp_ext_late_init');
			add_action('init', array(&$this, 'init'), 15);
			if ($late_init == 'on') {
				add_action('wp_enqueue_scripts', array(&$this, 'front_init'), 99);
			} else {
				add_action('wp', array(&$this, 'front_init'), 15);
			}
			add_shortcode('ulp', array(&$this, "shortcode_handler"));
		}
	}

	function init() {
		global $wpdb, $ulp;
		if (isset($_GET['ulp-confirm'])) {
			$confirmation_id = preg_replace('/[^a-zA-Z0-9-]/', '', $_GET['ulp-confirm']);
			$message = __('Invalid confirmation URL.', 'ulp');
			if (!empty($confirmation_id)) {
				$subscriber_details = $wpdb->get_row("SELECT t1.*, t2.title AS popup_title FROM ".$wpdb->prefix."ulp_subscribers t1 LEFT JOIN ".$wpdb->prefix."ulp_popups t2 ON t2.id = t1.popup_id WHERE t1.confirmation_id = '".esc_sql($confirmation_id)."' AND t1.deleted = '0'", ARRAY_A);
				if ($subscriber_details) {
					$popup_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."ulp_popups WHERE deleted = '0' AND id = '".$subscriber_details['popup_id']."'", ARRAY_A);
					if (empty($popup_details)) {
						$message = __('Relevant popup not found.', 'ulp');
					} else {
						$popup_options = unserialize($popup_details['options']);
						if (is_array($popup_options)) $popup_options = array_merge($ulp->default_popup_options, $popup_options);
						else $popup_options = $ulp->default_popup_options;
						$subscriber = array(
							'{id}' => $subscriber_details['id'],
							'{name}' => $subscriber_details['name'], 
							'{email}' => $subscriber_details['email'], 
							'{e-mail}' => $subscriber_details['email'], 
							'{phone}' => $subscriber_details['phone'], 
							'{message}' => $subscriber_details['message'],
							'{subscription-name}' => $subscriber_details['name'], 
							'{subscription-email}' => $subscriber_details['email'], 
							'{subscription-phone}' => $subscriber_details['phone'], 
							'{subscription-message}' => $subscriber_details['message'],
							'{popup}' => $popup_options['title'],
							'{popup-id}' => $popup_details['str_id'],
							'{confirmation-link}' => get_bloginfo('url').'?ulp-confirm='.$confirmation_id
						);
						if (array_key_exists('custom_fields', $subscriber_details) && !empty($subscriber_details['custom_fields'])) {
							$custom_fields = unserialize($subscriber_details['custom_fields']);
							if (array_key_exists('ip', $custom_fields) && is_array($custom_fields['ip'])) $subscriber['{ip}'] = $custom_fields['ip']['value'];
							if (array_key_exists('url', $custom_fields) && is_array($custom_fields['url'])) $subscriber['{url}'] = $custom_fields['url']['value'];
							if (array_key_exists('agent', $custom_fields) && is_array($custom_fields['agent'])) $subscriber['{user-agent}'] = $custom_fields['agent']['value'];
							$subscriber = apply_filters('ulp_subscriber_details_from_log', $subscriber, $popup_options, $custom_fields);
						}
						$custom_fields['confirmed-ip'] = array('name' => 'Confirmed from IP', 'value' => $_SERVER['REMOTE_ADDR']);
						$custom_fields['confirmed-time'] = array('name' => 'Confirmed at', 'value' => date("Y-m-d H:i:s"));
						$wpdb->query("UPDATE ".$wpdb->prefix."ulp_subscribers SET status = '".ULP_SUBSCRIBER_CONFIRMED."', custom_fields = '".esc_sql(serialize($custom_fields))."' WHERE deleted = '0' AND id = '".esc_sql($subscriber_details['id'])."'");
						$message = strtr($popup_options['doubleoptin_confirmation_message'], $subscriber);
						do_action('ulp_subscribe', $popup_options, $subscriber);
						if (!empty($popup_options['doubleoptin_redirect_url'])) {
							$urlencoded = $subscriber;
							foreach ($urlencoded as $key => $value) {
								$urlencoded[$key] = urlencode($value);
							}
							$return_url = apply_filters('ulp_thankyou_url', $popup_options['doubleoptin_redirect_url'], $popup_options, $subscriber);
							$return_url = strtr($return_url, $urlencoded);
							header('Location: '.$return_url);
							exit;
						}
					}
				}
			}
			echo '<!DOCTYPE html>
<html>
<head>
	<meta name="robots" content="noindex, nofollow, noarchive, nosnippet">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>'.__('Confirmation', 'ulp').'</title>
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,cyrillic-ext,greek-ext,latin-ext,cyrillic,greek,vietnamese" rel="stylesheet" type="text/css">
	<style>
	body {font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif; font-weight: 100; color: #333; background-color: #fff; font-size: 24px; line-height: 1.475;}
	.front-container {position: absolute;top: 0;right: 0;bottom: 0;left: 0;min-width: 240px;height: 100%;display: table;width: 100%;}
	.front-content {max-width: 1024px;margin: 0px auto;padding: 20px 0;position: relative;display: table-cell;text-align: center;vertical-align: middle;}
	</style>
</head>
<body>
	<div class="front-container">
		<div class="front-content">
			'.$message.'
		</div>
	</div>
</body>
</html>';
			exit;
		}
	}

	function front_init() {
		global $wpdb, $post, $current_user, $ulp;
		
		$posts_page = false;
		$posts_page_id = get_option('page_for_posts');
		if (is_home() && !empty($posts_page_id)) $posts_page = true;
		
		$woo_page = false;
		$woo_page_id = 0;
		if (function_exists('is_product')) {
			if (is_product()) {
				$woo_page_id = $post->ID;
				$woo_page = true;
			}
		}
		if (function_exists('is_shop')) {
			if (is_shop() && function_exists('woocommerce_get_page_id')) {
				$woo_page_id = woocommerce_get_page_id('shop');
				$woo_page = true;
			}
		}
		
		if ($ulp->options['no_preload'] != 'on') {
			$this->get_popups();
		}
		
		$common_roles = array();
		if ($current_user) $common_roles = array_intersect($current_user->roles, $ulp->options['disable_roles']);

		$ulp->front_header .= '
		<script>
			var ulp_cookie_value = "'.$ulp->options['cookie_value'].'";
			var ulp_recaptcha_enable = "'.$ulp->options['recaptcha_enable'].'";';
		$ulp->front_header .= $ulp->options['recaptcha_enable'] != 'on' ? '' : '
			var ulp_recaptcha_public_key = "'.esc_html($ulp->options['recaptcha_public_key']).'";';
		if (is_singular() || $posts_page || $woo_page) {
			if ($posts_page) $meta = $ulp->get_meta($posts_page_id);
			else if ($woo_page) $meta = $ulp->get_meta($woo_page_id);
			else $meta = $ulp->get_meta($post->ID);
			
			$onload_popup = ($meta['onload_popup'] == 'default' ? $ulp->options['onload_popup'] : $meta['onload_popup']);
			$onload_popup_mobile = ($meta['onload_popup_mobile'] == 'default' ? $ulp->options['onload_popup_mobile'] : $meta['onload_popup_mobile']);
			if ($onload_popup_mobile != 'same' && (!empty($onload_popup) || !empty($onload_popup_mobile))) $onload_popup .= '*'.$onload_popup_mobile;

			$onscroll_popup = ($meta['onscroll_popup'] == 'default' ? $ulp->options['onscroll_popup'] : $meta['onscroll_popup']);
			$onscroll_popup_mobile = ($meta['onscroll_popup_mobile'] == 'default' ? $ulp->options['onscroll_popup_mobile'] : $meta['onscroll_popup_mobile']);
			if ($onscroll_popup_mobile != 'same' && (!empty($onscroll_popup) || !empty($onscroll_popup_mobile))) $onscroll_popup .= '*'.$onscroll_popup_mobile;

			$onexit_popup = ($meta['onexit_popup'] == 'default' ? $ulp->options['onexit_popup'] : $meta['onexit_popup']);
			$onexit_popup_mobile = ($meta['onexit_popup_mobile'] == 'default' ? $ulp->options['onexit_popup_mobile'] : $meta['onexit_popup_mobile']);
			if ($onexit_popup_mobile != 'same' && (!empty($onexit_popup) || !empty($onexit_popup_mobile))) $onexit_popup .= '*'.$onexit_popup_mobile;

			$onidle_popup = ($meta['onidle_popup'] == 'default' ? $ulp->options['onidle_popup'] : $meta['onidle_popup']);
			$onidle_popup_mobile = ($meta['onidle_popup_mobile'] == 'default' ? $ulp->options['onidle_popup_mobile'] : $meta['onidle_popup_mobile']);
			if ($onidle_popup_mobile != 'same' && (!empty($onidle_popup) || !empty($onidle_popup_mobile))) $onidle_popup .= '*'.$onidle_popup_mobile;
			
			if (!empty($common_roles)) {
				$ulp->options['onload_mode'] = 'none';
				$ulp->options['onexit_mode'] = 'none';
				$ulp->options['onscroll_mode'] = 'none';
				$ulp->options['onidle_mode'] = 'none';
				$meta['onload_mode'] = 'none';
				$meta['onexit_mode'] = 'none';
				$meta['onscroll_mode'] = 'none';
				$meta['onidle_mode'] = 'none';
			}
			
			$ulp->front_header .= '
			var ulp_onload_mode = "'.($meta['onload_mode'] == 'default' ? $ulp->options['onload_mode'] : $meta['onload_mode']).'";
			var ulp_onload_period = "'.($meta['onload_mode'] == 'default' ? intval($ulp->options['onload_period']) : intval($meta['onload_period'])).'";
			var ulp_onload_popup = "'.$onload_popup.'";
			var ulp_onload_delay = "'.($meta['onload_popup'] == 'default' ? intval($ulp->options['onload_delay']) : intval($meta['onload_delay'])).'";
			var ulp_onload_close_delay = "'.($meta['onload_popup'] == 'default' ? intval($ulp->options['onload_close_delay']) : intval($meta['onload_close_delay'])).'";
			var ulp_onexit_mode = "'.($meta['onexit_mode'] == 'default' ? $ulp->options['onexit_mode'] : $meta['onexit_mode']).'";
			var ulp_onexit_period = "'.($meta['onexit_mode'] == 'default' ? intval($ulp->options['onexit_period']) : intval($meta['onexit_period'])).'";
			var ulp_onexit_popup = "'.$onexit_popup.'";
			var ulp_onscroll_mode = "'.($meta['onscroll_mode'] == 'default' ? $ulp->options['onscroll_mode'] : $meta['onscroll_mode']).'";
			var ulp_onscroll_period = "'.($meta['onscroll_mode'] == 'default' ? $ulp->options['onscroll_period'] : $meta['onscroll_period']).'";
			var ulp_onscroll_popup = "'.$onscroll_popup.'";
			var ulp_onscroll_offset = "'.($meta['onscroll_popup'] == 'default' ? intval($ulp->options['onscroll_offset']) : intval($meta['onscroll_offset'])).'";
			var ulp_onidle_mode = "'.($meta['onidle_mode'] == 'default' ? $ulp->options['onidle_mode'] : $meta['onidle_mode']).'";
			var ulp_onidle_period = "'.($meta['onidle_mode'] == 'default' ? $ulp->options['onidle_period'] : $meta['onidle_period']).'";
			var ulp_onidle_popup = "'.$onidle_popup.'";
			var ulp_onidle_delay = "'.($meta['onidle_popup'] == 'default' ? intval($ulp->options['onidle_delay']) : intval($meta['onidle_delay'])).'";';
		} else {
			$onload_popup = $ulp->options['onload_popup'];
			if ($ulp->options['onload_popup_mobile'] != 'same' && (!empty($onload_popup) || !empty($ulp->options['onload_popup_mobile']))) $onload_popup .= '*'.$ulp->options['onload_popup_mobile'];

			$onscroll_popup = $ulp->options['onscroll_popup'];
			if ($ulp->options['onscroll_popup_mobile'] != 'same' && (!empty($onscroll_popup) || !empty($ulp->options['onscroll_popup_mobile']))) $onscroll_popup .= '*'.$ulp->options['onscroll_popup_mobile'];

			$onexit_popup = $ulp->options['onexit_popup'];
			if ($ulp->options['onexit_popup_mobile'] != 'same' && (!empty($onexit_popup) || !empty($ulp->options['onexit_popup_mobile']))) $onexit_popup .= '*'.$ulp->options['onexit_popup_mobile'];

			$onidle_popup = $ulp->options['onidle_popup'];
			if ($ulp->options['onidle_popup_mobile'] != 'same' && (!empty($onidle_popup) || !empty($ulp->options['onidle_popup_mobile']))) $onidle_popup .= '*'.$ulp->options['onidle_popup_mobile'];

			if (!empty($common_roles)) {
				$ulp->options['onload_mode'] = 'none';
				$ulp->options['onexit_mode'] = 'none';
				$ulp->options['onscroll_mode'] = 'none';
				$ulp->options['onidle_mode'] = 'none';
			}
			
			$ulp->front_header .= '
			var ulp_onload_mode = "'.$ulp->options['onload_mode'].'";
			var ulp_onload_period = "'.intval($ulp->options['onload_period']).'";
			var ulp_onload_popup = "'.$onload_popup.'";
			var ulp_onload_delay = "'.intval($ulp->options['onload_delay']).'";
			var ulp_onload_close_delay = "'.intval($ulp->options['onload_close_delay']).'";
			var ulp_onexit_mode = "'.$ulp->options['onexit_mode'].'";
			var ulp_onexit_period = "'.intval($ulp->options['onexit_period']).'";
			var ulp_onexit_popup = "'.$onexit_popup.'";
			var ulp_onscroll_mode = "'.$ulp->options['onscroll_mode'].'";
			var ulp_onscroll_period = "'.intval($ulp->options['onscroll_period']).'";
			var ulp_onscroll_popup = "'.$onscroll_popup.'";
			var ulp_onscroll_offset = "'.intval($ulp->options['onscroll_offset']).'";
			var ulp_onidle_mode = "'.$ulp->options['onidle_mode'].'";
			var ulp_onidle_period = "'.intval($ulp->options['onidle_period']).'";
			var ulp_onidle_popup = "'.$onidle_popup.'";
			var ulp_onidle_delay = "'.intval($ulp->options['onidle_delay']).'";';
		}
		$ulp->front_header .= '
		</script>';
		
		$ulp->front_footer .= '
		<script>
			var ulp_ajax_url = "'.admin_url('admin-ajax.php').'";
			var ulp_css3_enable = "'.$ulp->options['css3_enable'].'";
			var ulp_ga_tracking = "'.$ulp->options['ga_tracking'].'";
			var ulp_km_tracking = "'.$ulp->options['km_tracking'].'";
			var ulp_onexit_limits = "'.$ulp->options['onexit_limits'].'";
			var ulp_no_preload = "'.$ulp->options['no_preload'].'";
			var ulp_campaigns = {';
		$campaigns = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_campaigns WHERE deleted = '0' AND blocked = '0' ORDER BY created DESC", ARRAY_A);
		foreach ($campaigns as $campaign) {
			$popups = $wpdb->get_results("SELECT t1.*, t2.str_id FROM ".$wpdb->prefix."ulp_campaign_items t1 JOIN ".$wpdb->prefix."ulp_popups t2 ON t2.id = t1.popup_id WHERE t1.campaign_id = '".$campaign['id']."' AND t1.deleted = '0' AND t2.deleted = '0' AND t2.blocked = '0' ORDER BY t1.created DESC", ARRAY_A);
			$campaign_popups = array();
			foreach($popups as $popup) {
				$campaign_popups[] = $popup['str_id'];
			}
			$ulp->front_footer .= '"'.$campaign['str_id'].'":["'.implode('","', $campaign_popups).'"],';
		}
		$ulp->front_footer .= '"none":[""]};';
		$popups = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_popups WHERE deleted = '0' AND blocked = '0'", ARRAY_A);
		$ulp->front_footer .= '
			var ulp_overlays = {';
		foreach ($popups as $popup) {
			$popup_options = unserialize($popup['options']);
			if (is_array($popup_options)) $popup_options = array_merge($ulp->default_popup_options, $popup_options);
			else $popup_options = $ulp->default_popup_options;
			$ulp->front_footer .= '"'.$popup['str_id'].'":["'.($popup_options['disable_overlay'] == 'on' ? '' : (!empty($popup_options['overlay_color']) ? $popup_options['overlay_color'] : 'transparent')).'", "'.$popup_options['overlay_opacity'].'", "'.$popup_options['enable_close'].'", "'.$popup_options['position'].'"],';
		}
		$ulp->front_footer .= '"none":["", "", "", ""]};';

		if (isset($_GET['ulp'])) {
			$str_id = preg_replace('/[^a-zA-Z0-9-]/', '', $_GET['ulp']);
			$ulp->front_footer .= '
			if (typeof ulp_inline_open == "function" && typeof ulp_open == "function") { 
				ulp_prepare_ids(); ulp_inline_open(false); ulp_open("'.$str_id.'"); jQuery(document).ready(function() {ulp_ready();});
			} else {
				jQuery(document).ready(function(){ulp_prepare_ids(); ulp_inline_open(false); ulp_ready(); ulp_open("'.$str_id.'");});
			}';
		} else {
			$ulp->front_footer .= '
			if (typeof ulp_inline_open == "function" && typeof ulp_init == "function") { 
				ulp_prepare_ids(); ulp_inline_open(false); ulp_init(); jQuery(document).ready(function() {ulp_ready();});
			} else {
				jQuery(document).ready(function(){ulp_prepare_ids(); ulp_inline_open(false); ulp_init(); ulp_ready();});
			}';
		}
		$ulp->front_footer .= '
		</script>';
		if ($ulp->ext_options['late_init'] == 'on') {
			$this->front_enqueue_scripts();
		} else {
			add_action('wp_enqueue_scripts', array(&$this, 'front_enqueue_scripts'), 99);
		}
		add_action('wp_head', array(&$this, 'front_header'), 15);
		add_action('wp_footer', array(&$this, 'front_footer'), 999);
	}

	static function get_popups($_popup_id = '', $_add_overlay = true) {
		global $wpdb, $post, $ulp;
		
		$posts_page = false;
		$posts_page_id = get_option('page_for_posts');
		if (is_home() && !empty($posts_page_id)) $posts_page = true;

		$woo_page = false;
		$woo_page_id = 0;
		if (function_exists('is_product')) {
			if (is_product()) {
				$woo_page_id = $post->ID;
				$woo_page = true;
			}
		}
		if (function_exists('is_shop')) {
			if (is_shop() && function_exists('woocommerce_get_page_id')) {
				$woo_page_id = woocommerce_get_page_id('shop');
				$woo_page = true;
			}
		}
		
		$layer_webfonts = array();
		$style = '';
		unset($str_id);
		$filtered = array();
		if (!empty($_popup_id)) {
			$_popup_id = preg_replace('/[^a-zA-Z0-9-]/', '', $_popup_id);
			$filtered[] = $_popup_id;
		} else if (isset($_GET['ulp'])) {
			$str_id = preg_replace('/[^a-zA-Z0-9-]/', '', $_GET['ulp']);
			if (substr($str_id, 0, 3) == 'ab-') {
				$sql = "SELECT t1.*, t2.str_id FROM ".$wpdb->prefix."ulp_campaign_items t1 JOIN ".$wpdb->prefix."ulp_popups t2 ON t2.id = t1.popup_id JOIN ".$wpdb->prefix."ulp_campaigns t3 ON t3.id = t1.campaign_id WHERE t2.deleted = '0' AND t2.blocked = '0' AND t3.deleted = '0' AND t3.blocked = '0' AND t3.str_id = '".$str_id."' AND t1.deleted = '0'";
				$rows = $wpdb->get_results($sql, ARRAY_A);
				if (sizeof($rows) > 0) {
					foreach ($rows as $row) {
						$filtered[] = $row['str_id'];
					}
				} else $filtered[] = 'none';
			} else $filtered[] = $str_id;
		} else if ($ulp->options['onload_only'] == 'on') {
			if (is_singular() || $posts_page || $woo_page) {
				if ($posts_page) $meta = $ulp->get_meta($posts_page_id);
				else if ($woo_page) $meta = $ulp->get_meta($woo_page_id);
				else $meta = $ulp->get_meta($post->ID);
				$filtered[] = $meta['onload_popup'] == 'default' ? $ulp->options['onload_popup'] : $meta['onload_popup'];
				$filtered[] = $meta['onexit_popup'] == 'default' ? $ulp->options['onexit_popup'] : $meta['onexit_popup'];
				$filtered[] = $meta['onscroll_popup'] == 'default' ? $ulp->options['onscroll_popup'] : $meta['onscroll_popup'];
				$filtered[] = $meta['onidle_popup'] == 'default' ? $ulp->options['onidle_popup'] : $meta['onidle_popup'];
			} else {
				$filtered[] = $ulp->options['onload_popup'];
				$filtered[] = $ulp->options['onexit_popup'];
				$filtered[] = $ulp->options['onscroll_popup'];
				$filtered[] = $ulp->options['onidle_popup'];
			}
			$sql = "SELECT t1.*, t2.str_id FROM ".$wpdb->prefix."ulp_campaign_items t1 JOIN ".$wpdb->prefix."ulp_popups t2 ON t2.id = t1.popup_id JOIN ".$wpdb->prefix."ulp_campaigns t3 ON t3.id = t1.campaign_id WHERE t2.deleted = '0' AND t2.blocked = '0' AND t3.deleted = '0' AND t3.blocked = '0' AND t3.str_id IN ('".implode("', '", $filtered)."') AND t1.deleted = '0'";
			$rows = $wpdb->get_results($sql, ARRAY_A);
			if (sizeof($rows) > 0) {
				foreach ($rows as $row) {
					$filtered[] = $row['str_id'];
				}
			}
		}
		
		$popups = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_popups WHERE deleted = '0'".(isset($str_id) ? "" : " AND blocked = '0'").(!empty($filtered) ? " AND str_id IN ('".implode("', '", $filtered)."')" : ""), ARRAY_A);
		foreach ($popups as $popup) {
			$popup_options = unserialize($popup['options']);
			if (is_array($popup_options)) $popup_options = array_merge($ulp->default_popup_options, $popup_options);
			else $popup_options = $ulp->default_popup_options;
			$from = $ulp->get_rgb($popup_options['button_color']);
			$total = $from['r']+$from['g']+$from['b'];
			if ($total == 0) $total = 1;
			$to = array();
			$to['r'] = max(0, $from['r']-intval(48*$from['r']/$total));
			$to['g'] = max(0, $from['g']-intval(48*$from['g']/$total));
			$to['b'] = max(0, $from['b']-intval(48*$from['b']/$total));
			$to_color = '#'.($to['r'] < 16 ? '0' : '').dechex($to['r']).($to['g'] < 16 ? '0' : '').dechex($to['g']).($to['b'] < 16 ? '0' : '').dechex($to['b']);
			$from_color = $popup_options['button_color'];
			if (!empty($popup_options['input_background_color'])) $bg_color = $ulp->get_rgb($popup_options['input_background_color']);
			if ($popup_options['button_gradient'] == 'on') {
				$style .= '#ulp-'.$popup['str_id'].' .ulp-submit,#ulp-'.$popup['str_id'].' .ulp-submit:visited{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$from_color.';border:1px solid '.$from_color.';background-image:linear-gradient('.$to_color.','.$from_color.');'.(!empty($popup_options['button_css']) ? $popup_options['button_css'] : '').'}';
				$style .= '#ulp-'.$popup['str_id'].' .ulp-submit:hover,#ulp-'.$popup['str_id'].' .ulp-submit:active{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$to_color.';border:1px solid '.$from_color.';background-image:linear-gradient('.$from_color.','.$to_color.');'.(!empty($popup_options['button_css_hover']) ? $popup_options['button_css_hover'] : '').'}';
			} else {
				$style .= '#ulp-'.$popup['str_id'].' .ulp-submit,#ulp-'.$popup['str_id'].' .ulp-submit:visited{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$from_color.';border:1px solid '.$from_color.';'.(!empty($popup_options['button_css']) ? $popup_options['button_css'] : '').'}';
				$style .= '#ulp-'.$popup['str_id'].' .ulp-submit:hover,#ulp-'.$popup['str_id'].' .ulp-submit:active{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$to_color.';border:1px solid '.$to_color.';'.(!empty($popup_options['button_css_hover']) ? $popup_options['button_css_hover'] : '').'}';
			}
			$style .= '#ulp-'.$popup['str_id'].' .ulp-input,#ulp-'.$popup['str_id'].' .ulp-input:hover,#ulp-'.$popup['str_id'].' .ulp-input:active,#ulp-'.$popup['str_id'].' .ulp-input:focus,#ulp-'.$popup['str_id'].' .ulp-checkbox{border-width: '.intval($popup_options['input_border_width']).'px !important; border-radius: '.intval($popup_options['input_border_radius']).'px !important; border-color:'.(empty($popup_options['input_border_color']) ? 'transparent' : $popup_options['input_border_color']).';background-color:'.(empty($popup_options['input_background_color']) ? 'transparent' : $popup_options['input_background_color']).' !important;background-color:'.(empty($popup_options['input_background_color']) ? 'transparent' : 'rgba('.$bg_color['r'].','.$bg_color['g'].','.$bg_color['b'].','.floatval($popup_options['input_background_opacity'])).') !important;'.(!empty($popup_options['input_css']) ? $popup_options['input_css'] : '').'}';
			if ($_add_overlay && $popup_options['disable_overlay'] != 'on') $style .= '#ulp-'.$popup['str_id'].'-overlay{background:'.(!empty($popup_options['overlay_color']) ? $popup_options['overlay_color'] : 'transparent').';opacity:'.$popup_options['overlay_opacity'].';-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=\''.intval(100*$popup_options['overlay_opacity']).'\')";filter:alpha(opacity="'.intval(100*$popup_options['overlay_opacity']).'");}';
			
			$style = apply_filters('ulp_front_popup_style', $style, $popup);
			
			if ($_add_overlay && $popup_options['disable_overlay'] != 'on') {
				$ulp->front_footer .= '
				<div class="ulp-overlay" id="ulp-'.$popup['str_id'].'-overlay"></div>';
			}
			$ulp->front_footer .= '
				<div class="ulp-window" id="ulp-'.$popup['str_id'].'" data-title="'.esc_html($popup['title']).'" data-width="'.$popup_options['width'].'" data-height="'.$popup_options['height'].'" data-close="'.$popup_options['enable_close'].'" data-position="'.$popup_options['position'].'">
					<div class="ulp-content">';
			$layers = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_layers WHERE popup_id = '".$popup['id']."' AND deleted = '0'", ARRAY_A);
			foreach ($layers as $layer) {
				$layer_options = unserialize($layer['details']);
				if (is_array($layer_options)) $layer_options = array_merge($ulp->default_layer_options, $layer_options);
				else $layer_options = $ulp->default_layer_options;
				$layer_options = $ulp->filter_lp($layer_options);
				
				$mask_class = '';
				$mask = '';
				if ($ulp->options['mask_enable'] == 'on') {
					$mask_class = $popup_options['phone_mask'] != 'none' ? ' ulp-input-mask' : '';
					if ($popup_options['phone_mask'] != 'none') $mask = ' data-mask="'.esc_html($popup_options['phone_mask'] != 'custom' ? $popup_options['phone_mask'] : $popup_options['phone_custom_mask']).'"';
				}
				$content = str_replace(
					array('{subscription-name}', '{subscription-email}', '{subscription-phone}', '{subscription-message}', '{subscription-submit}'),
					array(
						'<input class="ulp-input ulp-input-field" type="text" name="ulp-name" placeholder="'.esc_html($popup_options['name_placeholder']).'" value="" onfocus="jQuery(this).removeClass(\'ulp-input-error\');">'.($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on' ? '<div class="ulp-fa-input-table"><div class="ulp-fa-input-cell"><i class="fa fa-user"></i></div></div>' : ''),
						'<input class="ulp-input ulp-input-field" type="text" name="ulp-email" placeholder="'.esc_html($popup_options['email_placeholder']).'" value="" onfocus="jQuery(this).removeClass(\'ulp-input-error\');">'.($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on' ? '<div class="ulp-fa-input-table"><div class="ulp-fa-input-cell"><i class="fa fa-envelope"></i></div></div>' : ''),
						'<input class="ulp-input ulp-input-field'.$mask_class.'"'.$mask.'" type="text" name="ulp-phone" placeholder="'.esc_html($popup_options['phone_placeholder']).'" value="" onfocus="jQuery(this).removeClass(\'ulp-input-error\');">'.($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on' ? '<div class="ulp-fa-input-table"><div class="ulp-fa-input-cell"><i class="fa fa-phone"></i></div></div>' : ''),
						'<textarea class="ulp-input ulp-input-field" name="ulp-message" placeholder="'.esc_html($popup_options['message_placeholder']).'" onfocus="jQuery(this).removeClass(\'ulp-input-error\');"></textarea>',
						'<a class="ulp-submit'.($popup_options['button_inherit_size'] == 'on' ? ' ulp-inherited' : '').'" onclick="return ulp_subscribe(this);"'.($ulp->options['fa_enable'] == 'on' && !empty($popup_options['button_icon']) && $popup_options['button_icon'] != 'fa-noicon' ? ' data-icon="'.$popup_options['button_icon'].'"' : '').' data-label="'.esc_html($popup_options['button_label']).'" data-loading="'.esc_html($popup_options['button_label_loading']).'">'.($ulp->options['fa_enable'] == 'on' && !empty($popup_options['button_icon']) && $popup_options['button_icon'] != 'fa-noicon' ? '<i class="fa '.$popup_options['button_icon'].'"></i>&nbsp; ' : '').esc_html($popup_options['button_label']).'</a>'),
					$layer_options['content']);
				if ($ulp->options['recaptcha_enable'] == 'on') {
					$recaptcha_id = 'ulp-recaptcha-'.$ulp->random_string(8);
					$content = str_replace('{recaptcha}', '<div class="ulp-recaptcha" id="'.$recaptcha_id.'" name="'.$recaptcha_id.'" data-theme="'.esc_html($popup_options['recaptcha_theme']).'"></div>', $content);
				}
				$content = apply_filters('ulp_front_popup_content', $content, $popup_options);
				$content = do_shortcode($content);
				$base64 = false;
				if (strpos(strtolower($content), '<iframe') !== false || strpos(strtolower($content), '<video') !== false || strpos(strtolower($content), '<audio') !== false) {
					$base64 = true;
					$content = base64_encode($content);
				}
				$ulp->front_footer .= '
						<div class="ulp-layer" id="ulp-layer-'.$layer['id'].'" data-left="'.$layer_options['left'].'" data-top="'.$layer_options['top'].'" data-appearance="'.$layer_options['appearance'].'" data-appearance-speed="'.$layer_options['appearance_speed'].'" data-appearance-delay="'.$layer_options['appearance_delay'].'"'.(!empty($layer_options['width']) ? ' data-width="'.$layer_options['width'].'"' : '').(!empty($layer_options['height']) ? ' data-height="'.$layer_options['height'].'"' : '').' data-font-size="'.$layer_options['font_size'].'"'.($base64 ? ' data-base64="yes"' : '').' '.(!empty($layer_options['scrollbar']) ? ' data-scrollbar="'.$layer_options['scrollbar'].'"' : ' data-scrollbar="off"').(!empty($layer_options['confirmation_layer']) ? ' data-confirmation="'.$layer_options['confirmation_layer'].'"' : ' data-confirmation="off"').'>'.$content.'</div>';
				if (!empty($layer_options['background_color'])) {
					$rgb = $ulp->get_rgb($layer_options['background_color']);
					$background = 'background-color:'.$layer_options['background_color'].';background-color:rgba('.$rgb['r'].','.$rgb['g'].','.$rgb['b'].','.$layer_options['background_opacity'].');';
				} else $background = '';
				if (!empty($layer_options['background_image'])) {
					$background .= 'background-image:url('.$layer_options['background_image'].');background-repeat:repeat;';
				}
				
				$font = ($layer_options['text_shadow_size'] > 0 && !empty($layer_options['text_shadow_color']) ? "text-shadow: ".$layer_options['text_shadow_color']." ".$layer_options['text_shadow_size']."px ".$layer_options['text_shadow_size']."px ".$layer_options['text_shadow_size']."px;" : "");
				if ($layer_options['font'] != 'inherit') $font .= "font-family:'".$layer_options['font']."',arial;";
				if ($layer_options['font_weight'] != 'inherit') $font .= "font-weight:".$layer_options['font_weight'].";";
				if ($layer_options['font_color'] != '') $font .= "color:".$layer_options['font_color'].";";
				
				$style .= '#ulp-layer-'.$layer['id'].',#ulp-layer-'.$layer['id'].' p,#ulp-layer-'.$layer['id'].' a,#ulp-layer-'.$layer['id'].' span,#ulp-layer-'.$layer['id'].' li,#ulp-layer-'.$layer['id'].' input,#ulp-layer-'.$layer['id'].' button,#ulp-layer-'.$layer['id'].' textarea,#ulp-layer-'.$layer['id'].' select {'.$font.'}';
				$style .= '#ulp-layer-'.$layer['id'].' .ulp-checkbox label:after{background:'.$layer_options['font_color'].'}';
				if ($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on') {
					$style .= '#ulp-layer-'.$layer['id'].' input.ulp-input, #ulp-layer-'.$layer['id'].' select.ulp-input {padding-left: '.intval(4+2*$layer_options['font_size']).'px !important;} #ulp-layer-'.$layer['id'].' div.ulp-fa-input-cell {width: '.intval(2*$layer_options['font_size']).'px !important; padding-left: 4px !important;}';
				}
				$style .= '#ulp-layer-'.$layer['id'].'{'.$background.'z-index:'.($layer_options['index']+1000002).';text-align:'.$layer_options['content_align'].';'.$layer_options['style'].'}';
				if (!array_key_exists($layer_options['font'], $ulp->local_fonts)) $layer_webfonts[] = $layer_options['font'];
			}
			$ulp->front_footer .= '
					</div>
				</div>';
		}
		if (!empty($layer_webfonts)) {
			$layer_webfonts = array_unique($layer_webfonts);
			if ($ulp->options['version'] >= 4.58) {
				$webfonts_array = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_webfonts WHERE family IN ('".implode("', '", $layer_webfonts)."') AND deleted = '0' ORDER BY family", ARRAY_A);
			} else $webfonts_array = array();
			if(!empty($webfonts_array)){
				$families = array();
				$subsets = array();
				foreach($webfonts_array as $webfont) {
					$families[] = str_replace(' ', '+', $webfont['family']).':'.$webfont['variants'];
					$webfont_subsets = explode(',', $webfont['subsets']);
					if (!empty($webfont_subsets) && is_array($webfont_subsets)) $subsets = array_merge($subsets, $webfont_subsets);
				}
				$subsets = array_unique($subsets);
				$query = '?family='.implode('|', $families);
				if (!empty($subsets)) $query .= '&subset='.implode(',', $subsets);
				$ulp->front_header .= '<link href="//fonts.googleapis.com/css'.$query.'" rel="stylesheet" type="text/css">';
			}
		}
		$ulp->front_header .= '<style>'.$style.'</style>';
	}

	function front_enqueue_scripts() {
		global $ulp;
		wp_enqueue_script("jquery");
		if ($ulp->ext_options['minified_sources'] == 'on') {
			wp_enqueue_style('ulp', $ulp->plugins_url.'/css/style.min.css', array(), ULP_VERSION);
			wp_enqueue_style('ulp-link-buttons', $ulp->plugins_url.'/css/link-buttons.min.css', array(), ULP_VERSION);
			wp_enqueue_script('ulp', $ulp->plugins_url.'/js/script.min.js', array('jquery'), ULP_VERSION, true);
			if ($ulp->options['fa_enable'] == 'on' && $ulp->options['fa_css_disable'] != 'on') wp_enqueue_style('font-awesome', $ulp->plugins_url.'/css/font-awesome.min.css', array(), ULP_VERSION);
			wp_enqueue_style('perfect-scrollbar', $ulp->plugins_url.'/css/perfect-scrollbar-0.4.6.min.css', array(), ULP_VERSION);
			wp_enqueue_script('perfect-scrollbar', $ulp->plugins_url.'/js/perfect-scrollbar-0.4.6.with-mousewheel.min.js', array('ulp'), ULP_VERSION, true);
			if ($ulp->options['mask_enable'] == 'on' && $ulp->options['mask_js_disable'] != 'on') wp_enqueue_script('jquery-mask', $ulp->plugins_url.'/js/jquery.mask.min.js', array('ulp'), ULP_VERSION, true);
			if ($ulp->options['css3_enable'] == 'on') wp_enqueue_style('animate.css', $ulp->plugins_url.'/css/animate.min.css', array(), ULP_VERSION);
		} else {
			wp_enqueue_style('ulp', $ulp->plugins_url.'/css/style.css', array(), ULP_VERSION);
			wp_enqueue_style('ulp-link-buttons', $ulp->plugins_url.'/css/link-buttons.css', array(), ULP_VERSION);
			wp_enqueue_script('ulp', $ulp->plugins_url.'/js/script.js', array('jquery'), ULP_VERSION, true);
			if ($ulp->options['fa_enable'] == 'on' && $ulp->options['fa_css_disable'] != 'on') wp_enqueue_style('font-awesome', $ulp->plugins_url.'/css/font-awesome.css', array(), ULP_VERSION);
			wp_enqueue_style('perfect-scrollbar', $ulp->plugins_url.'/css/perfect-scrollbar.css', array(), ULP_VERSION);
			wp_enqueue_script('perfect-scrollbar', $ulp->plugins_url.'/js/perfect-scrollbar-0.4.6.with-mousewheel.min.js', array('ulp'), ULP_VERSION, true);
			if ($ulp->options['mask_enable'] == 'on' && $ulp->options['mask_js_disable'] != 'on') wp_enqueue_script('jquery-mask', $ulp->plugins_url.'/js/jquery.mask.js', array('ulp'), ULP_VERSION, true);
			if ($ulp->options['css3_enable'] == 'on') wp_enqueue_style('animate.css', $ulp->plugins_url.'/css/animate.css', array(), ULP_VERSION);
		}
		if ($ulp->options['recaptcha_enable'] == 'on' && $ulp->options['recaptcha_js_disable'] != 'on') {
			wp_deregister_script('recaptcha');
			wp_register_script('recaptcha', 'https://www.google.com/recaptcha/api.js?onload=ulp_recaptcha_loaded&render=explicit&hl=en');
			wp_enqueue_script("recaptcha", true, array(), ULP_VERSION, false);
		}
	}
	
	function front_header() {
		global $wpdb, $ulp;
		echo $ulp->front_header;
	}

	function front_footer() {
		global $wpdb, $ulp;
		echo $ulp->front_footer;
	}

	function shortcode_handler($_atts) {
		global $post, $wpdb, $ulp;
		$html = '';
		$layer_webfonts = array();
		$style = '';
		//if ($ulp->check_options() === true) {
			if (isset($_atts['id'])) {
				$str_id = $_atts["id"];
				$popup = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."ulp_popups WHERE deleted = '0' AND str_id = '".esc_sql($str_id)."' AND blocked = '0'", ARRAY_A);
				if ($popup) {
					$popup_options = unserialize($popup['options']);
					if (is_array($popup_options)) $popup_options = array_merge($ulp->default_popup_options, $popup_options);
					else $popup_options = $ulp->default_popup_options;
					$from = $ulp->get_rgb($popup_options['button_color']);
					$total = $from['r']+$from['g']+$from['b'];
					if ($total == 0) $total = 1;
					$to = array();
					$to['r'] = max(0, $from['r']-intval(48*$from['r']/$total));
					$to['g'] = max(0, $from['g']-intval(48*$from['g']/$total));
					$to['b'] = max(0, $from['b']-intval(48*$from['b']/$total));
					$to_color = '#'.($to['r'] < 16 ? '0' : '').dechex($to['r']).($to['g'] < 16 ? '0' : '').dechex($to['g']).($to['b'] < 16 ? '0' : '').dechex($to['b']);
					$from_color = $popup_options['button_color'];
					if (isset($_atts['inline_id'])) $inline_id = $_atts['inline_id'];
					else $inline_id = $ulp->random_string();
					if (!empty($popup_options['input_background_color'])) $bg_color = $ulp->get_rgb($popup_options['input_background_color']);
					if ($popup_options['button_gradient'] == 'on') {
						$style .= '#ulp-inline-'.$inline_id.' .ulp-submit,#ulp-inline-'.$inline_id.' .ulp-submit:visited{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$from_color.';border:1px solid '.$from_color.';background-image:linear-gradient('.$to_color.','.$from_color.');'.(!empty($popup_options['button_css']) ? $popup_options['button_css'] : '').'}';
						$style .= '#ulp-inline-'.$inline_id.' .ulp-submit:hover,#ulp-inline-'.$inline_id.' .ulp-submit:active{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$to_color.';border:1px solid '.$from_color.';background-image:linear-gradient('.$from_color.','.$to_color.');'.(!empty($popup_options['button_css_hover']) ? $popup_options['button_css_hover'] : '').'}';
					} else {
						$style .= '#ulp-inline-'.$inline_id.' .ulp-submit,#ulp-inline-'.$inline_id.' .ulp-submit:visited{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$from_color.';border:1px solid '.$from_color.';'.(!empty($popup_options['button_css']) ? $popup_options['button_css'] : '').'}';
						$style .= '#ulp-inline-'.$inline_id.' .ulp-submit:hover,#ulp-inline-'.$inline_id.' .ulp-submit:active{border-radius: '.intval($popup_options['button_border_radius']).'px !important; background: '.$to_color.';border:1px solid '.$to_color.';'.(!empty($popup_options['button_css_hover']) ? $popup_options['button_css_hover'] : '').'}';
					}
					$style .= '#ulp-inline-'.$inline_id.' .ulp-input,#ulp-inline-'.$inline_id.' .ulp-input:hover,#ulp-inline-'.$inline_id.' .ulp-input:active,#ulp-inline-'.$inline_id.' .ulp-input:focus,#ulp-inline-'.$inline_id.' .ulp-checkbox{border-width: '.intval($popup_options['input_border_width']).'px !important; border-radius: '.intval($popup_options['input_border_radius']).'px !important; border-color:'.(empty($popup_options['input_border_color']) ? 'transparent' : $popup_options['input_border_color']).';background-color:'.(empty($popup_options['input_background_color']) ? 'transparent' : $popup_options['input_background_color']).' !important;background-color:'.(empty($popup_options['input_background_color']) ? 'transparent' : 'rgba('.$bg_color['r'].','.$bg_color['g'].','.$bg_color['b'].','.floatval($popup_options['input_background_opacity'])).') !important;'.(!empty($popup_options['input_css']) ? $popup_options['input_css'] : '').'}';
			
					$style = apply_filters('ulp_front_inline_style', $style, $inline_id, $popup);

					$html = '
						<div class="ulp-inline-window" id="ulp-inline-'.$inline_id.'" data-id="'.$popup['str_id'].'" data-title="'.esc_html($popup['title']).'" data-width="'.$popup_options['width'].'" data-height="'.$popup_options['height'].'" data-close="'.$popup_options['enable_close'].'">
							<div class="ulp-content">';
					$layers = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_layers WHERE popup_id = '".$popup['id']."' AND deleted = '0'", ARRAY_A);
					foreach ($layers as $layer) {
						$layer_options = unserialize($layer['details']);
						if (is_array($layer_options)) $layer_options = array_merge($ulp->default_layer_options, $layer_options);
						else $layer_options = $ulp->default_layer_options;
						$layer_options = $ulp->filter_lp($layer_options);
						if ($layer_options['inline_disable'] == 'on') continue;
						$mask_class = '';
						$mask = '';
						if ($ulp->options['mask_enable'] == 'on') {
							$mask_class = $popup_options['phone_mask'] != 'none' ? ' ulp-input-mask' : '';
							if ($popup_options['phone_mask'] != 'none') $mask = ' data-mask="'.esc_html($popup_options['phone_mask'] != 'custom' ? $popup_options['phone_mask'] : $popup_options['phone_custom_mask']).'"';
						}
						$content = str_replace(
							array('{subscription-name}', '{subscription-email}', '{subscription-phone}', '{subscription-message}', '{subscription-submit}'),
							array(
								'<input class="ulp-input ulp-input-field" type="text" name="ulp-name" placeholder="'.esc_html($popup_options['name_placeholder']).'" value="" onfocus="jQuery(this).removeClass(\'ulp-input-error\');">'.($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on' ? '<div class="ulp-fa-input-table"><div class="ulp-fa-input-cell"><i class="fa fa-user"></i></div></div>' : ''),
								'<input class="ulp-input ulp-input-field" type="text" name="ulp-email" placeholder="'.esc_html($popup_options['email_placeholder']).'" value="" onfocus="jQuery(this).removeClass(\'ulp-input-error\');">'.($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on' ? '<div class="ulp-fa-input-table"><div class="ulp-fa-input-cell"><i class="fa fa-envelope"></i></div></div>' : ''),
								'<input class="ulp-input ulp-input-field'.$mask_class.'"'.$mask.' type="text" name="ulp-phone" placeholder="'.esc_html($popup_options['phone_placeholder']).'" value="" onfocus="jQuery(this).removeClass(\'ulp-input-error\');">'.($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on' ? '<div class="ulp-fa-input-table"><div class="ulp-fa-input-cell"><i class="fa fa-phone"></i></div></div>' : ''),
								'<textarea class="ulp-input ulp-input-field" name="ulp-message" placeholder="'.esc_html($popup_options['message_placeholder']).'" onfocus="jQuery(this).removeClass(\'ulp-input-error\');"></textarea>',
								'<a class="ulp-submit'.($popup_options['button_inherit_size'] == 'on' ? ' ulp-inherited' : '').'" onclick="return ulp_subscribe(this);"'.($ulp->options['fa_enable'] == 'on' && !empty($popup_options['button_icon']) && $popup_options['button_icon'] != 'fa-noicon' ? ' data-icon="'.$popup_options['button_icon'].'"' : '').' data-label="'.esc_html($popup_options['button_label']).'" data-loading="'.esc_html($popup_options['button_label_loading']).'">'.($ulp->options['fa_enable'] == 'on' && !empty($popup_options['button_icon']) && $popup_options['button_icon'] != 'fa-noicon' ? '<i class="fa '.$popup_options['button_icon'].'"></i>&nbsp; ' : '').esc_html($popup_options['button_label']).'</a>'),
							$layer_options['content']);
						if ($ulp->options['recaptcha_enable'] == 'on') {
							$recaptcha_id = 'ulp-recaptcha-'.$ulp->random_string(8);
							$content = str_replace('{recaptcha}', '<div class="ulp-recaptcha" id="'.$recaptcha_id.'" name="'.$recaptcha_id.'" data-theme="'.esc_html($popup_options['recaptcha_theme']).'"></div>', $content);
						}
						$content = apply_filters('ulp_front_popup_content', $content, $popup_options);
						$content = do_shortcode($content);
						$base64 = false;
						if (strpos(strtolower($content), '<iframe') !== false || strpos(strtolower($content), '<video') !== false || strpos(strtolower($content), '<audio') !== false) {
							$base64 = true;
							$content = base64_encode($content);
						}
						$html .= '
								<div class="ulp-layer" id="ulp-inline-layer-'.$inline_id.'-'.$layer['id'].'" data-left="'.$layer_options['left'].'" data-top="'.$layer_options['top'].'" data-appearance="'.$layer_options['appearance'].'" data-appearance-speed="'.$layer_options['appearance_speed'].'" data-appearance-delay="'.$layer_options['appearance_delay'].'"'.(!empty($layer_options['width']) ? ' data-width="'.$layer_options['width'].'"' : '').(!empty($layer_options['height']) ? ' data-height="'.$layer_options['height'].'"' : '').' data-font-size="'.$layer_options['font_size'].'"'.($base64 ? ' data-base64="yes"' : '').' '.(!empty($layer_options['scrollbar']) ? ' data-scrollbar="'.$layer_options['scrollbar'].'"' : ' data-scrollbar="off"').(!empty($layer_options['confirmation_layer']) ? ' data-confirmation="'.$layer_options['confirmation_layer'].'"' : ' data-confirmation="off"').'>'.$content.'</div>';
						if (!empty($layer_options['background_color'])) {
							$rgb = $ulp->get_rgb($layer_options['background_color']);
							$background = 'background-color:'.$layer_options['background_color'].';background-color:rgba('.$rgb['r'].','.$rgb['g'].','.$rgb['b'].','.$layer_options['background_opacity'].');';
						} else $background = '';
						if (!empty($layer_options['background_image'])) {
							$background .= 'background-image:url('.$layer_options['background_image'].');background-repeat:repeat;';
						}
						
						$font = ($layer_options['text_shadow_size'] > 0 && !empty($layer_options['text_shadow_color']) ? "text-shadow: ".$layer_options['text_shadow_color']." ".$layer_options['text_shadow_size']."px ".$layer_options['text_shadow_size']."px ".$layer_options['text_shadow_size']."px;" : "");
						if ($layer_options['font'] != 'inherit') $font .= "font-family:'".$layer_options['font']."',arial;";
						if ($layer_options['font_weight'] != 'inherit') $font .= "font-weight:".$layer_options['font_weight'].";";
						if ($layer_options['font_color'] != '') $font .= "color:".$layer_options['font_color'].";";
						
						$style .= '#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].',#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' p,#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' a,#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' span,#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' li,#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' input,#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' button,#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' textarea,#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' select {'.$font.'}';
						$style .= '#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' .ulp-checkbox label:after{background:'.$layer_options['font_color'].'}';
						if ($ulp->options['fa_enable'] == 'on' && $popup_options['input_icons'] == 'on') {
							$style .= '#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' input.ulp-input, #ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' select.ulp-input {padding-left: '.intval(4+2*$layer_options['font_size']).'px !important;} #ulp-inline-layer-'.$inline_id.'-'.$layer['id'].' div.ulp-fa-input-cell {width: '.intval(2*$layer_options['font_size']).'px !important; padding-left: 4px !important;}';
						}
						$style .= '#ulp-inline-layer-'.$inline_id.'-'.$layer['id'].'{'.$background.'z-index:'.($layer_options['index']+10).';text-align:'.$layer_options['content_align'].';'.$layer_options['style'].'}';
						if (!array_key_exists($layer_options['font'], $ulp->local_fonts)) $layer_webfonts[] = $layer_options['font'];
					}
					$html .= '
							</div>
						</div>';
					$html = '<style>'.$style.'</style>'.$html;
					
					if (!empty($layer_webfonts)) {
						$layer_webfonts = array_unique($layer_webfonts);
						if ($ulp->options['version'] >= 4.58) {
							$webfonts_array = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_webfonts WHERE family IN ('".implode("', '", $layer_webfonts)."') AND deleted = '0' ORDER BY family", ARRAY_A);
						} else $webfonts_array = array();
						if(!empty($webfonts_array)){
							$families = array();
							$subsets = array();
							foreach($webfonts_array as $webfont) {
								$families[] = str_replace(' ', '+', $webfont['family']).':'.$webfont['variants'];
								$webfont_subsets = explode(',', $webfont['subsets']);
								if (!empty($webfont_subsets) && is_array($webfont_subsets)) $subsets = array_merge($subsets, $webfont_subsets);
							}
							$subsets = array_unique($subsets);
							$query = '?family='.implode('|', $families);
							if (!empty($subsets)) $query .= '&subset='.implode(',', $subsets);
							$html = '<link href="//fonts.googleapis.com/css'.$query.'" rel="stylesheet" type="text/css">'.$html;
						}
					}
				}
			}
		//}
		return $html;
	}

}
?>