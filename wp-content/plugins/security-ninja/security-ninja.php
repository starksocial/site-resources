<?php
/*
Plugin Name: Security Ninja
Plugin URI: http://security-ninja.webfactoryltd.com/
Description: Check your site for <strong>security vulnerabilities</strong> and get precise suggestions for corrective actions on passwords, user accounts, file permissions, database security, version hiding, plugins, themes and other security aspects.
Author: Web factory Ltd
Version: 2.0
Author URI: http://www.webfactoryltd.com/

== License ==
(c) 2011. - 2015. Web factory Ltd
This plugin is comprised of two parts.
(1) the PHP code and integrated HTML are licensed under the General Public
License (GPL). You will find a copy of the GPL in the License directory.
(2) All other parts, but not limited to the CSS code, images, and design are
licensed according to the license purchased from Envato.
Read more about licensing here: http://themeforest.net/licenses
*/


if (!function_exists('add_action')) {
  die('Please don\'t open this file directly!');
}


// constants
define('WF_SN_DIC', plugin_dir_path(__FILE__) . 'brute-force-dictionary.txt');
define('WF_SN_OPTIONS_KEY', 'wf_sn_results');
define('WF_SN_MAX_USERS_ATTACK', 3);
define('WF_SN_MAX_EXEC_SEC', 200);


require_once 'sn-tests.php';


class wf_sn {
  static $version = 2.0;

  // init plugin
  static function init() {
    // does the user have enough privilages to use the plugin?
    if (is_admin() && current_user_can('administrator')) {
      // this plugin requires WP v3.7
      if (!version_compare(get_bloginfo('version'), '3.7',  '>=')) {
        add_action('admin_notices', array(__CLASS__, 'min_version_error'));
        return;
      } else {
        // add menu item to tools
        add_action('admin_menu', array(__CLASS__, 'admin_menu'));

        // aditional links in plugin description
        add_filter('plugin_action_links_' . basename(dirname(__FILE__)) . '/' . basename(__FILE__),
                   array(__CLASS__, 'plugin_action_links'));
        add_filter('plugin_row_meta', array(__CLASS__, 'plugin_meta_links'), 10, 2);

        // enqueue scripts
        add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueue_scripts'));

        // register ajax endpoints
        add_action('wp_ajax_sn_run_tests', array(__CLASS__, 'run_tests'));
        add_action('wp_ajax_sn_hide_tab', array(__CLASS__, 'hide_tab'));
        add_action('wp_ajax_sn_refresh_update', array(__CLASS__, 'get_updates_info'));

        // warn if tests were not run
        add_action('admin_notices', array(__CLASS__, 'run_tests_warning'));

        // warn if Wordfence is active
        add_action('admin_notices', array(__CLASS__, 'wordfence_warning'));

        // add_markup for UI overlay
        add_action('admin_footer', array(__CLASS__, 'admin_footer'));
      } // if version
    } // if
  } // init


  // add links to plugin's description in plugins table
  static function plugin_meta_links($links, $file) {
    $documentation_link = '<a target="_blank" href="' . plugin_dir_url(__FILE__) . 'documentation/' .
                          '" title="View documentation">Documentation</a>';
    $support_link = '<a target="_blank" href="http://codecanyon.net/user/WebFactory#contact" title="Contact Web factory">Support</a>';

    if ($file == plugin_basename(__FILE__)) {
      $links[] = $documentation_link;
      $links[] = $support_link;
    }

    return $links;
  } // plugin_meta_links


  // add settings link to plugins page
  static function plugin_action_links($links) {
    $settings_link = '<a href="tools.php?page=wf-sn" title="Security Ninja">Analyze site</a>';
    array_unshift($links, $settings_link);

    return $links;
  } // plugin_action_links


  // test if plugin's page is visible
  static function is_plugin_page() {
    $current_screen = get_current_screen();

    if ($current_screen->id == 'tools_page_wf-sn') {
      return true;
    } else {
      return false;
    }
  } // is_plugin_page


  // hide any add-on tab
  static function hide_tab() {
    $tab = trim(@$_POST['tab']);
    $tabs = get_transient('wf_sn_hidden_tabs');
    $tabs[] = $tab;

    set_transient('wf_sn_hidden_tabs', $tabs, DAY_IN_SECONDS * 90);

    wp_send_json_success();
  } // hide_tab


  // enqueue CSS and JS scripts on plugin's pages
  static function enqueue_scripts() {
    if (self::is_plugin_page()) {
      global $wp_styles;
      $plugin_url = plugin_dir_url(__FILE__);

      wp_enqueue_script('jquery-ui-tabs');
      wp_enqueue_script('sn-jquery-plugins', $plugin_url . 'js/wf-sn-jquery-plugins.js', array(), self::$version, true);
      wp_enqueue_script('sn-js', $plugin_url . 'js/wf-sn-common.js', array(), self::$version, true);
      wp_enqueue_style('sn-css', $plugin_url . 'css/wf-sn-style.css', array(), self::$version);

      $open_sans = str_replace('400,600', '400,600,800', $wp_styles->registered['open-sans']->src);
      wp_deregister_style('open-sans');
      wp_enqueue_style('open-sans', $open_sans);

      wp_localize_script('jquery', 'sn_plugin_url', $plugin_url);
    } // if
  } // enqueue_scripts


  // add entry to admin menu
  static function admin_menu() {
    add_management_page('Security Ninja', 'Security Ninja', 'manage_options', 'wf-sn', array(__CLASS__, 'options_page'));
  } // admin_menu


  // display warning if test were never run
  static function run_tests_warning() {
    $tests = get_option(WF_SN_OPTIONS_KEY);

    if (self::is_plugin_page() && !$tests['last_run']) {
      echo '<div class="error"><p>Security Ninja <strong>tests were never run.</strong> Click "Run tests" to run them now and analyze your site for security vulnerabilities.</p></div>';
    } elseif (self::is_plugin_page() && (current_time('timestamp') - 30*24*60*60) > $tests['last_run']) {
      echo '<div class="error"><p>Security Ninja <strong>tests were not run for more than 30 days.</strong> It\'s advisable to run them once in a while. Click "Run tests" to run them now and analyze your site for security vulnerabilities.</p></div>';
    }
  } // run_tests_warning


  // display warning if Wordfence plugin is active
  static function wordfence_warning() {
    if (defined('WORDFENCE_VERSION') && WORDFENCE_VERSION) {
      echo '<div class="error"><p>Please <strong>deactivate Wordfence plugin</strong> before running Security Ninja tests. Some tests are detected as site attacks by Wordfence and hence can\'t be performed properly. Activate Wordfence once you\'re done testing.</p></div>';
    }
  } // wordfence_warning


  // display warning if test were never run
  static function min_version_error() {
    echo '<div class="error"><p>Security Ninja <b>requires WordPress version 3.7</b> or higher to function properly. You\'re using WordPress version ' . get_bloginfo('version') . '. Please <a href="' . admin_url('update-core.php') . '" title="Update WP core">update</a>.</p></div>';
  } // min_version_error


  // add markup for UI overlay
  static function admin_footer() {
    if (self::is_plugin_page()) {
      echo '<div id="sn_overlay"><div class="sn-overlay-wrapper">';
      echo '<div class="inner">';

      // Title & sitting Ninja
      echo '<div class="wf-sn-title">
             <h2>Security Ninja</h2>
             <div class="sitting-ninja">&nbsp;</div>
           </div>';

      // Outer
      echo '<div class="wf-sn-overlay-outer">';

      // Content
      echo '<div class="wf-sn-overlay-content">';
      echo '<div id="sn-site-scan" style="display: none;">';
      echo '<h3>Security Ninja is analyzing your site.<br/>It will only take a few moments ...</h3>';
      echo '</div>';

      do_action('sn_overlay_content');

      echo '<div class="loader"><img title="Loading ..." src="' . plugins_url('images/ajax-loader.gif', __FILE__) . '" alt="Loading..." /></div>';
      echo '<p><br><br><a id="abort-scan" href="#" class="button button-secondary input-button red">Abort scanning</a></p>';

      do_action('sn_overlay_content_after');

      echo '</div>'; // wf-sn-overlay-content

      echo '</div></div></div></div>';
    } // if is_plugin_page
  } // admin_footer


  // ad for add-on
  static function core_ad_page() {
    echo '<div class="submit-test-container"><p><b>Core Scanner</b> is an add-on available for Security Ninja. It gives you a peace of mind by scanning all your core WP files (600+) to ensure they have not been modified by a 3rd party.</p>
<p><a target="_blank" href="http://codecanyon.net/item/core-scanner-addon-for-security-ninja/2927931/?ref=WebFactory" class="button-primary input-button">View details and get the Core Scanner add-on</a></p></div>';

    echo '<table class="addon-ad" width="100%"><tr><td width="50%" valign="top">';
    echo '<ul class="sn-list">
<li>scan WP core files with <strong>one click</strong></li>
<li>quickly identify <strong>problematic files</strong></li>
<li><strong>restore modified files</strong> with one click</li>
<li>great for removing <strong>exploits</strong> and fixing accidental file edits/deletes</li>
<li>view files\' <strong>source</strong> to take a closer look</li>
<li><strong>fix</strong> broken WP auto-updates</li>
<li>detailed help and description</li>
<li><strong>color-coded results</strong> separate files into 5 categories:
<ul>
<li>files that are modified and should not have been</li>
<li>files that are missing and should not be</li>
<li>files that are modified and they are supposed to be</li>
<li>files that are missing but they are not vital to WP</li>
<li>files that are intact</li>
</ul></li>
<li>complete integration with Ninja\'s easy-to-use GUI</li>
</ul>';

    echo '<p><a href="#" class="hide_tab" data-tab-id="core" title="Hide this tab"><i>No thank you, I\'m not interested (hide this tab)</i></a></p>';

    echo '</td><td>';
    echo '<a target="_blank" href="http://codecanyon.net/item/core-scanner-addon-for-security-ninja/2927931/?ref=WebFactory" title="Core Scanner add-on"><img style="max-width: 100%;" src="' .  plugin_dir_url(__FILE__) . 'images/core-scanner.jpg" title="Core Scanner add-on" alt="Core Scanner add-on" /></a>';
    echo '</td></tr></table>';
  } // core_ad_page


  // ad for add-on
  static function schedule_ad_page() {
    echo '<div class="submit-test-container"><p><b>Scheduled Scanner</b> is an add-on available for Security Ninja. It gives you an additional peace of mind by automatically running Security Ninja and Core Scanner tests every day. If any changes occur or your site gets hacked you\'ll immediately get notified via email</p>
    <p><a target="_blank" href="http://codecanyon.net/item/scheduled-scanner-addon-for-security-ninja/3686330?ref=WebFactory" class="button-primary input-button">View details and get the Scheduled Scanner add-on</a></p>
    </div>';

    echo '<table class="addon-ad" width="100%"><tr><td width="50%" valign="top">';
    echo '<ul class="sn-list">
<li>give yourself a peace of mind with <strong>automated scans</strong> and email reports</li>
<li><strong>get alerted</strong> when your site is <strong>hacked</strong></li>
<li>compatible with both <strong>Security Ninja & Core Scanner add-on</strong></li>
<li>extremely <strong>easy</strong> to setup - set once and forget</li>
<li>optional <strong>email reports</strong> - get them after every scan or only after changes occur on your site</li>
<li>detailed, color-coded <strong>scan log</strong></li>
<li>complete integration with Ninja\'s easy-to-use GUI</li>
</ul>';

    echo '<p><a class="hide_tab" data-tab-id="schedule" href="#" title="Hide this tab"><i>No thank you, I\'m not interested (hide this tab)</i></a></p>';

    echo '</td><td>';
    echo '<a target="_blank" href="http://codecanyon.net/item/scheduled-scanner-addon-for-security-ninja/3686330?ref=WebFactory" title="Scheduled Scanner add-on"><img style="max-width: 100%;" src="' .  plugin_dir_url(__FILE__) . 'images/scheduled-scanner.jpg" title="Scheduled Scanner add-on" alt="Scheduled Scanner add-on" /></a>';
    echo '</td></tr></table>';
  } // schedule_ad_page


  // ad for add-on
  static function logger_ad_page() {
    echo '<div class="submit-test-container"><p><b>Events Logger</b> is an add-on available for Security Ninja. It monitors, tracks and reports every change on your WordPress site, both in the admin and on the frontend.</p><p>
    <a target="_blank" href="http://security-ninja.webfactoryltd.com/get-events-logger/" class="button-primary input-button">View details and get the Events Logger add-on</a></p></div>';

    echo '<table class="addon-ad" width="100%"><tr><td width="50%" valign="top">';
    echo '<ul class="sn-list">';
    echo '<li>monitor, track and <b>log more than 50 events</b> on the site in great detail</li>
          <li><b>know what happened</b> on the site at any time, in the admin and on the frontend</li>
          <li>prevent <b>"I didn\'t do it"</b> conversations with clients - Events Logger doesn\'t forget or lie</li>
          <li>easily <b>filter</b> trough the data</li>
          <li>know exactly when and <b>how an action happened</b>, and who did it</li>
          <li>receive <b>email alerts</b> for selected groups of events</li>
          <li>each logged event has the following details:<ul>
             <li>date and time</li>
             <li>event description (ie: "Search widget was added to Primary sidebar" or "Failed login attempt with username asdf.")</li>
             <li>username and role of user who did the action</li>
             <li>IP and user agent of the user</li>
             <li>module</li>
             <li>WordPress action/filter</li></ul></li>
          <li>complete integration with Ninja\'s easy-to-use GUI</li>
          <li>it\'s compatible with all themes and plugins</li>';
    echo '</ul>';
    echo '<p><a class="hide_tab" data-tab-id="logger" href="#" title="Hide this tab"><i>No thank you, I\'m not interested (hide this tab)</i></a></p>';
    echo '</td><td>';
    echo '<a target="_blank" href="http://security-ninja.webfactoryltd.com/get-events-logger/" title="Events Logger add-on"><img style="max-width: 100%;" src="' .  plugin_dir_url(__FILE__) . 'images/events-logger.jpg" title="Events Logger add-on" alt="Events Logger add-on" /></a>';
    echo '</td></tr></table>';
  } // logger_ad_page


  // ad for add-on
  static function malware_ad_page() {
    echo '<div class="submit-test-container"><p><b>Malware Scanner</b> is an add-on available for Security Ninja. It scans all of plugin, theme and custom <i>wp-content</i> files as well as the options DB table in search for malware and other suspicious code.</p>
    <p><a target="_blank" href="http://security-ninja.webfactoryltd.com/malware-scanner/" class="button-primary input-button">View details and get the Malware Scanner add-on</a></p></div>';

    echo '<table class="addon-ad" width="100%"><tr><td width="50%" valign="top">';
    echo '<ul class="sn-list">';
    echo '<li><b>one click scan</b> - quickly identify problematic files</li>
          <li>scan all (active and disabled) <b>theme files</b></li>
          <li>scan all (active and disabled) <b>plugin files</b></li>
          <li>scan all files uploaded to <i>wp-content</i> folder</li>
          <li>scan <b>options DB table</b></li>
          <li>more than <b>20 tests performed</b> on each file</li>
          <li>see exact parts of the file that malware scanner marked as suspicious</li>
          <li>whitelist files that you have inspected and know are safe</li>
          <li>optimised for large WP installations with numerous files</li>
          <li>complete integration with Security Ninja\'s easy-to-use GUI</li>
          <li>compatible with all themes and plugins</li>';
    echo '</ul>';
    echo '<p><a class="hide_tab" data-tab-id="malware" href="#" title="Hide this tab"><i>No thank you, I\'m not interested (hide this tab)</i></a></p>';
    echo '</td><td>';
    echo '<a target="_blank" href="http://security-ninja.webfactoryltd.com/malware-scanner/" title="Malware Scanner add-on"><img style="max-width: 100%;" src="' .  plugin_dir_url(__FILE__) . 'images/malware-scanner.jpg" title="Malware Scanner add-on" alt="Malware Scanner add-on" /></a>';
    echo '</td></tr></table>';
  } // malware_ad_page


  static function get_updates_info($force = false) {
    $updates = get_transient('wf_sn_updates');

    if (defined('DOING_AJAX') && DOING_AJAX) {
      $force = true;
    }

    if (!$updates || $force) {
      $args = array('wf_sn' => (string) wf_sn::$version,
                    'wf_sn_cs' => (class_exists('wf_sn_cs') && isset(wf_sn_cs::$version))? (string) wf_sn_cs::$version: 0,
                    'wf_sn_ms' => (class_exists('wf_sn_ms') && isset(wf_sn_ms::$version))? (string) wf_sn_ms::$version: 0,
                    'wf_sn_ss' => (class_exists('wf_sn_ss') && isset(wf_sn_ss::$version))? (string) wf_sn_ss::$version: 0,
                    'wf_sn_el' => (class_exists('wf_sn_el') && isset(wf_sn_el::$version))? (string) wf_sn_el::$version: 0
                    );
      $request = wp_remote_get(add_query_arg(array('action' => 'check', 'ver' => $args), 'http://security-ninja.webfactoryltd.com/updates.php'));

      if (!is_wp_error($request) && $request['response']['code'] == 200) {
        $updates = unserialize($request['body']);
      } else {
        $updates = false;
      }
      set_transient('wf_sn_updates', $updates, DAY_IN_SECONDS * 3);
    }

    if (defined('DOING_AJAX') && DOING_AJAX) {
      wp_send_json_success();
    } else {
      return $updates;
    }

  } // get_updates_info


  // display updates info about all SN plugins
  static function updates_page() {
    $updates = self::get_updates_info(false);

    if (!$updates) {
      echo '<p>Update info is currently unavailable.</p>';
      echo '<p><br><a href="#" class="button button-secondary input-button gray" id="sn-refresh-update">Refresh update info</a></p>';
      return;
    }

    $msg_ok = '<span class="green">no updates are available</span>';
    $msg_upd = '<span class="orange">updates are available; <a href="http://codecanyon.net/downloads" target="_blank">download</a></span>';

    echo '<table class="wp-list-table widefat" id="security-ninja" cellspacing="0">';
    echo '<thead><tr>';
    echo '<th>Module</th>';
    echo '<th>Status</th>';
    echo '</tr></thead>';
    echo '<tbody>';
    echo '<tr>';
    if ($updates['wf_sn']) {
      echo '<td>Security Ninja</td><td>' . $msg_upd . '</td>';
    } else {
      echo '<td>Security Ninja</td><td>' . $msg_ok . '</td>';
    }
    echo '</tr>';
    echo '<tr>';
    if (class_exists('wf_sn_cs')) {
      if ($updates['wf_sn_cs']) {
        echo '<td>Core Scanner add-on</td><td>' . $msg_upd . '</td>';
      } else {
        echo '<td>Core Scanner add-on</td><td>' . $msg_ok . '</td>';
      }
    } else {
      echo '<td>Core Scanner add-on</td><td>add-on is not installed</td>';
    }
    echo '</tr>';
    echo '<tr>';
    if (class_exists('wf_sn_ms')) {
      if ($updates['wf_sn_ms']) {
        echo '<td>Malware Scanner add-on</td><td>' . $msg_upd . '</td>';
      } else {
        echo '<td>Malware Scanner add-on</td><td>' . $msg_ok . '</td>';
      }
    } else {
      echo '<td>Malware Scanner add-on</td><td>add-on is not installed</td>';
    }
    echo '</tr>';
    echo '<tr>';
    if (class_exists('wf_sn_ss')) {
      if ($updates['wf_sn_ss']) {
        echo '<td>Scheduled Scanner add-on</td><td>' . $msg_upd . '</td>';
      } else {
        echo '<td>Scheduled Scanner add-on</td><td>' . $msg_ok . '</td>';
      }
    } else {
      echo '<td>Scheduled Scanner add-on</td><td>add-on is not installed</td>';
    }
    echo '</tr>';
    echo '<tr>';
    if (class_exists('wf_sn_el')) {
      if ($updates['wf_sn_el']) {
        echo '<td>Events Logger add-on</td><td>' . $msg_upd . '</td>';
      } else {
        echo '<td>Events Logger add-on</td><td>' . $msg_ok . '</td>';
      }
    } else {
      echo '<td>Events Logger add-on</td><td>add-on is not installed</td>';
    }
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';

    echo '<p><br><a href="#" class="button button-secondary input-button gray" id="sn-refresh-update">Refresh update info</a></p>';
  } // updates_page


  // whole options page
  static function options_page() {
    // does the user have enough privilages to access this page?
    if (!current_user_can('administrator'))  {
      wp_die('You do not have sufficient permissions to access this page.');
    }

    $hidden_tabs = get_transient('wf_sn_hidden_tabs');
    if (!$hidden_tabs) {
      $hidden_tabs = array();
    }
    $tabs = array();
    $tabs[] = array('id' => 'sn_tests', 'class' => '', 'label' => 'Tests', 'callback' => array('self', 'tests_table'));
    $tabs[] = array('id' => 'sn_help', 'class' => 'sn_help', 'label' => 'Tests\' details &amp; help', 'callback' => array('self', 'help_table'));
    $tabs[] = array('id' => 'sn_updates', 'class' => 'sn_updates', 'label' => 'Updates', 'callback' => array('self', 'updates_page'));
    if (!in_array('core', $hidden_tabs)) {
      $tabs[] = array('id' => 'sn_core', 'class' => 'promo_tab', 'label' => 'Core Scanner', 'callback' => array('self', 'core_ad_page'));
    }
    if (!in_array('malware', $hidden_tabs)) {
      $tabs[] = array('id' => 'sn_malware', 'class' => 'promo_tab', 'label' => 'Malware Scanner', 'callback' => array('self', 'malware_ad_page'));
    }
    if (!in_array('schedule', $hidden_tabs)) {
      $tabs[] = array('id' => 'sn_schedule', 'class' => 'promo_tab', 'label' => 'Scheduled Scanner', 'callback' => array('self', 'schedule_ad_page'));
    }
    if (!in_array('logger', $hidden_tabs)) {
      $tabs[] = array('id' => 'sn_logger', 'class' => 'promo_tab', 'label' => 'Events Logger', 'callback' => array('self', 'logger_ad_page'));
    }
    $tabs = apply_filters('sn_tabs', $tabs);

    echo '<div class="wrap">' . get_screen_icon('sn-lock');
    echo '<h2>Security Ninja</h2>';

    echo '<div class="wf-sn-title">
           <h2>Security Ninja</h2>
           <div class="sitting-ninja">&nbsp;</div>
         </div>';

    echo '<div id="tabs">';
    echo '<ul>';
    foreach ($tabs as $tab) {
      echo '<li><a href="#' . $tab['id'] . '" class="' . $tab['class'] . '">' . $tab['label'] . '</a></li>';
    }
    echo '</ul>';

    foreach ($tabs as $tab) {
      echo '<div style="display: none;" id="' . $tab['id'] . '">';
      call_user_func($tab['callback']);
      echo '</div>';
    }

    echo '</div>'; // tabs
    echo '</div>'; // wrap
  } // options_page


  // display tests help & info
  static function help_table() {
    require_once 'tests-description.php';
  } // help_table


  // display tests table
  static function tests_table() {
    // get test results from cache
    $tests = get_option(WF_SN_OPTIONS_KEY);

    echo '<div class="submit-test-container">
          <input type="submit" value=" Run tests " id="run-tests" class="button-primary" name="Submit" />';

    if ($tests['last_run']) {
      echo '<span class="sn-notice">Tests were last run on: ' . date(get_option('date_format') . ' ' . get_option('time_format'), $tests['last_run']) . '.</span>';
    }

    echo '<p><strong>Please read!</strong> These tests only serve as suggestions! Although they cover years of best practices getting all test <i>green</i> will not guarantee your site will not get hacked. Likewise, getting them all <i>red</i> doesn\'t mean you\'ll certainly get hacked. Please read each test\'s detailed information to see if it represents a real security issue for your site. Suggestions and test results apply to public, production sites, not local, development ones. <br /> <span class="red">If you need help getting your WordPress secured by experts please contact our <a href="http://codecanyon.net/user/WebFactory#contact" target="_blank">support</a>.</span></p><br />';

    echo '</div>';

    if ($tests['last_run']) {
      echo '<table class="wp-list-table widefat" cellspacing="0" id="security-ninja">';
      echo '<thead><tr>';
      echo '<th class="sn-status">Status</th>';
      echo '<th>Test description</th>';
      echo '<th>Test results</th>';
      echo '<th>&nbsp;</th>';
      echo '</tr></thead>';
      echo '<tbody>';

      if (is_array($tests['test'])) {
        // test Results
        foreach($tests['test'] as $test_name => $details) {
          echo '<tr>
                  <td class="sn-status">' . self::status($details['status']) . '</td>
                  <td>' . $details['title'] . '</td>
                  <td>' . $details['msg'] . '</td>
                  <td class="sn-details"><a href="#' . $test_name . '" class="button action">Details, tips &amp; help</a></td>
                </tr>';
        } // foreach ($tests)
      } else { // no test results
        echo '<tr>
                <td colspan="4">No test results are available. Click "Run tests" to run tests now.</td>
              </tr>';
      } // if tests

      echo '</tbody>';
      echo '<tfoot><tr>';
      echo '<th class="sn-status">Status</th>';
      echo '<th>Test description</th>';
      echo '<th>Test results</th>';
      echo '<th>&nbsp;</th>';
      echo '</tr></tfoot>';
      echo '</table>';
    } // if $results
  } // tests_table


  // run all tests; via AJAX
  static function run_tests($return = false) {
    @set_time_limit(WF_SN_MAX_EXEC_SEC);
    $test_count = 0;
    $start_time = microtime(true);
    $test_description['last_run'] = current_time('timestamp');

    foreach(wf_sn_tests::$security_tests as $test_name => $test){
      if ($test_name[0] == '_') {
        continue;
      }
      $response = wf_sn_tests::$test_name();

      $test_description['test'][$test_name]['title'] = $test['title'];
      $test_description['test'][$test_name]['status'] = $response['status'];

      if (!isset($response['msg'])) {
        $response['msg'] = '';
      }

      if ($response['status'] == 10) {
        $test_description['test'][$test_name]['msg'] = sprintf($test['msg_ok'], $response['msg']);
      } elseif ($response['status'] == 0) {
        $test_description['test'][$test_name]['msg'] = sprintf($test['msg_bad'], $response['msg']);
      } else {
        $test_description['test'][$test_name]['msg'] = sprintf($test['msg_warning'], $response['msg']);
      }
      $test_count++;
    } // foreach

    do_action('security_ninja_done_testing', $test_description, microtime(true) - $start_time);

    if ($return) {
      return $test_description;
    } else {
      update_option(WF_SN_OPTIONS_KEY, $test_description);
      die('1');
    }
  } // run_test


  // convert status integer to button
  static function status($int) {
    if ($int == 0) {
      $string = '<span class="sn-error">Critical</span>';
    } elseif ($int == 10) {
      $string = '<span class="sn-success">Good</span>';
    } else {
      $string = '<span class="sn-warning">Warning</span>';
    }

    return $string;
  } // status


  // clean-up when deactivated
  static function deactivate() {
    delete_option(WF_SN_OPTIONS_KEY);
    delete_transient('wf_sn_hidden_tabs');
  } // deactivate
} // wf_sn class


// hook everything up
add_action('init', array('wf_sn', 'init'));

// when deativated clean up
register_deactivation_hook( __FILE__, array('wf_sn', 'deactivate'));