<?php
/**
 * Init related functions and actions.
 *
 * @author 		Tunafish
 * @package 	pro_ads_buy_and_sell_woocommerce/classes
 * @version     3.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Pro_Ads_Bs_Woo_Templates' ) ) :


class Pro_Ads_Bs_Woo_Templates {	

	public function __construct() 
	{
		add_action( 'woocommerce_before_single_product_summary', array( &$this, 'add_banner_designer'), 15 );
		add_action( 'woocommerce_before_add_to_cart_button', array($this, 'extend_product_options'));
	}
	
	
	
	
	
	
	
	
	// Buy sell example
	public function buyandsell_placeholder( $adzone_id )
	{
		global $pro_ads_bs_woo_main;
		
		$buyandsell_order_screen_url = $pro_ads_bs_woo_main->buyandsell_product_url();
		
		$url = add_query_arg( 'adzone_id', $adzone_id, $buyandsell_order_screen_url );
		$html = '';
		
		//$html.= '<a adzone_id="'.$adzone_id.'" href="'.$buyandsell_order_screen_url.'?adzone_id='.$adzone_id.'" class="buyandsell nopop">';
		$html.= '<a adzone_id="'.$adzone_id.'" href="'.$url.'" class="buyandsell nopop">';
		
			$html.= '<table border="0" cellpadding="0" cellspacing="0">';
				$html.= '<tbody>';
					$html.= '<tr>';
						$html.='<td>'. __('advertise here', 'wpproads') .'</td>';
					$html.= '</tr>';
				$html.= '</tbody>';
			$html.= '</table>';
		$html.= '</a>';
		
		return $html;
	}
	
	
	
	
	
	
	
	
	public function add_banner_designer() 
	{
		global $post, $wpdb, $product, $woocommerce, $pro_ads_bs_woo_main, $pro_ads_advertisers, $pro_ads_campaigns, $pro_ads_banners, $pro_ads_adzones;
		
		if( $pro_ads_bs_woo_main->is_proadswoo_product('', $post->ID) )
		{
			?>
            <div class="pro_ads_buyandsell_woo_ajax_content content_holder" style="display:inline;">
				<?php
                if( isset($_GET['adzone_id']) && !empty($_GET['adzone_id']) )
                {
                    echo $this->buy_and_sell_woo_order_content($_GET['adzone_id']);
                }
                else
                {
					echo '<h4>'.__('Advertise on this Website','wpproads').'</h4>';
                    echo $this->buy_and_sell_woo_select_adzone();
                }
				?>
            </div>
            <?php
		}
	}
	
	
	
	
	
	
	
	public function buy_and_sell_woo_select_adzone( $redirect = 0 )
	{
		global $pro_ads_adzones;
		
		$bs_woo_product = get_option('buyandsell_woocommerce_product', 0);
		
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
		
		$html = '';
		
		$html.= '<style>.product_title, .summary { display:none; }</style>';
		$html.= '<div class="login_box" style="margin:0 0 50px 0;">';
			$html.= '<select id="order_select_adzone" name="order_select_adzone" ajaxurl="'.admin_url('admin-ajax.php').'">';
				$html.= '<option value="">'.__('Select an Adzone','wpproads').'</option>';
				
				$all_adzones = $pro_ads_adzones->get_adzones();
				if( !empty($all_adzones))
				{
					foreach($all_adzones as $adzone)
					{
						$available_spots = $pro_ads_adzones->check_if_adzone_has_available_spots( $adzone->ID );
						if( $available_spots )
						{
							$adzone_no_buyandsell = get_post_meta( $adzone->ID, '_adzone_no_buyandsell', true );
							if( !$adzone_no_buyandsell )
							{
								if( $redirect )
								{
									$url = add_query_arg( 'adzone_id', $adzone->ID, get_permalink($bs_woo_product) );
									//$html.= '<option value="'.get_permalink($bs_woo_product).'?adzone_id='.$adzone->ID.'">'.get_the_title($adzone->ID).'</option>';
									$html.= '<option value="'.$url.'">'.get_the_title($adzone->ID).'</option>';
								}
								else
								{
									$html.= '<option value="'.$adzone->ID.'">'.get_the_title($adzone->ID).'</option>';
								}
							}
						}
					}
				}
				
			$html.= '</select>';
			$html.= '<span class="loading_dots order_page_loading" style="display:none;">'.__('Loading','wpproads').'<span>.</span><span>.</span><span>.</span></span>';
		$html.= '</div>';
       
	   return $html;
	}
	
	
	
	
	
	
	
	
	public function buy_and_sell_woo_order_content($adzone_id)
	{
		global $post, $wpdb, $product, $woocommerce, $pro_ads_bs_woo_main, $pro_ads_advertisers, $pro_ads_campaigns, $pro_ads_banners, $pro_ads_adzones;
		
		$arr = $pro_ads_adzones->get_adzone_data( $adzone_id );
			
		if( !empty($arr['max_banners']) )
		{
			$max_ads = $arr['max_banners'];
		}
		
		$available_spots = $pro_ads_adzones->check_if_adzone_has_available_spots( $adzone_id );
		
		$contract = $pro_ads_bs_woo_main->get_contract_type($arr['adzone_buyandsell_contract']);
		$price = $pro_ads_bs_woo_main->get_price($arr['adzone_buyandsell_price']);
		
		$available = !empty($max_ads) ? sprintf('%s '.__('of', 'wpproads').' %s', $available_spots, $max_ads) : __('Unlimited', 'wpproads');
		$impressions = !empty($arr['adzone_buyandsell_est_impressions']) ? $arr['adzone_buyandsell_est_impressions'] : __('n/a','wpproads');
		$size = !empty($arr['size']) ? $arr['size'][0].'x'.$arr['size'][1] : __('Responsive', 'wpproads');
		$cost = $price['price_val'] > 0 ? woocommerce_price($price['price_val']) : $price['price'];
		$cost_info = $price['price_val'] > 0 ? sprintf(__('per', 'wpproads').' %s %s', $arr['adzone_buyandsell_duration'], strtolower($contract['name_short_multiple'])) : '';
		
		$current_user = wp_get_current_user();
		
		$html = '';
	
		//remove product image, there you gonna see the product designer
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
		
		if( $available_spots )
		{
			$html.= '<div class="login_box">';
				$html.= '<h4>'.__('Advertise on this Website','wpproads').'</h4>';
				$html.= '<table>';
					$html.= '<tbody>';
						$html.= '<tr>';
							$html.= '<td>';
								$html.= '<div class="value">'.get_the_title($adzone_id).'</div>';
								$html.= '<div class="info">'.$arr['adzone_description'].'</div>';
							$html.= '</td>';
							$html.= '<td>';
								$html.= '<div class="value">'.$size.'</div>';
								$html.= '<div class="info">'.__('Size','wpproads').'</div>';
							$html.= '</td>';
							$html.= '<td>';
								$html.= '<div class="value">'.$impressions.'</div>';
								$html.= '<div class="info">'.__('Est.impressions','wpproads').'</div>';
							$html.= '</td>';
							$html.= '<td>';
								$html.= '<div class="value">'.$available.'</div>';
								$html.= '<div class="info">'.__('Available','wpproads').'</div>';
							$html.= '</td>';
							$html.= '<td>';
								$html.= '<div class="value price" style="color:#85AD74;">'.$cost.'</div>';
								$html.= '<div class="info">';
									$html.= $cost_info;
								$html.= '</div>';
							$html.= '</td>';
						$html.= '</tr>';
					$html.= '</tbody>';
				$html.= '</table>';
				
			$html.= '</div>';
			
			$html.= '<div style=" display:inline-block;">';
				$html.= '<h4>'.__('Upload your Banner','wpproads').'</h4>';
				
				$adzone_size_style = !empty($arr['size']) ? 'width:'.$arr['size'][0].'px; height:'.$arr['size'][1].'px;' : 'width:100%; height:auto;';
				
				$html.= '<div id="buyandsell-upload-container">';
					$html.= '<a id="buyandsell-uploader" class="buyandsell_button wpproads_button" href="#">'.__('Select your Banner','wpproads').'</a>';
				
					$html.= '<div id="buyandsell-upload-imagelist">';
						$html.= '<ul id="buyandsell-ul-list" class="buyandsell-upload-list"></ul>';
					$html.= '</div>';
					
				$html.= '</div>';
			$html.= '</div>';
			
			$html.= '<script>';
			$html.= 'load_ajax_upload("'.admin_url( 'admin-ajax.php').'");';
			$html.= '</script>';
		}
		else
		{
			//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
			//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
			
			$html.= '<h4>'.__('Advertise on this Website','wpproads').'</h4> <div class="user_info"><p>'.__('Currently no spots available for this adzone.','wpproads').'</p></div>';	
			$html.= $this->buy_and_sell_woo_select_adzone();
			$html.= '<style>.product_title, .summary { display:none; }</style>';
		}
		
		$html.= '<style>.product_title, .summary .price { display:none; } .buyandsell-uploaded-files .adzone_preview { background:#EEE; '.$adzone_size_style.' }</style>';
		
		return $html;
	}
	
	
	
	
	
	
	
	function extend_product_options()
	{
		global $post, $wpdb, $product, $woocommerce, $pro_ads_bs_woo_main, $pro_ads_advertisers, $pro_ads_campaigns, $pro_ads_banners, $pro_ads_adzones;
		
		if( $pro_ads_bs_woo_main->is_proadswoo_product('', $post->ID) )
		{
			$adzone_id = isset($_GET['adzone_id']) && !empty($_GET['adzone_id']) ? $_GET['adzone_id'] : 0;
			echo $this->extend_product_options_form($adzone_id);
		}
	}
	
	
	
	
	
	
	public function extend_product_options_form($adzone_id)
	{
		global $post, $wpdb, $product, $woocommerce, $pro_ads_bs_woo_main, $pro_ads_advertisers, $pro_ads_campaigns, $pro_ads_banners, $pro_ads_adzones;
		
		$arr = $pro_ads_adzones->get_adzone_data( $adzone_id );
		$size = !empty($arr['size']) ? $arr['size'][0].'x'.$arr['size'][1] : __('responsive', 'wpproads');
		
		$html = '';
		
		$html.= '<div id="proadswoo_custom_data">';
			$html.= '<h4>'.__('Banner Options','wpproads').'</h4>';
			$html.= '<input type="hidden" name="banner_adzone" value="'.$adzone_id.'" />';
			$html.= '<input type="hidden" name="banner_size" value="'.$size.'" />';
			$html.= '<input type="hidden" name="banner_price" value="'.$arr['adzone_buyandsell_price'].'" />';
			$html.= '<div>';
				$html.= '<div>'.__('Banner Title','wpproads').'</div>';
				$html.= '<div>';
					$html.= '<input class="buyandsell_input banner_title_'.$adzone_id.'" name="banner_title" type="text" value="" />';
				$html.= '</div>';
			$html.= '</div>';
			$html.= '<div>';
				$html.= '<div>'.__('Banner Link','wpproads').'</div>';
				$html.= '<div>';
					$html.= '<input class="buyandsell_input banner_link_'.$adzone_id.'" name="banner_link" type="text" value="" placeholder="http://www.yourlink.com" />';
				$html.= '</div>';
			$html.= '</div>';
		$html.= '</div>';
		$html.= '<br />';
		
		return $html;
	}
	
	
	
	
	
	
	public function pro_ad_buy_and_sell_woo_settings()
	{
		$bs_woo_product = get_option('buyandsell_woocommerce_product', 0);
		?>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="tuna_meta metabox-holder">

                <div class="postbox nobg">
                    <div class="inside">
                        <table class="form-table">
                            <tbody>
                                
                                <tr>
                                    <th scope="row">
                                        <?php _e('BuyandSell Ads Woocommerce Product.', 'wpproads'); ?>
                                        <span class="description"><?php _e('This product handles sales for all adzones/banners on your website. No need do edit this item. All settings including pricing and contract duration will be automatically added from the Adzone Buy and Sell Options.','wpproads'); ?></span>
                                    </th>
                                    <td>
                                    	<?php
										if( $bs_woo_product )
										{
											?>
                                        	<a href="post.php?post=<?php echo $bs_woo_product; ?>&action=edit" class="main_button" target="_blank"><?php _e('BuyandSell Ads Woocommerce Product','wpproads'); ?></a>
                                        	<span class="description"><?php _e('','wpproads'); ?></span>
                                            <?php
										}
										else
										{
											?>
                                            <input type="submit" class="main_button" name="bsads_woo" value="<?php _e('Create BuyandSell Ads Woocommerce Product','wpproads'); ?>" />
                                            <span class="description"><?php _e('It seems like you have not yet created the BuyandSell Ads Woocommerce Product.','wpproads'); ?></span>
                                            <?php
										}
										?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
        <?php
	}
	
	
}

endif;
?>