<?php
class Pro_Ads_Bs_Woo_Shortcodes {	

	public function __construct() 
	{
		add_shortcode('buyandsell_available_adzones', array($this, 'sc_available_adzones'));
	}
	
	
	
	
	/*
	 * Shortcode function - [buyandsell_available_adzones]
	 *
	 * @access public
	 * @return array
	*/
	public function sc_available_adzones( $atts, $content = null ) 
	{	
		global $pro_ads_bs_woo_templates;
		
		extract( shortcode_atts( array(
			'id' => 0
		), $atts ) );
		
		$html = '';
		$html.= '<div class="pro_ads_buyandsell_woo_ajax_content content_holder" style="display:inline;">';
			$html.= $pro_ads_bs_woo_templates->buy_and_sell_woo_select_adzone(1);
		$html.= '</div>';
		
		return $html;
	}
}
?>