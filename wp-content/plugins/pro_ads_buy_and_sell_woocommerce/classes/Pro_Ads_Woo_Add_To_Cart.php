<?php
/*
 * Plugin Name:  Pro_Ads_Woo_Add_To_Cart
*/
class Pro_Ads_Woo_Add_To_Cart {
  private static $instance;

	public static function register() {
		if (self::$instance == null) {
			self::$instance = new Pro_Ads_Woo_Add_To_Cart();
		}
	}

	public function __construct() {
		// Add to cart
		add_filter('woocommerce_add_cart_item', array($this, 'add_cart_item'), 10, 1);

		// Add item data to the cart
		add_filter('woocommerce_add_cart_item_data', array($this, 'add_cart_item_data'), 10, 2);

		// Load cart data per page load
		add_filter('woocommerce_get_cart_item_from_session', array($this, 'get_cart_item_from_session'), 10, 2);

		// Validate when adding to cart
		//add_filter('woocommerce_add_to_cart_validation', array($this, 'validate_add_cart_item'), 10, 3);

		// Add meta to order
		add_action('woocommerce_order_item_meta', array($this, 'order_item_meta'), 10, 2);

		// Add meta to order - WC 2.x
		add_action('woocommerce_add_order_item_meta', array($this, 'order_item_meta_2'), 10, 2);
	}

	/**
	 * add_cart_item function.
	 *
	 * @access public
	 * @param mixed $cart_item
	 * @return void
	 */
	function add_cart_item($cart_item) 
	{
		global $pro_ads_bs_woo_main;
		
		if( $pro_ads_bs_woo_main->is_proadswoo_product($cart_item) )
		{
			$extra_cost = $cart_item['proadswoo_data']['_banner_price'];
			//$cart_item['data']->adjust_price($extra_cost);
			$cart_item['data']->price = $extra_cost;
		}

		return $cart_item;
	}
	
	
	
	
	
	function add_cart_item_data($cart_item_meta, $product_id)
	{
		$cart_item_meta['proadswoo_data']['banner_adzone'] = ( isset($_POST['banner_adzone']) && !empty($_POST['banner_adzone'])) ? get_the_title($_POST['banner_adzone']) : '';
		$cart_item_meta['proadswoo_data']['_banner_adzone_id'] = ( isset($_POST['banner_adzone']) && !empty($_POST['banner_adzone'])) ? $_POST['banner_adzone'] : 0;
		$cart_item_meta['proadswoo_data']['_banner_price'] = ( isset($_POST['banner_price']) && !empty($_POST['banner_price'])) ? $_POST['banner_price'] : 0;
		$cart_item_meta['proadswoo_data']['banner_size'] = ( isset($_POST['banner_size']) && !empty($_POST['banner_size'])) ? $_POST['banner_size'] : 'responsive';
		$cart_item_meta['proadswoo_data']['banner_title'] = ( isset($_POST['banner_title']) && !empty($_POST['banner_title'])) ? $_POST['banner_title'] : '';
		$cart_item_meta['proadswoo_data']['banner_link'] = ( isset($_POST['banner_link']) && !empty($_POST['banner_link'])) ? $_POST['banner_link'] : ''; 
		$cart_item_meta['proadswoo_data']['banner_img'] = ( isset($_POST['banner_img']) && !empty($_POST['banner_img'])) ? $_POST['banner_img'] : '';
		$cart_item_meta['proadswoo_data']['_banner_attach_id'] = ( isset($_POST['attach_id']) && !empty($_POST['attach_id'])) ? $_POST['attach_id'] : '';
		// Fix for WooCommerce 2.3
		$cart_item_meta['proadswoo_data']['_banner_img_txt'] = ( isset($_POST['banner_img']) && !empty($_POST['banner_img'])) ? $_POST['banner_img'] : '';
		$cart_item_meta['proadswoo_data']['_banner_link_txt'] = ( isset($_POST['banner_link']) && !empty($_POST['banner_link'])) ? $_POST['banner_link'] : '';
	
		return $cart_item_meta;
	}
	
	
	
	

	/**
	 * get_cart_item_from_session function.
	 *
	 * @access public
	 * @param mixed $cart_item
	 * @param mixed $values
	 * @return void
	 */
	function get_cart_item_from_session($cart_item, $values) 
	{
		if( isset($values['proadswoo_data'] ))
		{
			$cart_item['proadswoo_data'] = $values['proadswoo_data'];
		}
		
		//$cart_item = $this->add_cart_item($cart_item);
		$this->add_cart_item($cart_item);
		
		return $cart_item;
	}

	/**
	 * order_item_meta function.
	 *
	 * @access public
	 * @param mixed $item_meta
	 * @param mixed $cart_item
	 * @return void
	 */
	function order_item_meta($item_meta, $cart_item) 
	{
		global $pro_ads_bs_woo_main;
		
		if( $pro_ads_bs_woo_main->is_proadswoo_product($cart_item) )
		{
			$item_meta->add('Your Meta Name', 'Your Meta Value');
		}
	}

	/**
	 * order_item_meta_2 function.
	 *
	 * @access public
	 * @param mixed $item_id
	 * @param mixed $values
	 * @return void
	 */
	function order_item_meta_2($item_id, $cart_item) 
	{
		global $pro_ads_bs_woo_main;
		
		if (function_exists('woocommerce_add_order_item_meta')) 
		{
			if( $pro_ads_bs_woo_main->is_proadswoo_product($cart_item) )
			{
				$data = $cart_item['proadswoo_data'];
				if ( !empty( $cart_item['proadswoo_data'] ) )	
				{
					woocommerce_add_order_item_meta($item_id, 'banner_title', $data['banner_title']);
					woocommerce_add_order_item_meta($item_id, 'banner_link', $data['banner_link']);
					woocommerce_add_order_item_meta($item_id, 'banner_size', $data['banner_size']);
					woocommerce_add_order_item_meta($item_id, 'banner_adzone', $data['banner_adzone']);
					woocommerce_add_order_item_meta($item_id, 'banner_img', $data['banner_img']);
					
					woocommerce_add_order_item_meta($item_id, '_banner_img_txt', $data['_banner_img_txt']); // Fix for WooCommerce 2.3
					woocommerce_add_order_item_meta($item_id, '_banner_link_txt', $data['_banner_link_txt']); // Fix for WooCommerce 2.3
					woocommerce_add_order_item_meta($item_id, '_banner_adzone_id', $data['_banner_adzone_id']);
					woocommerce_add_order_item_meta($item_id, '_banner_price', $data['_banner_price']);
					woocommerce_add_order_item_meta($item_id, '_banner_attach_id', $data['_banner_attach_id']);
				}
			}
		}
	}

}

Pro_Ads_Woo_Add_To_Cart::register();
?>