<?php
/**
 * Init related functions and actions.
 *
 * @author 		Tunafish
 * @package 	pro_ads_buy_and_sell_woocommerce/classes
 * @version     3.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Pro_Ads_Bs_Woo_Init' ) ) :


class Pro_Ads_Bs_Woo_Init {	
	
	public function __construct() 
	{
		global $pro_ads_bs_woo_main, $pro_ads_bs_woo_templates, $pro_ads_bs_woo_ajax_image_upload, $pro_ads_bs_woo_shortcodes;
		
		// Run this on activation.
		register_activation_hook( WP_ADS_BS_WOO_FILE, array( $this, 'install' ) );
		
		// Load Functions ------------------------------------------------- 
		require_once( WP_ADS_BS_WOO_INC_DIR .'/ajax_functions.php');
		
		// Load Classes --------------------------------------------------- 
		require_once( WP_ADS_BS_WOO_DIR.'classes/Pro_Ads_Bs_Woo_Main.php');
		require_once( WP_ADS_BS_WOO_DIR.'classes/Pro_Ads_Bs_Woo_Templates.php');
		require_once( WP_ADS_BS_WOO_DIR.'classes/Pro_Ads_Woo_Add_To_Cart.php');
		require_once( WP_ADS_BS_WOO_DIR.'classes/Pro_Ads_Bs_Woo_Ajax_Image_upload.php');
		require_once( WP_ADS_BS_WOO_DIR.'classes/Pro_Ads_Bs_Woo_Meta_Options.php');
		require_once( WP_ADS_BS_WOO_DIR.'classes/Pro_Ads_Bs_Woo_Shortcodes.php');
		
		/* ----------------------------------------------------------------
		 * Set Classes
		 * ---------------------------------------------------------------- */
		$pro_ads_bs_woo_main = new Pro_Ads_Bs_Woo_Main();
		$pro_ads_bs_woo_templates = new Pro_Ads_Bs_Woo_Templates();
		$pro_ads_bs_woo_ajax_image_upload = new Pro_Ads_Bs_Woo_Ajax_Image_upload();
		$pro_ads_bs_woo_meta_options = new Pro_Ads_Bs_Woo_Meta_Options();
		$pro_ads_bs_woo_shortcodes = new Pro_Ads_Bs_Woo_Shortcodes();
		
		// Actions --------------------------------------------------------
		add_action('init', array( $this, 'init_method') );
		add_action('admin_menu', array( $this,'admin_actions') );
		add_action('delete_post', array($pro_ads_bs_woo_main, 'pro_ads_bs_woo_delete_action') );
	}
	
	
	
	
	
	
	
	/**
	 * Install PASBS
	 */
	public function install() 
	{
		global $pro_ads_bs_woo_main;
		
		$bs_woo_product = get_option('buyandsell_woocommerce_product', 0);
		
		if( !$bs_woo_product )
		{
			$product_id = $pro_ads_bs_woo_main->create_product( array('post_title' => 'BuyandSell Ads') );
			update_option( 'buyandsell_woocommerce_product', $product_id );
			$pro_ads_bs_woo_main->wpproads_bs_woo_plugin_is_network_activated() ? update_site_option( 'buyandsell_woocommerce_product', $product_id ) : '';
		}
	}
	
	
	
	
	
	public function init_method()
	{
		global $woocommerce, $pro_ads_bs_woo_main, $pro_ads_bs_woo_ajax_image_upload;
		
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('wp_pro_ads_bs_woo_js_functions', WP_ADS_BS_WOO_TPL_URL.'/js/buyandsellwoo.js');
		
		wp_enqueue_style("wp_pro_ads_bs_woo_style", WP_ADS_BS_WOO_TPL_URL."/css/proadsbswoo.css", false, WP_ADS_BS_WOO_VERSION, "all");
		
		if( isset($_GET['create']) )
		{
			$pro_ads_bs_woo_main->create_product();
		}
		
		if( isset($_GET['add_to_cart']) )
		{
			$pro_ads_bs_woo_main->pro_ads_add_to_cart( $_GET['add_to_cart'] );
		}
		
		$pro_ads_bs_woo_ajax_image_upload->add_script();
		
		//load_plugin_textdomain('wpproads', false, basename( dirname( __FILE__ ) ) . '/localization' );
		
		add_action('wp_ajax_buyandsell_upload', array($pro_ads_bs_woo_ajax_image_upload, 'upload'));
		add_action('wp_ajax_buyandsell_delete', array($pro_ads_bs_woo_ajax_image_upload, 'delete_file'));
		
		/* For non logged-in user */
		add_action('wp_ajax_nopriv_buyandsell_upload', array($pro_ads_bs_woo_ajax_image_upload, 'upload'));
		add_action('wp_ajax_nopriv_buyandsell_delete', array($pro_ads_bs_woo_ajax_image_upload, 'delete_file'));
			
		
	    //echo get_post_meta( get_option('buyandsell_woocommerce_product', 0), '_visibility', true );
		/*
		$order = new WC_Order( 1241 );
		
		//echo $order->billing_email; // $order->billing_first_name, $order->billing_last_name
		echo $order->user_id;
		
		$items = $order->get_items(); 
		foreach ($items as $key => $product ) 
		{
			//print_r($product);
			//echo $product['item_meta']['_product_id'][0];
			if( $pro_ads_bs_woo_main->is_proadswoo_product('', $product['item_meta']['_product_id'][0]) )
			{
				echo $product['banner_title'].' '.$product['banner_adzone_id'].' '.$product['banner_price'];
			}
		}*/
		//
		
	}
	
	
	
	
	
	
	
	/*
	 * Admin page Init actions
	 *
	 * @access public
	 * @return null
	*/
	public function admin_actions() 
	{	
		if(is_admin())
		{
			wp_enqueue_script('wp_pro_ads_bs_woo_js_admin_functions', WP_ADS_BS_WOO_TPL_URL.'/js/admin.js');
		}
	}
	
}

endif;
?>