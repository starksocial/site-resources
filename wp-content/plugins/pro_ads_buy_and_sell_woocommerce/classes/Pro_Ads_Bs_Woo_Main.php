<?php
/**
 * Init related functions and actions.
 *
 * @author 		Tunafish
 * @package 	pro_ads_buy_and_sell_woocommerce/classes
 * @version     3.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Pro_Ads_Bs_Woo_Main' ) ) :


class Pro_Ads_Bs_Woo_Main {	

	public function __construct() 
	{
		add_action( 'woocommerce_order_status_completed', array(&$this, 'order_complete_create_banner') );
		add_filter( 'woocommerce_get_item_data', array(&$this, 'get_item_data'), 10, 2 );
		
		add_filter( 'woocommerce_cart_item_thumbnail', array($this, 'cart_item_thumbnail'), 10, 3 );
		add_filter( 'woocommerce_loop_add_to_cart_link', array(&$this, 'add_to_cart_cat_text'), 10, 2 );
		add_filter( 'woocommerce_cart_item_name', array(&$this, 'cart_item_name'), 10, 3);
		
		//add_filter('woocommerce_price_html', array($this, 'cart_item_price_edit'));
	}

	
	
	/**
	 * Check if product is a pro ads woo item.
	 *
	 * @access public
	 * @param array $cart_item, int $product_id
	 * @return bool
	 */
	public function is_proadswoo_product( $cart_item = '', $product_id = '' )
	{
		$is_proadswoo_product = 0;
		
		if( !empty($cart_item) )
		{
			if( !empty($cart_item['proadswoo_data']))
			{
				$ats = get_post_meta( $cart_item['product_id'] , '_product_attributes', true );
				
				if( !empty( $ats['proads_bs_woo'] ))
				{
					$is_proadswoo_product = 1;
				}
			}
		}
		elseif( !empty($product_id))
		{
			$ats = get_post_meta( $product_id, '_product_attributes', true );
				
			if( !empty( $ats['proads_bs_woo'] ))
			{
				$is_proadswoo_product = 1;
			}
		}
		
		return $is_proadswoo_product;
	}
	
	
	
	
	
	
	
	/**
	 * Order Complete - Create banner
	 *
	 * @access public
	 * @param int $order_id
	 * @return null
	 */
	public function order_complete_create_banner( $order_id )
	{
		global $wpdb, $pro_ads_advertisers, $pro_ads_campaigns, $pro_ads_adzones;
		
		$order = new WC_Order( $order_id );
		
		$user_id = $order->user_id;
		$email = $order->billing_email; //$order->billing_first_name, $order->billing_last_name
		$full_name = $order->billing_first_name.' '.$order->billing_last_name;
		
		$items = $order->get_items(); 
		// PRODUCT - $product
		foreach ($items as $key => $product ) 
		{
			if( $this->is_proadswoo_product('', $product['item_meta']['_product_id'][0]) )
			{
				/**
				 * Order Complete - Create Banner
				*/
				// Check WP User
				$user = get_user_by( 'email', $email );
				$wpuser_id = $user_id ? $user_id : '';
				
				$advertiser = $pro_ads_advertisers->get_advertisers( 
					array(
						'meta_key'       => '_proad_advertiser_email',
						'meta_value'     => $email
					)
				);
	
				if( !empty($advertiser) )
				{ 
					$advertiser_id = $advertiser[0]->ID;
				}
				else
				{
					$advertiser_id = wp_insert_post( array('post_status' => 'publish', 'post_type' => 'advertisers', 'post_title' => $full_name, ) );
					update_post_meta( $advertiser_id, '_proad_advertiser_email', $email );
	  				update_post_meta( $advertiser_id, '_proad_advertiser_wpuser', $wpuser_id );
				}
				
				// Create Campaign, if not exists
				$campaign = $pro_ads_campaigns->get_campaigns( 
					array(
						'name'           => $advertiser_id.'-buy-and-sell-campaign',
						'meta_key'       => '_campaign_advertiser_id',
						'meta_value'     => $advertiser_id
					)
				);
				if( !empty($campaign) )
				{ 
					$campaign_id = $campaign[0]->ID;
				}
				else
				{
					$campaign_id = wp_insert_post(array('post_status' => 'publish', 'post_type' => 'campaigns', 'post_title' => $advertiser_id.' Buy and Sell Campaign', 'post_author' => $wpuser_id));
					update_post_meta( $campaign_id, '_campaign_advertiser_id', $advertiser_id );
					update_post_meta( $campaign_id, '_campaign_status', 1 );
				}
				
				
				// Create Banner
				$defaults = array(
					'post_status'           => 'publish', 
					'post_type'             => 'banners',
					'post_author'           => $wpuser_id,
					'post_title'            => $product['banner_title'],
				);
				$banner_id = wp_insert_post( $defaults );
				//echo 'oioi'.$product['banner_img'];
				$path_info = !empty( $product['banner_img_txt'] ) ? pathinfo( $product['banner_img_txt'] ) : '';
				$banner_type = !empty( $path_info['extension'] ) ? $path_info['extension'] : '';
				$banner_size = !empty( $product['banner_img_txt'] ) ? getimagesize($product['banner_img_txt']) : '';
				$size = !empty($banner_size) ? $banner_size[0].'x'.$banner_size[1] : '';
				$banner_status  = !empty($banner_type) ? 1 : 0;
				
				update_post_meta( $banner_id, '_banner_advertiser_id', $advertiser_id );
				update_post_meta( $banner_id, '_banner_campaign_id', $campaign_id );
				update_post_meta( $banner_id, '_banner_url', $product['banner_img_txt'] );
				update_post_meta( $banner_id, '_banner_link', $product['banner_link_txt'] );
				update_post_meta( $banner_id, '_banner_target', '_blank' );
				update_post_meta( $banner_id, '_banner_status', 1 );
				update_post_meta( $banner_id, '_banner_type', $banner_type );
				update_post_meta( $banner_id, '_banner_size', $size );
				//update_post_meta( $banner_id, 'banner_no_follow', $banner_no_follow );
				
				$banner_start_date = get_post_meta( $banner_id, '_banner_start_date', true );
				if( empty( $banner_start_date ) && $banner_status == 1)
				{
					update_post_meta( $banner_id, '_banner_start_date', time() );
				}
				
				$adzone_id = $product['banner_adzone_id'];
				
				// Check contract and duration
				$adzonecontract   = get_post_meta( $adzone_id, 'adzone_buyandsell_contract', true );
				$adzone_duration  = get_post_meta( $adzone_id, 'adzone_buyandsell_duration', true );
				update_post_meta( $banner_id, '_banner_contract', $adzonecontract );
				update_post_meta( $banner_id, '_banner_duration', $adzone_duration );
				
				// LINK banner to adzone
				$pro_ads_adzones->pro_ad_link_banner_to_adzone( $adzone_id, $banner_id );
				
			}
		}
	}
	
	
	
	
	
	
	
	public function add_to_cart_cat_text( $handler, $product ) 
	{
			
		if( $this->is_proadswoo_product('', $product->id) )
		{
			return sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button product_type_%s">%s</a>',
				esc_url( get_permalink($product->ID) ),
				esc_attr( $product->id ),
				esc_attr( $product->get_sku() ),
				esc_attr( $product->product_type ),
				esc_html( __('Create Banner','wpproads') )
			);
		}

		return $handler;
	}
	
	
	
	
	/* 
	 * https://gist.github.com/claudiosmweb/4185057
	*/
	/*
	function cart_item_price_edit() 
	{
		global $post, $pro_ads_adzones;
		
		if( $this->is_proadswoo_product('', $post->ID) )
		{
			$adzone_id = isset($_GET['adzone_id']) && !empty($_GET['adzone_id']) ? $_GET['adzone_id'] : 0;
			$arr = $pro_ads_adzones->get_adzone_data( $adzone_id );
			
			//$product = new WC_Product( $post->ID );
			//$price = $product->price + $arr['adzone_buyandsell_price'];
			
			$pricing = $this->get_price($arr['adzone_buyandsell_price']);
			$contract = $this->get_contract_type($arr['adzone_buyandsell_contract']);
			
			$price = $pricing['price_val'] > 0 ? woocommerce_price($pricing['price_val']) : $pricing['price'];
			$duration = $pricing['price_val'] > 0 ? sprintf(__('per', 'wpproads').' %s %s', $arr['adzone_buyandsell_duration'], strtolower($contract['name_short_multiple'])) : '';
			
			echo $price.' '.$duration;
		}
	}*/
	
	
	
	
	
	function cart_item_thumbnail( $thumb, $cart_item, $cart_item_key ) 
	{
		if( $this->is_proadswoo_product($cart_item) )
		{
			$thumb = '<img src="'.$cart_item['proadswoo_data']['banner_img'].'" />';
			//$thumb = $cart_item['proadswoo_data']['banner_img'];
		}
		
		return $thumb;	
	}
	
	
	
	
	
	public function cart_item_name($title, $cart_item, $cart_item_key)
	{
		if( $this->is_proadswoo_product($cart_item) )
		{
			return $title.': '.$cart_item['proadswoo_data']['banner_title'];
		}
		
		return $title;
	}
	
	
	
	
	
	
	
	
	
	
	
	public function get_item_data( $other_data, $cart_item ) 
	{
		if( $this->is_proadswoo_product($cart_item) )
		{
			array_push($other_data, 
				array(
					'name' => __('Size', 'wpproads'),
					//'value' => '<a href="'.add_query_arg( array('cart_item_key' => $cik), get_permalink($cart_item['product_id']) ).'">'.__('Edit', 'wpproads').'</a>'
					'value' => $cart_item['proadswoo_data']['banner_size']
				),
				array(
					'name' => __('Link', 'wpproads'),
					'value' => $cart_item['proadswoo_data']['banner_link']
				)
				
			);
		}
		
		return $other_data;
	}
	
	
	
	
	
	
	
	
	public function create_product( $args = array() )
	{
		global $wp_error;
		
		$defaults = array(
			'post_author' => 1,
			'post_content' => '',
			'post_status' => "publish",
			'post_title' => 'BuyandSell Ads',
			'post_parent' => '',
			'post_type' => "product",
		);
		
		$post = wp_parse_args( $args, $defaults );
		
		//Create post
		$post_id = wp_insert_post( $post, $wp_error );
		if($post_id)
		{
			$attach_id = get_post_meta($product->parent_id, "_thumbnail_id", true);
			add_post_meta($post_id, '_thumbnail_id', $attach_id);
			
			wp_set_object_terms($post_id, 'simple', 'product_type');
			
			$product_attributes = array(
				'proads_bs_woo'=>array(
					'name'=>'proads_bs_woo',
					'value'=>'1',
					'is_visible' => '0', 
					'is_variation' => '0',
					'is_taxonomy' => '0'
				)
			);
			
			update_post_meta( $post_id, '_visibility', 'hidden' ); // visible
			update_post_meta( $post_id, '_stock_status', 'instock');
			update_post_meta( $post_id, 'total_sales', '0');
			update_post_meta( $post_id, '_downloadable', 'no');
			update_post_meta( $post_id, '_virtual', 'yes');
			update_post_meta( $post_id, '_regular_price', "1" );
			update_post_meta( $post_id, '_sale_price', "1" );
			update_post_meta( $post_id, '_purchase_note', "" );
			update_post_meta( $post_id, '_featured', "no" );
			update_post_meta( $post_id, '_weight', "" );
			update_post_meta( $post_id, '_length', "" );
			update_post_meta( $post_id, '_width', "" );
			update_post_meta( $post_id, '_height', "" );
			update_post_meta( $post_id, '_sku', "");
			update_post_meta( $post_id, '_product_attributes', $product_attributes); // array()
			update_post_meta( $post_id, '_sale_price_dates_from', "" );
			update_post_meta( $post_id, '_sale_price_dates_to', "" );
			update_post_meta( $post_id, '_price', "1" );
			update_post_meta( $post_id, '_sold_individually', "yes" );
			update_post_meta( $post_id, '_manage_stock', "no" );
			update_post_meta( $post_id, '_backorders', "no" );
			update_post_meta( $post_id, '_stock', "" );
			
		}
		
		return $post_id;
	}
	
	
	
	
	
	public function pro_ads_add_to_cart( $product_id )
	{
		global $woocommerce;
			
		$woocommerce->cart->add_to_cart($product_id);
	}
	
	
	



	
	
	
	
	
	public function get_price($nr)
	{
		$cur = 'USD';
		$cur_sign = '$';
		
		$price = $nr > 0 ? $nr : __('Free','wpproads');
		
		$array = array(
			'cur'        => $cur,
			'cur_sign'   => $cur_sign,
			'price'      => $price,
			'price_val'  => $nr
		);
		
		return $array;
	}
	
	
	
	
	
	
	public function get_contract_type($id)
	{
		if($id == 1 )
		{
			$array = array(
				'name' => __('Pay per click','wpproads'),
				'name_short' => __('Click','wpproads'),
				'name_short_multiple' => __('Clicks','wpproads'),
				'slug' => 'click'
			);
		}
		elseif($id == 2)
		{
			$array = array(
				'name' => __('Pay per view','wpproads'),
				'name_short' => __('View','wpproads'),
				'name_short_multiple' => __('Views','wpproads'),
				'slug' => 'view'
			);
		}
		else
		{
			$array = array(
				'name' => __('Duration','wpproads'),
				'name_short' => __('Day','wpproads'),
				'name_short_multiple' => __('Days','wpproads'),
				'slug' => 'duration'
			);
		}
		
		return $array;
	}
	
	
	
	
	
	
	
	/**
	 * Receive the Buy and Sell Woocommerce Product URL
	 *
	 * @access public
	 * @return string
	 */
	public function buyandsell_product_url()
	{
		//$bs_woo_product = get_option('buyandsell_woocommerce_product', 0);
		$bs_woo_product = $this->wpproads_bs_woo_get_option( 'buyandsell_woocommerce_product', 0 );
		$url = $this->wpproads_bs_woo_plugin_is_network_activated() ? get_blog_permalink( BLOG_ID_CURRENT_SITE, $bs_woo_product ) : get_permalink($bs_woo_product);
		return $url;	
	}
	
	
	
	
	
	
	
	/**
	 * Action when posts are deleted
	 *
	 * @access public
	 * @return string
	 */
	public function pro_ads_bs_woo_delete_action($post_id)
	{
		$post_type = get_post_type( $post_id );
		if( $post_type == 'product') 
		{
			$bs_woo_product = get_option('buyandsell_woocommerce_product', 0);
			if( $post_id == $bs_woo_product )
			{
				update_option( 'buyandsell_woocommerce_product', 0);
			}
		}
	}
	
	
	
	
	
	
	/*
	 * Check if the plugin is network activated
	 *
	 * @access public
	 * @return bool
	*/
	public function wpproads_bs_woo_plugin_is_network_activated()
	{
		$active = 0;
		
		if( is_multisite() )
		{
			if( !function_exists( 'is_plugin_active_for_network' ) )
			{
				require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
				// Makes sure the plugin is defined before trying to use it
			}
			 
			if( is_plugin_active_for_network( 'pro_ads_buy_and_sell_woocommerce/pro_ads_buy_and_sell_woocommerce.php' ) ) 
			{
				$active = 1;
			}
		}
		
		return $active;
	}
	
	
	
	
	
	/*
	 * Load site option - get_option() - for multisite installations.
	 *
	 * @access public
	 * @return array/string
	*/
	public function wpproads_bs_woo_get_option( $name, $value = '' )
	{
		global $wpdb;
		
		if( $this->wpproads_bs_woo_plugin_is_network_activated() )
		{
			$option = get_site_option($name, $value);
		}
		else
		{
			$option = get_option($name, $value);
		}
		
		return $option;
	}
	
	
	
	
	
	
	/**
	 * SAVE OPTIONS IN "WP PRO ADVERTISING SYSTEM" DASHBOARD
	 *
	 * @access public
	 * @return string
	 */
	public function wpproads_buyandsell_woo_save_options()
	{
		$product_id = $this->create_product( array('post_title' => 'BuyandSell Ads') );
		update_option( 'buyandsell_woocommerce_product', $product_id );
		$this->wpproads_bs_woo_plugin_is_network_activated() ? update_site_option( 'buyandsell_woocommerce_product', $product_id ) : '';
		
		$notice = __('Buy and Sell WooCommerce Product updated successfully.','wpproads');
	}
	
}

endif;
?>