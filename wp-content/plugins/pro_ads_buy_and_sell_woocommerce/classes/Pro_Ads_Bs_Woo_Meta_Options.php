<?php
class Pro_Ads_Bs_Woo_Meta_Options {	
	
	
	public function __construct() 
	{
		add_action( 'add_meta_boxes', array($this, 'bs_adzone_meta_options'));
		add_action( 'save_post', array($this, 'pro_ads_bs_adzones_meta_options_save_postdata' ));
	}
	
	
	
	
	/*
	 * Adds a box to the main column on the Post and Page edit screens.
	 *
	 * @access public
	*/
	public function bs_adzone_meta_options() 
	{
		$screens = array( 'adzones' );
	
		foreach ( $screens as $screen ) 
		{	
			add_meta_box( 'pro_ads_bs_'.$screen.'_meta_options_id', sprintf(__( 'Buy and Sell %s Options:', 'wpproads' ), $screen), array($this, 'pro_ads_bs_'.$screen.'_meta_options_custom_box'), $screen, 'normal', 'default' );
			
		}
	}
	
	
	
	
	
	function pro_ads_bs_adzones_meta_options_custom_box( $post ) 
	{
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'pro_ads_bs_adzones_meta_options_inner_custom_box', 'pro_ads_bs_adzones_meta_options_inner_custom_box_nonce' );
		
		$adzone_buyandsell_contract        = get_post_meta( $post->ID, 'adzone_buyandsell_contract', true );
		$adzone_buyandsell_duration        = get_post_meta( $post->ID, 'adzone_buyandsell_duration', true );
		$adzone_buyandsell_price           = get_post_meta( $post->ID, 'adzone_buyandsell_price', true );
		$adzone_buyandsell_est_impressions = get_post_meta( $post->ID, 'adzone_buyandsell_est_impressions', true );
		?>
        <div class="tuna_meta">
			<table class="form-table">
				<tbody>
		  			<tr valign="top">
						<th scope="row">
							<?php _e( "Contract", 'wpproads' ); ?>
							<span class="description"><?php _e('Select the contract type and pricing for this adzone.', 'wpproads'); ?></span>
						</th>
						<td>
                            <select id="buyandsell_contract" name="adzone_buyandsell_contract">
                            	<option value="1" <?php echo $adzone_buyandsell_contract == 1 ? 'selected' : ''; ?> txt="<?php _e('Amount of clicks', 'wpproads'); ?>"><?php _e('Pay per click', 'wpproads'); ?></option>
                            	<option value="2" <?php echo $adzone_buyandsell_contract == 2 ? 'selected' : ''; ?> txt="<?php _e('Amount of views', 'wpproads'); ?>"><?php _e('Pay per view', 'wpproads'); ?></option>
                                <option value="3" <?php echo $adzone_buyandsell_contract == 3 ? 'selected' : ''; ?> txt="<?php _e('Amount of days', 'wpproads'); ?>"><?php _e('Duration', 'wpproads'); ?></option>
                          	</select>
                            
                            <span class="description"></span>
						</td>
					</tr>
                    <tr>
                    	<th scope="row">
                            <span class="contract_duration"><?php _e('Amount of clicks', 'wpproads'); ?></span>
                            <span class="description"><?php _e('', 'wpproads'); ?></span>
                        </th>
                        <td>
                        	<input type="text" name="adzone_buyandsell_duration" value="<?php echo !empty($adzone_buyandsell_duration) ? $adzone_buyandsell_duration : ''; ?>" style="width:100px;">
                            <span class="description"></span>
                        </td>
                    </tr>
                    <tr>
                    	<th scope="row">
                            <?php _e('Estimated impressions', 'wpproads'); ?>
                            <span class="description"><?php _e('How many impressions do you expect for this adzone during the contract duration.', 'wpproads'); ?></span>
                        </th>
                        <td>
                        	<input type="text" name="adzone_buyandsell_est_impressions" value="<?php echo !empty($adzone_buyandsell_est_impressions) ? $adzone_buyandsell_est_impressions : ''; ?>" style="width:100px;">
                            <span class="description"></span>
                        </td>
                    </tr>
                    <tr>
                    	<th scope="row">
                            <?php _e('Price', 'wpproads'); ?>
                            <span class="description"><?php _e('The price for this contract.', 'wpproads'); ?></span>
                        </th>
                        <td>
                        	<input type="text" name="adzone_buyandsell_price" value="<?php echo !empty($adzone_buyandsell_price) ? $adzone_buyandsell_price : ''; ?>" style="width:100px;">
                            <span class="description"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <?php
	}
	
	
	
	
	
	
	function pro_ads_bs_adzones_meta_options_save_postdata( $post_id ) 
	{
		// Check if our nonce is set.
		if ( ! isset( $_POST['pro_ads_bs_adzones_meta_options_inner_custom_box_nonce'] ) )
		return $post_id;
		$nonce = $_POST['pro_ads_bs_adzones_meta_options_inner_custom_box_nonce'];
		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'pro_ads_bs_adzones_meta_options_inner_custom_box' ) )
		  return $post_id;
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		  return $post_id;
		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) )
			return $post_id;
		} else {
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return $post_id;
		}
		/* OK, its safe for us to save the data now. */
			
		// Sanitize user input.
		$adzone_buyandsell_contract         = sanitize_text_field( $_POST['adzone_buyandsell_contract'] );
		$adzone_buyandsell_duration         = sanitize_text_field( $_POST['adzone_buyandsell_duration'] );
		$adzone_buyandsell_price            = sanitize_text_field( $_POST['adzone_buyandsell_price'] );
		$adzone_buyandsell_est_impressions  = sanitize_text_field( $_POST['adzone_buyandsell_est_impressions'] );
		
		// Update the meta field in the database.
		update_post_meta( $post_id, 'adzone_buyandsell_contract', $adzone_buyandsell_contract );
		update_post_meta( $post_id, 'adzone_buyandsell_duration', $adzone_buyandsell_duration );
		update_post_meta( $post_id, 'adzone_buyandsell_price', $adzone_buyandsell_price );
		update_post_meta( $post_id, 'adzone_buyandsell_est_impressions', $adzone_buyandsell_est_impressions );
	}
	
}