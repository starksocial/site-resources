/*
 * AJAX UPLOADER
*/


function load_ajax_upload(ajaxurl){
jQuery(function($){
	
    var buyandsell_Upload = {
        init:function () {
			
            window.buyandsellUploadCount = typeof(window.buyandsellUploadCount) == 'undefined' ? 0 : window.buyandsellUploadCount;
            this.maxFiles = parseInt(buyandsell_upload.number);

            $('#buyandsell-upload-imagelist').on('click', 'a.action-delete', this.removeUploads);

            this.attach();
            this.hideUploader();
        },
        attach:function () {
            // wordpress plupload if not found
            if (typeof(plupload) === 'undefined') {
                return;
            }

            if (buyandsell_upload.upload_enabled !== '1') {
                return
            }

            var uploader = new plupload.Uploader(buyandsell_upload.plupload);

            $('#buyandsell-uploader').click(function (e) {
                uploader.start();
                // To prevent default behavior of a tag
                e.preventDefault();
            });

            //initilize  wp plupload
            uploader.init();

            uploader.bind('FilesAdded', function (up, files) {
                $.each(files, function (i, file) {
                    $('#buyandsell-upload-imagelist').append(
                        '<div id="' + file.id + '">' +
                            file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                            '</div>');
                });

                up.refresh(); // Reposition Flash/Silverlight
                uploader.start();
            });

            uploader.bind('UploadProgress', function (up, file) {
                $('#' + file.id + " b").html(file.percent + "%");
            });

            // On erro occur
            uploader.bind('Error', function (up, err) {
                $('#buyandsell-upload-imagelist').append("<div>Error: " + err.code +
                    ", Message: " + err.message +
                    (err.file ? ", File: " + err.file.name : "") +
                    "</div>"
                );

                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                var result = $.parseJSON(response.response);
                $('#' + file.id).remove();
                if (result.success) {
                    window.buyandsellUploadCount += 1;
                    $('#buyandsell-upload-imagelist ul').append(result.html);
					$('#proadswoo_custom_data').append(result.form);

                    buyandsell_Upload.hideUploader();
                }
            });


        },

        hideUploader:function () {

            if (buyandsell_Upload.maxFiles !== 0 && window.buyandsellUploadCount >= buyandsell_Upload.maxFiles) {
                $('#buyandsell-uploader').hide();
            }
        },

        removeUploads:function (e) {
            e.preventDefault();

            if (confirm(buyandsell_upload.confirmMsg)) {

                var el = $(this),
                    data = {
                        'attach_id':el.data('upload_id'),
                        'nonce':buyandsell_upload.remove,
                        'action':'buyandsell_delete'
                    };

                $.post(buyandsell_upload.ajaxurl, data, function () {
                    el.parent().remove();

                    window.buyandsellUploadCount -= 1;
                    if (buyandsell_Upload.maxFiles !== 0 && window.buyandsellUploadCount < buyandsell_Upload.maxFiles) {
                        $('#buyandsell-uploader').show();
                    }
                });
            }
        }

    };
	
	buyandsell_Upload.init();
	
	
});

}