<?php
/*
 * AJAX REQUEST FUNCTIONS
 *
 * http://codex.wordpress.org/AJAX_in_Plugins
 * For not logged-in users use: add_action('wp_ajax_nopriv_my_action', 'my_action_callback');
*/


add_action('wp_ajax_nopriv_buyandsell_woo_popup_ajax_content', 'buyandsell_woo_popup_ajax_content_callback');
add_action('wp_ajax_buyandsell_woo_popup_ajax_content', "buyandsell_woo_popup_ajax_content_callback");
function buyandsell_woo_popup_ajax_content_callback()
{
	global $pro_ads_bs_woo_templates;
	
	//echo $pro_ads_bs_woo_templates->buy_and_sell_woo_order_content($_POST['adzone_id']);
	echo json_encode( array( 'content' => $pro_ads_bs_woo_templates->buy_and_sell_woo_order_content($_POST['adzone_id']), 'options' => $pro_ads_bs_woo_templates->extend_product_options_form($_POST['adzone_id'])) );
	
	exit;
}


?>