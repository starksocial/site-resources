<?php
/**
 * Plugin Name: WP Pro Ad System Add-On Buy and Sell Woocemmerce
 * Plugin URI: http://tunasite.com/wp-pro-advertising-system/
 * Description: Pro Ad System Add-On to sell advertisement spots on your website using Woocommerce.
 * Version: 1.0.6
 * Author: Tunafish
 * Author URI: http://www.tunasite.com
 * Requires at least: 3.8
 * Tested up to: 4.1
 *
 * Text Domain: wpproads
 * Domain Path: /localization/
 *
 * @package Wp_Pro_Ads_Buy_And_Sell_Woocommerce
 * @category Core
 * @author Tunafish
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Wp_Pro_Ads_Buy_And_Sell_Woocommerce' ) ) :

final class Wp_Pro_Ads_Buy_And_Sell_Woocommerce
{
	/**
	 * @var string
	 */
	public $version = '1.0.6';
	
	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;
	
	
	/**
	 * Main Wp_Pro_Ads_Buy_And_Sell Instance
	 *
	 * Ensures only one instance of Wp_Pro_Ads_Buy_And_Sell is loaded or can be loaded.
	 *
	 * @since 3.0.0
	 * @static
	 * @see PAS()
	 * @return Wp_Pro_Ads_Buy_And_Sell - Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	
	
	
	public function __construct() 
	{
		// Define constants
		$this->define_constants();
		
		// Classes ------------------------------------------------------------
		require_once( WP_ADS_BS_WOO_DIR .'classes/Pro_Ads_Bs_Woo_Init.php');
		
		
		/* ----------------------------------------------------------------
		 * Set Classes
		 * ---------------------------------------------------------------- */
		$pro_ads_bs_woo_init = new Pro_Ads_Bs_Woo_Init();
	}
	
	
	private function define_constants() 
	{
		define( 'WP_ADS_BS_WOO_VERSION', $this->version );
		
		define( 'WP_ADS_BS_WOO_FILE', __FILE__ );
		
		define( 'WP_ADS_BS_WOO_URL', WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)) );
		define( 'WP_ADS_BS_WOO_DIR', ABSPATH. 'wp-content/plugins/' .str_replace(basename( __FILE__),"",plugin_basename(__FILE__)) );
		
		define( 'WP_ADS_BS_WOO_INC_URL', WP_ADS_BS_WOO_URL. 'includes' );
		define( 'WP_ADS_BS_WOO_INC_DIR', WP_ADS_BS_WOO_DIR. 'includes' );
		define( 'WP_ADS_BS_WOO_TPL_URL', WP_ADS_BS_WOO_URL. 'templates' );
		define( 'WP_ADS_BS_WOO_TPL_DIR', WP_ADS_BS_WOO_DIR. 'templates' );
		define( 'WP_ADS_BS_WOO_PLUGIN_SLUG', basename(dirname(__FILE__)) );
		
		define( 'WP_ADS_BS_WOO_ROLE_ADMIN', 'add_users' );
		define( 'WP_ADS_BS_WOO_ROLE_USER', 'read' );
		
		load_plugin_textdomain( 'wpproads', false, plugin_basename( dirname( __FILE__ ) ) . '/localization' );
	}
}

endif;

/**
 * Returns the main instance of PASBS to prevent the need to use globals.
 *
 * @since  3.0.0
 * @return Wp_pro_ad_system
 */
function PASBSWOO() {
	return Wp_Pro_Ads_Buy_And_Sell_Woocommerce::instance();
}

PASBSWOO();
?>