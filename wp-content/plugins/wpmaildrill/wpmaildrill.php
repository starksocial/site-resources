<?php

if (!defined('ABSPATH'))
    die('Direct access of plugin file not allowed');

/*
  Plugin Name: WPMailDrill
  Plugin URI: http://www.WPMailDrill.com
  Description: Easily convert your Wordpress site to send emails with Mandrill.
  Version: 1.0.0
  Author: Codethena Software Ltd.
  Author URI: http://www.Codethena.com
 */

if (!defined('WPMAILDRILL_PLUGIN_FILE')) {
    define('WPMAILDRILL_PLUGIN_FILE', __FILE__);
}

require_once 'class/install.php';
require_once 'class/AJAX.php';



add_action('wp_ajax_wpmaildrill_load_mandrill_info', 'wpmaildrill_AJAX::wpmaildrill_load_mandrill_info');
add_action('wp_ajax_wpmaildrill_load_mandrill_stats', 'wpmaildrill_AJAX::wpmaildrill_load_mandrill_stats');



add_action('admin_menu', 'wpmaildrill_menu');

function wpmaildrill_menu() {
    add_menu_page("WPMailDrill", "WPMailDrill", 'edit_posts', "wpmaildrill_about", "wpmaildrill_about", plugins_url("/images/wpmaildrill_menu_icon.png", __FILE__));
    add_submenu_page("wpmaildrill_about", "WPMailDrill - About", "About", 'edit_posts', "wpmaildrill_about", "wpmaildrill_about");
    add_submenu_page("wpmaildrill_about", "WPMailDrill - Settings", "Settings", 'edit_posts', "wpmaildrill_settings", "wpmaildrill_settings");
    add_submenu_page("wpmaildrill_about", "WPMailDrill - Sent Email Log", "Sent Email Log", 'edit_posts', "wpmaildrill_sent_email_log", "wpmaildrill_sent_email_log");
    add_submenu_page("wpmaildrill_about", "WPMailDrill - Analytics", "Analytics", 'edit_posts', "wpmaildrill_analytics", "wpmaildrill_analytics");
}

function wpmaildrill_about() {
    include 'admin/wpmaildrill_about.php';
}

function wpmaildrill_settings() {
    include 'admin/wpmaildrill_settings.php';
}

function wpmaildrill_analytics() {
    include 'admin/wpmaildrill_analytics.php';
}

function wpmaildrill_sent_email_log() {
    include 'admin/wpmaildrill_sent_email_log.php';
}

if (!function_exists('wp_mail')) {

    function wp_mail($to, $subject, $message, $headers = '', $attachments = array()) {
        include_once(plugin_dir_path(__FILE__) . 'class/core.php');
        $core = new wpmaildrill_core();
        return $core->send_email($to, $subject, $message, $headers, $attachments);
    }

}
?>