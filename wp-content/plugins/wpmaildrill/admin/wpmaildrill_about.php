<link rel="stylesheet" type="text/css" href="<?php echo(plugins_url('/styles/wpmaildrill_about_blocks.css', __FILE__)); ?>">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<div class="wrap">
    <?php
    if (!defined('ABSPATH'))
        die('Direct access of plugin file not allowed');

    global $wpdb;

    $table_name_wpmaildrill_email_send_log = $wpdb->prefix . "wpmaildrill_email_send_log";
    $number_of_emails = $wpdb->get_var("SELECT count(*) FROM $table_name_wpmaildrill_email_send_log");
    $number_of_emails_today = $wpdb->get_var("SELECT count(*) FROM $table_name_wpmaildrill_email_send_log WHERE DATE(sent_date) = DATE(NOW())");
    $number_of_emails_hour = $wpdb->get_var("SELECT count(*) FROM $table_name_wpmaildrill_email_send_log WHERE sent_date >= DATE_SUB(NOW(),INTERVAL 1 HOUR)");
    ?>
    <img src="<?php echo(plugins_url('../images/wpmaildrill_header_logo.png', __FILE__)); ?>" style="height:26px; margin-right:5px; float:left;padding-top:8px" >
    <h2> - About</h2>



    <?php include 'wpmaildrill_admin_menu.php'; ?>



    <div class="panel-area">
        <div class="panel panel-blue">
            <div class="panel-heading">
                <div class="panel-icon">
                    <i class="fa fa-plug fa-5x"></i>
                </div>
                <div class="panel-info">
                    <div class="panel-headline"><?php echo(esc_html(get_option('wpmaildrill_version'))); ?></div>
                    <div class="panel-subhead">WPMailDrill Version</div>
                </div>
            </div>
            <a href="admin.php?page=wpmaildrill_about">
                <div class="panel-footer">
                    <div class="footer-text">
                        View Details
                        <i class="fa fa-arrow-circle-right"></i>
                    </div>
                </div>
            </a>
        </div>

        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="panel-icon">
                    <i class="fa fa-envelope-o fa-5x"></i>
                </div>
                <div class="panel-info">
                    <div class="panel-headline"><?php echo(esc_html($number_of_emails)); ?></div>
                    <div class="panel-subhead">Total emails sent</div>
                </div>
            </div>
            <a href="admin.php?page=wpmaildrill_analytics">
                <div class="panel-footer">
                    <div class="footer-text">
                        View Details
                        <i class="fa fa-arrow-circle-right"></i>
                    </div>
                </div>
            </a>
        </div>


        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="panel-icon">
                    <i class="fa fa-sun-o fa-5x"></i>
                </div>
                <div class="panel-info">
                    <div class="panel-headline"><?php echo(esc_html($number_of_emails_today)); ?></div>
                    <div class="panel-subhead">Sent Today</div>
                </div>
            </div>
            <a href="admin.php?page=wpmaildrill_analytics">
                <div class="panel-footer">
                    <div class="footer-text">
                        View Details
                        <i class="fa fa-arrow-circle-right"></i>
                    </div>
                </div>
            </a>
        </div>

        <div class="panel panel-orange panel-last">
            <div class="panel-heading">
                <div class="panel-icon">
                    <i class="fa fa-clock-o fa-5x"></i>
                </div>
                <div class="panel-info">
                    <div class="panel-headline"><?php echo(esc_html($number_of_emails_hour)); ?></div>
                    <div class="panel-subhead">Sent In Last Hour</div>
                </div>
            </div>
            <a href="admin.php?page=wpmaildrill_analytics">
                <div class="panel-footer">
                    <div class="footer-text">
                        View Details
                        <i class="fa fa-arrow-circle-right"></i>
                    </div>
                </div>
            </a>
        </div>



    </div>
    <div class="clear"></div>
    <div id ="poststuff">
        <div id="post-body" class="metabox-holder columns-1">
            <div id="postbox-container-2" class="postbox-container">
                <div class="postbox">
                    <h3 class="hndle"><span>Thank you</span></h3>
                    <div class="inside">
                        <p>Thank you for purchasing WPMailDrill, we really appreciate your support.</p>
                    </div>
                </div>

                <div class="postbox">
                    <h3 class="hndle"><span>Getting Started</span></h3>
                    <div class="inside">
                        <p>It's easy to get up WPMailDrill up and running, we try to configure as much so you don't have to but there are still a couple of things to do first.</p>
                        <p>A guide to getting up and running is available at <a href="http://www.wpmaildrill.com/documentation/setup/initial-set-up/">www.wpmaildrill.com/documentation/setup/initial-set-up</a>.</p>
                        <p>For support with anything else you can visit <a href="http://www.wpmaildrill.com/documentation/">www.wpmaildrill.com/documentation</a> which has many more support pages available.</p>
                    </div>
                </div>

                <div class="postbox">
                    <h3 class="hndle"><span>WPMailDrill Information</span></h3>
                    <div class="inside">
                        <table class="form-table" style="clear:left;">
                            <tbody>

                                <tr valign="top">
                                    <th scope="row" style="width: 220px">Version</th>
                                    <td> 
                                        <?php echo(esc_html(get_option('wpmaildrill_version'))); ?>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" style="width: 220px">Last Update Date</th>
                                    <td> 
                                        <?php echo(esc_html(get_option('wpmaildrill_last_update'))); ?>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" style="width: 220px">WPMailDrill Website</th>
                                    <td> 
                                        <a href="http://www.WPMailDrill.com">www.WPMailDrill.com</a>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" style="width: 220px">WPMailDrill Documentation</th>
                                    <td> 
                                        <a href="http://www.wpmaildrill.com/documentation/">www.wpmaildrill.com/documentation</a>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" style="width: 220px">WPMailDrill Support</th>
                                    <td> 
                                        <a href="http://codethena.freshdesk.com/">codethena.freshdesk.com</a>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" style="width: 220px">Author</th>
                                    <td> 
                                        Codethena Software Ltd.
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" style="width: 220px">Author Website</th>
                                    <td> 
                                        <a href="http://www.Codethena.com">www.Codethena.com</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>         

                </div>
            </div>
        </div>