
<div class="wrap">

    <?php
    if (!defined('ABSPATH'))
        die('Direct access of plugin file not allowed');

    if (isset($_POST['UpdatewpmaildrillOptions']) && $_POST['UpdatewpmaildrillOptions'] == "Save Changes") {
        check_admin_referer('wpmaildrill_settings_edit');
        update_option('wpmaildrill_mandrill_apikey', sanitize_text_field($_POST["wpmaildrill_mandrill_apikey"]));
        update_option('wpmaildrill_mandrill_active', sanitize_text_field($_POST["wpmaildrill_mandrill_active"]));
        update_option('wpmaildrill_store_sent_emails', sanitize_text_field($_POST["wpmaildrill_store_sent_emails"]));
        update_option('wpmaildrill_store_raw_data', sanitize_text_field($_POST["wpmaildrill_store_raw_data"]));
        update_option('wpmaildrill_track_opens', sanitize_text_field($_POST["wpmaildrill_track_opens"]));
        update_option('wpmaildrill_track_clicks', sanitize_text_field($_POST["wpmaildrill_track_clicks"]));
    }
    ?>
    <img src="<?php echo(plugins_url('../images/wpmaildrill_header_logo.png', __FILE__)); ?>" style="height:26px; margin-right:5px; float:left;padding-top:8px" >
    <h2> - Settings</h2>
    <?php include 'wpmaildrill_admin_menu.php'; ?>



    <form action="" method="post">
        <?php wp_nonce_field('wpmaildrill_settings_edit'); ?>
        <div id ="poststuff">
            <div id="post-body" class="metabox-holder columns-2">
                <div id="postbox-container-1" class="postbox-container">
                    <div id="side-sortables" class="meta-box-sortables ui-sortable">
                        <div class="postbox ">
                            <h3 class="hndle"><span>Save</span></h3>
                            <div class="inside">
                                <div class="submitbox" id="submitpost">
                                    <div class="clear"></div>
                                    <div id="misc-publishing-actions">
                                        <input type="submit" name="UpdatewpmaildrillOptions" value="Save Changes" class="button button-primary" style="float:right;"/>
                                        <?php
                                        if (isset($_POST['UpdatewpmaildrillOptions']) && $_POST['UpdatewpmaildrillOptions'] == "Save Changes") {
                                            echo("Settings saved.");
                                        }
                                        ?>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="postbox-container-2" class="postbox-container">
                    <?php
                    if (isset($_POST['SendTestEmail']) && $_POST['SendTestEmail'] == "Send Test Email") {
                        ?>
                        <div class="postbox">
                            <h3 class="hndle"><span>Test Email Result</span></h3>
                            <div class="inside">

                                <?php
                                $email = $_POST["wpmaildrill_test_email_address"];

                                $email_result = wp_mail(sanitize_email($email), "Test: WPMailDrill ", "this is a test email from WPMailDrill", "Content-type: text/html");
                                if ($email_result) {
                                    ?>
                                    <p>The test email has been sent.</p>
                                    <?php
                                } else {
                                    ?> <p>Sorry but the test email cannot be sent, please check your Wordpress configuration.</p> 
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="postbox">
                        <h3 class="hndle"><span>Email Settings</span></h3>
                        <div class="inside">
                            <p>Changing these values will allow you to configure your Gift List and how it is displayed to your guests, any changes you make will be instant and irreversible so please be careful changing any values.</p> 

                            <table class="form-table" style="clear:left;">
                                <tbody>
                                    <tr valign="top">
                                        <th scope="row">Mandrill Key</th>
                                        <td>                <input type="text" name="wpmaildrill_mandrill_apikey" class="widefat"
                                                                   value="<?php echo(esc_html(get_option('wpmaildrill_mandrill_apikey'))); ?>"></td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row">WPMailDrill Mode</th>
                                        <td> 
                                            <select name="wpmaildrill_mandrill_active">
                                                <option value="1" <?php
                                                if (get_option('wpmaildrill_mandrill_active') == '1') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?> >All emails sent through Mandrill</option>
                                                <option value="0" <?php
                                                if (get_option('wpmaildrill_mandrill_active') == '0') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?>>All emails sent through wp_mail</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row">Track when emails are opened?</th>
                                        <td>
                                            <select name="wpmaildrill_track_opens">
                                                <option value="1" <?php
                                                if (get_option('wpmaildrill_track_opens') == '1') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?> >Yes</option>
                                                <option value="0" <?php
                                                if (get_option('wpmaildrill_track_opens') == '0') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?>>No</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr valign="top">
                                        <th scope="row">Track when links in emails are clicked?</th>
                                        <td>
                                            <select name="wpmaildrill_track_clicks">
                                                <option value="1" <?php
                                                if (get_option('wpmaildrill_track_clicks') == '1') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?> >Yes</option>
                                                <option value="0" <?php
                                                if (get_option('wpmaildrill_track_clicks') == '0') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?>>No</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row">Store Sent Emails?</th>
                                        <td>
                                            <select name="wpmaildrill_store_sent_emails">
                                                <option value="1" <?php
                                                if (get_option('wpmaildrill_store_sent_emails') == '1') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?> >Yes</option>
                                                <option value="0" <?php
                                                if (get_option('wpmaildrill_store_sent_emails') == '0') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?>>No</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row">Store Raw Email Data?</th>
                                        <td>
                                            <select name="wpmaildrill_store_raw_data">
                                                <option value="1" <?php
                                                if (get_option('wpmaildrill_store_raw_data') == '1') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?> >Yes</option>
                                                <option value="0" <?php
                                                if (get_option('wpmaildrill_store_raw_data') == '0') {
                                                    echo('Selected="Selected"');
                                                }
                                                ?>>No</option>
                                            </select>


                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="postbox">
                        <h3 class="hndle"><span>Send Test Email</span></h3>
                        <div class="inside">
                            <p></p> 

                            <table class="form-table" style="clear:left;">
                                <tbody>

                                    <tr valign="top">
                                        <th scope="row">Email Address</th>
                                        <td> 
                                            <div class="widefat">
                                                <input type="text" name="wpmaildrill_test_email_address" style="width:34%"
                                                       value="">

                                            </div> 
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row">Send</th>
                                        <td> 
                                            <div class="widefat">
                                                <input type="submit" name="SendTestEmail" value="Send Test Email" class="button button-primary" style="float:left;"/>

                                            </div> 
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>