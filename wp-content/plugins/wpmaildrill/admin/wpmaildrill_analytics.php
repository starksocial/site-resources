<?php
if (!defined('ABSPATH')) {
    die('Direct access of plugin file not allowed');
}

wp_nonce_field('wpmaildrill_analytics_page');

wp_enqueue_script('jquery');
wp_register_script('wpmaildrill_datatables', plugins_url('../js/datatables/jquery.dataTables.min.js', __FILE__));
wp_enqueue_script('wpmaildrill_datatables');

wp_register_style('wpmaildrill_datatables_css', plugins_url('../styles/datatables/jquery.dataTables.min.css', __FILE__));
wp_enqueue_style('wpmaildrill_datatables_css');

wp_register_script('wpmaildrill_flot', plugins_url('../js/flot/jquery.flot.min.js', __FILE__));
wp_enqueue_script('wpmaildrill_flot');
wp_register_script('wpmaildrill_flot_time', plugins_url('../js/flot/jquery.flot.time.min.js', __FILE__));
wp_enqueue_script('wpmaildrill_flot_time');

wp_register_script('wpmaildrill_analytics', plugins_url('js/wpmaildrill_analytics.js', __FILE__));
wp_enqueue_script('wpmaildrill_analytics');

add_thickbox();

global $wpdb;
$table_name_wpmaildrill_email_send_log = $wpdb->prefix . "wpmaildrill_email_send_log";


if (isset($_POST['delete_attempt']) && $_POST['delete_attempt'] == "Delete") {
    
}
$query = "SELECT * FROM $table_name_wpmaildrill_email_send_log ORDER BY id desc";
$result = $wpdb->get_results($query);
?>
<div class="wrap">
    <img src="<?php echo(plugins_url('../images/wpmaildrill_header_logo.png', __FILE__)); ?>" style="height:26px; margin-right:5px; float:left;padding-top:8px" >
    <h2> - Analytics</h2>
    <?php include 'wpmaildrill_admin_menu.php'; ?>

    <div id ="poststuff">
        <div id="post-body" class="metabox-holder columns-1">
            <div class="postbox-container-1" class="postbox-container">

                <div class="postbox" id="postbox-error" style="display:none">
                    <h3 class="hndle"><span>WPMandrill Analytics Error</span></h3>
                    <div class="inside">
                        <p>Please ensure you have provided a valid API key in the settings page before viewing analytics.</p>

                    </div>
                </div>


                <div class="postbox" id="postbox-analytics" style="display:none">
                    <h3 class="hndle"><span>WPMandrill Analytics</span></h3>
                    <div class="inside" style="height:550px">
                        <div id="choices-container" style="float:right; width:20%;">
                            <h3>
                                Date Range
                                <p>From: <input type="date" name="wpmaildrill_stats_from" id="wpmaildrill_stats_from" value="<?php echo(esc_html(date("Y-m-d", strtotime("-1 month")))); ?>"></p>
                                <p>To:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="date" name="wpmaildrill_stats_to" id="wpmaildrill_stats_to" value="<?php echo(esc_html(date("Y-m-d"))); ?>"></p>
                                <a href="#" onclick="updateDates();">Update Dates</a> <p id="analytics_no_data_message" style="display:none;">Sorry there was no data available.</p>
                            </h3>
                            <h3>Data</h3>
                            <div id="choices" ></div>
                        </div>
                        <br/>
                        <div id="chart_placeholder" style="float:left; width:80%;height:500px"></div>

                    </div>
                </div>

            </div>


        </div>
    </div>

</div>


