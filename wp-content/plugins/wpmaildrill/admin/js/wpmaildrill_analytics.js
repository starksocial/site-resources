(function ($) {

    $(document).ready(function () {
        load_mandrill_stats();
    });

})(jQuery);



function load_mandrill_stats() {
    (function ($) {

        $.post(ajaxurl, {
            action: 'wpmaildrill_load_mandrill_stats',
            _wpnonce: $('#_wpnonce').val(),
            from: $("#wpmaildrill_stats_from").val(),
            to: $("#wpmaildrill_stats_to").val(),
            dataType: "json"}, function (response) {

            var data = jQuery.parseJSON(response);
            if (data["error"] === true)
            {
                $('#postbox-error').show();
            }
            else
            {
                $('#postbox-analytics').show();

                var datasets = data_to_dataset(data);

                var i = 0;
                $.each(datasets, function (key, val) {
                    val.color = i;
                    ++i;
                });

                // insert checkboxes 
                var choiceContainer = $("#choices");
                $.each(datasets, function (key, val) {
                    choiceContainer.append("<br/><input type='checkbox' name='" + key +
                            "' checked='checked' id='id" + key + "'></input>" +
                            "<label for='id" + key + "'>"
                            + val.label + "</label>");
                });

                choiceContainer.find("input").click(function () {
                    plotAccordingToChoices(choiceContainer, datasets);
                });

                plotAccordingToChoices(choiceContainer, datasets);
            }
        });


    })(jQuery);
}

function plotAccordingToChoices(choiceContainer, datasets) {
    (function ($) {
        var data = [];
        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && [key]) {
                data.push(datasets[key]);
            }
        });

        if (data.length > 0) {
            $.plot("#chart_placeholder", data, {
                yaxis: {
                    min: 0
                },
                xaxis: {
                    mode: "time",
                    timeformat: "%b %d <br> %H:%M"
                },
                series: {
                    lines: {show: true},
                    points: {show: true}
                }
            });
        }
    })(jQuery);
}
function data_to_dataset(data)
{

    if (data["stats"].length == 0)
    {
        jQuery("#analytics_no_data_message").show();
    }
    else
    {
        jQuery("#analytics_no_data_message").hide();
    }
    var d1 = [];
    var d2 = [];
    var d3 = [];
    var d4 = [];
    var d5 = [];
    var d6 = [];
    var d7 = [];
    for (var i = 0; i < data["stats"].length; i++) {
        var date_of_stat = (Date.parse(data["stats"][i]["time"]));
        d1.push([date_of_stat, data["stats"][i]["sent"]]);
        d2.push([date_of_stat, data["stats"][i]["opens"]]);
        d3.push([date_of_stat, data["stats"][i]["clicks"]]);
        d4.push([date_of_stat, data["stats"][i]["hard_bounces"]]);
        d5.push([date_of_stat, data["stats"][i]["soft_bounces"]]);
        d6.push([date_of_stat, data["stats"][i]["rejects"]]);
        d7.push([date_of_stat, data["stats"][i]["complaints"]]);
    }

    var datasets = {
        "sent": {
            label: "Sent",
            data: d1
        },
        "opens": {
            label: "Opens",
            data: d2
        },
        "clicks": {
            label: "Clicks",
            data: d3
        },
        "hard_bounces": {
            label: "Hard Bounces",
            data: d4
        },
        "soft_bounces": {
            label: "Soft Bounces",
            data: d5
        },
        "rejects": {
            label: "Rejects",
            data: d6
        },
        "complaints": {
            label: "Complaints",
            data: d7
        }

    };

    return datasets;
}

function updateDates() {
    (function ($) {

        $.post(ajaxurl, {
            action: 'wpmaildrill_load_mandrill_stats',
            _wpnonce: $('#_wpnonce').val(),
            from: $("#wpmaildrill_stats_from").val(),
            to: $("#wpmaildrill_stats_to").val(),
            dataType: "json"},
        function (response) {
            var data = jQuery.parseJSON(response);
            if (data["error"] === true)
            {
                $('#postbox-error').show();
            }
            else
            {
                $('#postbox-analytics').show();
                var datasets = data_to_dataset(data);
                var i = 0;
                $.each(datasets, function (key, val) {
                    val.color = i;
                    ++i;
                });
                var choiceContainer = $("#choices");
                choiceContainer.find("input").click(function () {
                    plotAccordingToChoices(choiceContainer, datasets);
                });
                plotAccordingToChoices(choiceContainer, datasets);
            }
        });
    })(jQuery);
}