(function ($) {

    $(document).ready(function () {

        $('#wpmaildrill_sent_emails').dataTable({
            "order": [[0, "desc"]]
        });

    });

})(jQuery);

function load_mandrill_info(messageId, nonce) {
    (function ($) {
        $("#latest_info_modal_inside").empty();
        $("#latest_info_modal_inside").html("Loading");
        $.post(ajaxurl, {action: 'wpmaildrill_load_mandrill_info', messageId: messageId, _wpnonce: nonce, dataType: "json"}, function (response) {

            var data = jQuery.parseJSON(response);
            var table = "";
            if (data["error"] === true)
            {
                table += "Sorry there is no data yet, please try again later otherwise please check your Mandrill API key is correct.";
            }
            else
            {

                table += "<table class=\"display widefat\"><thead><tr><th>Item</th><th>Value</th></tr></thead><tbody>";
                table += ("<tr><td>ID</td><td>" + data["_id"] + "</td></tr>");
                table += ("<tr class=\"alt\" ><td>State</td><td>" + data["state"] + "</td></tr>");
                table += ("<tr><td>Opens</td><td>" + data["opens"] + "</td></tr>");
                table += ("<tr class=\"alt\"><td>Clicks</td><td>" + data["clicks"] + "</td></tr>");

                var alt = false;
                for (var i = 0; i < data["opens_detail"].length; i++) {

                    var alt_text = "";
                    if (alt)
                    {
                        alt_text = "class=\"alt\"";
                    }
                    else
                    {
                        alt = true;
                    }
                    table += ("<tr " + alt_text + "><td>Open Details " + (i + 1) + "</td><td><table style=\"background-color: #ffffff;\">");
                    table += ("<tr><td>Time Stamp</td><td>" + data["opens_detail"][i]["ts"] + "</td></tr>");
                    table += ("<tr class=\"alt\"><td>IP</td><td>" + data["opens_detail"][i]["ip"] + "</td></tr>");
                    table += ("<tr><td>Location</td><td>" + data["opens_detail"][i]["location"] + "</td></tr>");
                    table += ("<tr class=\"alt\"><td>User Agent</td><td>" + data["opens_detail"][i]["ua"] + "</td></tr>");
                    table += ("</table></td></tr>");
                }

                for (var i = 0; i < data["clicks_detail"].length; i++) {

                    var alt_text = "";
                    if (alt)
                    {
                        alt_text = "class=\"alt\"";
                    }
                    else
                    {
                        alt = true;
                    }
                    table += ("<tr " + alt_text + "><td>Open Details " + (i + 1) + "</td><td><table style=\"background-color: #ffffff;\">");
                    table += ("<tr><td>Time Stamp</td><td>" + data["clicks_detail"][i]["ts"] + "</td></tr>");
                    table += ("<tr  class=\"alt\"><td>URL</td><td>" + data["clicks_detail"][i]["url"] + "</td></tr>");
                    table += ("<tr><td>IP</td><td>" + data["clicks_detail"][i]["ip"] + "</td></tr>");
                    table += ("<tr class=\"alt\"><td>Location</td><td>" + data["clicks_detail"][i]["location"] + "</td></tr>");
                    table += ("<tr><td>User Agent</td><td>" + data["clicks_detail"][i]["ua"] + "</td></tr>");
                    table += ("</table></td></tr>");
                }


                for (var i = 0; i < data["smtp_events"].length; i++) {

                    var alt_text = "";
                    if (alt)
                    {
                        alt_text = "class=\"alt\"";
                    }
                    else
                    {
                        alt = true;
                    }
                    table += ("<tr " + alt_text + "><td>SMTP Events " + (i + 1) + "</td><td><table style=\"background-color: #ffffff;\">");
                    table += ("<tr><td>Time Stamp</td><td>" + data["smtp_events"][i]["ts"] + "</td></tr>");
                    table += ("<tr  class=\"alt\"><td>Type</td><td>" + data["smtp_events"][i]["type"] + "</td></tr>");
                    table += ("<tr><td>Diagnostics</td><td>" + data["smtp_events"][i]["diag"] + "</td></tr>");
                    table += ("<tr class=\"alt\"><td>Source IP</td><td>" + data["smtp_events"][i]["source_ip"] + "</td></tr>");
                    table += ("<tr><td>Destination IP</td><td>" + data["smtp_events"][i]["destination_ip"] + "</td></tr>");
                    table += ("<tr class=\"alt\"><td>Size</td><td>" + data["smtp_events"][i]["size"] + "</td></tr>");
                    table += ("</table></td></tr>");
                }

                table += "</tbody></table>";
            }
            $("#latest_info_modal_inside").empty();
            $("#latest_info_modal_inside").html(table);


        });
    })(jQuery);
}