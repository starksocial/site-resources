<?php
if (!defined('ABSPATH')) {
    die('Direct access of plugin file not allowed');
}

//wp_nonce_field( '' );

wp_enqueue_script('jquery');
wp_register_script('wpmaildrill_datatables', plugins_url('../js/datatables/jquery.dataTables.min.js', __FILE__));
wp_enqueue_script('wpmaildrill_datatables');

wp_register_style('wpmaildrill_datatables_css', plugins_url('../styles/datatables/jquery.dataTables.min.css', __FILE__));
wp_enqueue_style('wpmaildrill_datatables_css');

wp_register_script('wpmaildrill_email_sent_log', plugins_url('js/wpmaildrill_email_sent_log.js', __FILE__));
wp_enqueue_script('wpmaildrill_email_sent_log');

add_thickbox();

global $wpdb;
$table_name_wpmaildrill_email_send_log = $wpdb->prefix . "wpmaildrill_email_send_log";


if (isset($_POST['delete_attempt']) && $_POST['delete_attempt'] == "Delete") {
    
}
$query = "SELECT * FROM $table_name_wpmaildrill_email_send_log ORDER BY id desc";
$result = $wpdb->get_results($query);
?>
<div class="wrap">
    <img src="<?php echo(plugins_url('../images/wpmaildrill_header_logo.png', __FILE__)); ?>" style="height:26px; margin-right:5px; float:left;padding-top:8px" >
    <h2> - Sent Email Log</h2>
    <?php include 'wpmaildrill_admin_menu.php'; ?>

    <div id ="poststuff">
        <div id="post-body" class="metabox-holder columns-1">
            <div class="postbox-container-1" class="postbox-container">



                <div class="postbox">
                    <h3 class="hndle"><span>Email History</span></h3>
                    <div class="inside">
                        <p>Below are the emails that WPMailDrill has sent.
                            You can also view more details by clicking 'More Details'</p>
                        <table id="wpmaildrill_sent_emails" class="display widefat">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Sent Date</th>					
                                    <th>To</th>
                                    <th>Subject</th>
                                    <th>Initial Mandrill Status</th>
                                    <th>View More Details</th>
                                    <th>View Latest Statistics</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $alt = true;
                                foreach ($result as $row) {
                                    if (!$alt) {
                                        echo("<tr>");
                                    } else {
                                        echo('<tr class="alt">');
                                    }
                                    echo("<td>" . esc_html($row->id) . "</td>");
                                    echo("<td>" . esc_html($row->created_date) . "</td>");
                                    echo("<td>" . esc_html($row->sent_to) . "</td>");
                                    echo("<td>" . esc_html($row->subject) . "</td>");
                                    echo("<td>" . esc_html($row->response_status) . "</td>");
                                    echo("<td><a href=\"#TB_inline?width=800&height=850&inlineId=email_" . esc_html($row->id) . "\" class=\"thickbox\" title=\"Viewing data for email ID:  " . esc_html($row->id) . "\">View More</a></td>");
                                    if (strlen(esc_html($row->response_mandrill_id)) > 0) {
                                        if (sanitize_text_field(get_option('wpmaildrill_mandrill_apikey')) != '') {
                                            echo("<td><a href=\"#TB_inline?width=800&height=850&inlineId=latest_info_modal\" class=\"thickbox\" title=\"Viewing data for email ID:  " . esc_html($row->id) . "\" onclick=\"load_mandrill_info('" . esc_html($row->response_mandrill_id) . "','" . esc_html(wp_create_nonce("wpmaildrill_get_sent_email_" . $row->response_mandrill_id)) . "')\">View Latest Stats</a></td>");
                                        } else {
                                            echo("<td>Please provide your Mandrill key to see the latest statistics.</td>");
                                        }
                                    } else {
                                        echo("<td></td>");
                                    }
                                    $alt = !$alt;
                                    echo("</tr>");
                                }
                                ?>



                            </tbody>
                        </table>
                        <?php
                        foreach ($result as $row) {
                            ?>
                            <div id="email_<?php echo(esc_html($row->id)) ?>" style="display:none;">
                                <table class="display widefat">
                                    <thead><tr><th>Item</th><th>Value</th></tr></thead>
                                    <tbody>

                                        <?php
                                        echo("<tr><td>ID</td><td>" . esc_html($row->id) . "</td></tr>");
                                        echo("<tr class=\"alt\"><td>Sent Date</td><td>" . esc_html($row->created_date) . "</td></tr>");
                                        echo("<tr><td>To</td><td>" . esc_html($row->sent_to) . "</td></tr>");
                                        echo("<tr class=\"alt\"><td>Subject</td><td>" . esc_html($row->subject) . "</td></tr>");
                                        echo("<tr><td>Message</td><td><textarea rows=\"4\" cols=\"50\">" . esc_html($row->message) . "</textarea></td></tr>");
                                        echo("<tr class=\"alt\"><td>Raw Data Sent</td><td><textarea rows=\"4\" cols=\"50\">" . esc_html($row->data_sent) . "</textarea></td></tr>");
                                        echo("<tr><td>Mandrill Response ID</td><td>" . esc_html($row->response_mandrill_id) . "</td></tr>");
                                        echo("<tr class=\"alt\"><td>Mandrill Response Status</td><td>" . esc_html($row->response_status) . "</td></tr>");
                                        echo("<tr><td>Mandrill Response Email</td><td>" . esc_html($row->response_email) . "</td></tr>");
                                        echo("<tr class=\"alt\"><td>Mandrill Response Reject Reason</td><td>" . esc_html($row->response_reject_reason) . "</td></tr>");
                                        echo("<tr><td>Mandrill Response Error Code</td><td>" . esc_html($row->response_code) . "</td></tr>");
                                        echo("<tr class=\"alt\"><td>Mandrill Response Error Name</td><td>" . esc_html($row->response_name) . "</td></tr>");
                                        echo("<tr><td>Mandrill Response Error Message</td><td>" . esc_html($row->response_message) . "</td></tr>");
                                        echo("<tr class=\"alt\"><td>Mandrill Raw Response </td><td><textarea rows=\"4\" cols=\"50\">" . esc_html($row->response_data) . "</textarea></td></tr>");
                                        ?>



                                    </tbody>
                                </table>
                            </div>
                            <?php
                        }
                        ?>

                        <div id="latest_info_modal" style="display:none;">
                            <div id = "latest_info_modal_inside">

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


