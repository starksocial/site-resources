<?php
if (!defined('ABSPATH')) {
    die('Direct access of plugin file not allowed');
}
?>
<h2 class="nav-tab-wrapper">
    <a href="admin.php?page=wpmaildrill_about" class="nav-tab">About</a>
    <a href="admin.php?page=wpmaildrill_settings" class="nav-tab">Settings</a>
    <a href="admin.php?page=wpmaildrill_analytics" class="nav-tab">Analytics</a>
    <a href="admin.php?page=wpmaildrill_sent_email_log" class="nav-tab">Sent Email log</a>
</h2>