<?php

if (!defined('ABSPATH')) {
    die('Direct access of plugin file not allowed');
}

//if uninstall not called from WordPress exit
if (!defined('WP_UNINSTALL_PLUGIN'))
    exit();

$option_name = 'plugin_option_name';

// For Single site
if (!is_multisite()) {
    //Remove options
    delete_option('wpmaildrill_version', '1.0.0', '', 'yes');
    delete_option('wpmaildrill_last_update', '19th December 2014', '', 'yes');
    delete_option('wpmaildrill_mandrill_apikey', '', '', 'yes');
    delete_option('wpmaildrill_mandrill_active', '0', '', 'yes');
    delete_option('wpmaildrill_store_sent_emails', '1', '', 'yes');
    delete_option('wpmaildrill_store_raw_data', '0', '', 'yes');
    delete_option('wpmaildrill_track_opens', '1', '', 'yes');
    delete_option('wpmaildrill_track_clicks', '1', '', 'yes');
    //Remove DB

    global $wpdb;
    $table_name = $wpdb->prefix . "wpmaildrill_email_send_log";
    $result = $wpdb->query("DROP TABLE $table_name") or die(mysql_error());
}
// For Multisite
else {
    // For regular options.
    global $wpdb;
    $blog_ids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
    $original_blog_id = get_current_blog_id();
    foreach ($blog_ids as $blog_id) {
        switch_to_blog($blog_id);
        delete_option('wpmaildrill_version', '1.0.0', '', 'yes');
        delete_option('wpmaildrill_last_update', '19th December 2014', '', 'yes');
        delete_option('wpmaildrill_mandrill_apikey', '', '', 'yes');
        delete_option('wpmaildrill_mandrill_active', '0', '', 'yes');
        delete_option('wpmaildrill_store_sent_emails', '1', '', 'yes');
        delete_option('wpmaildrill_store_raw_data', '0', '', 'yes');
        delete_option('wpmaildrill_track_opens', '1', '', 'yes');
        delete_option('wpmaildrill_track_clicks', '1', '', 'yes');

        //Remove DB

        global $wpdb;
        $table_name = $wpdb->prefix . "wpmaildrill_email_send_log";
        $result = $wpdb->query("DROP TABLE $table_name") or die(mysql_error());
    }
    switch_to_blog($original_blog_id);
}
?>