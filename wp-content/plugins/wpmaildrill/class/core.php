<?php

class wpmaildrill_core {

    public static function send_email($to, $subject, $message, $headers = '', $attachments = array()) {
        if (get_option('wpmaildrill_mandrill_active') == '0') {
            return @mail($to, $subject, $message, $headers);
        }
        require_once(plugin_dir_path(__FILE__) . 'email_send_log.php');
        require_once(plugin_dir_path(__FILE__) . '../api/Mandrill.php');

        $store_log = FALSE;
        $store_raw_log = FALSE;
        if (get_option('wpmaildrill_store_sent_emails') == '1') {
            $store_log = true;
        }
        if (get_option('wpmaildrill_store_raw_data') == '1') {
            $store_raw_log = true;
        }

        $email_send_log = new email_send_log();
        if ($store_log) {
            $email_id = $email_send_log->save_log($to, $subject, $message, $headers);
        }

        // Compact the input, apply the filters, and extract them back out

        /**
         * Filter the wp_mail() arguments.
         *
         * @since 2.2.0
         *
         * @param array $args A compacted array of wp_mail() arguments, including the "to" email,
         *                    subject, message, headers, and attachments values.
         */
        $atts = apply_filters('wp_mail', compact('to', 'subject', 'message', 'headers', 'attachments'));

        if (isset($atts['to'])) {
            $to = $atts['to'];
        }

        if (isset($atts['subject'])) {
            $subject = $atts['subject'];
        }

        if (isset($atts['message'])) {
            $message = $atts['message'];
        }

        if (isset($atts['headers'])) {
            $headers = $atts['headers'];
        }

        if (isset($atts['attachments'])) {
            $attachments = $atts['attachments'];
        }

        if (!is_array($attachments)) {
            $attachments = explode("\n", str_replace("\r\n", "\n", $attachments));
        }
        global $phpmailer;

        // (Re)create it, if it's gone missing
        if (!is_object($phpmailer) || !is_a($phpmailer, 'PHPMailer')) {
            require_once ABSPATH . WPINC . '/class-phpmailer.php';
            require_once ABSPATH . WPINC . '/class-smtp.php';
            $phpmailer = new PHPMailer(true);
        }

        // Headers
        if (empty($headers)) {
            $headers = array();
        } else {
            if (!is_array($headers)) {
                // Explode the headers out, so this function can take both
                // string headers and an array of headers.
                $tempheaders = explode("\n", str_replace("\r\n", "\n", $headers));
            } else {
                $tempheaders = $headers;
            }
            $headers = array();
            $cc = array();
            $bcc = array();

            // If it's actually got contents
            if (!empty($tempheaders)) {
                // Iterate through the raw headers
                foreach ((array) $tempheaders as $header) {
                    if (strpos($header, ':') === false) {
                        if (false !== stripos($header, 'boundary=')) {
                            $parts = preg_split('/boundary=/i', trim($header));
                            $boundary = trim(str_replace(array("'", '"'), '', $parts[1]));
                        }
                        continue;
                    }
                    // Explode them out
                    list( $name, $content ) = explode(':', trim($header), 2);

                    // Cleanup crew
                    $name = trim($name);
                    $content = trim($content);

                    switch (strtolower($name)) {
                        // Mainly for legacy -- process a From: header if it's there
                        case 'from':
                            if (strpos($content, '<') !== false) {
                                // So... making my life hard again?
                                $from_name = substr($content, 0, strpos($content, '<') - 1);
                                $from_name = str_replace('"', '', $from_name);
                                $from_name = trim($from_name);

                                $from_email = substr($content, strpos($content, '<') + 1);
                                $from_email = str_replace('>', '', $from_email);
                                $from_email = trim($from_email);
                            } else {
                                $from_email = trim($content);
                            }
                            break;
                        case 'content-type':
                            if (strpos($content, ';') !== false) {
                                list( $type, $charset ) = explode(';', $content);
                                $content_type = trim($type);
                                if (false !== stripos($charset, 'charset=')) {
                                    $charset = trim(str_replace(array('charset=', '"'), '', $charset));
                                } elseif (false !== stripos($charset, 'boundary=')) {
                                    $boundary = trim(str_replace(array('BOUNDARY=', 'boundary=', '"'), '', $charset));
                                    $charset = '';
                                }
                            } else {
                                $content_type = trim($content);
                            }
                            break;
                        case 'cc':
                            $cc = array_merge((array) $cc, explode(',', $content));
                            break;
                        case 'bcc':
                            $bcc = array_merge((array) $bcc, explode(',', $content));
                            break;
                        default:
                            // Add it to our grand headers array
                            $headers[trim($name)] = trim($content);
                            break;
                    }
                }
            }
        }

        // Empty out the values that may be set
        $phpmailer->ClearAllRecipients();
        $phpmailer->ClearAttachments();
        $phpmailer->ClearCustomHeaders();
        $phpmailer->ClearReplyTos();

        // From email and name
        // If we don't have a name from the input headers
        if (!isset($from_name))
            $from_name = 'WordPress';

        /* If we don't have an email from the input headers default to wordpress@$sitename
         * Some hosts will block outgoing mail from this address if it doesn't exist but
         * there's no easy alternative. Defaulting to admin_email might appear to be another
         * option but some hosts may refuse to relay mail from an unknown domain. See
         * http://trac.wordpress.org/ticket/5007.
         */

        if (!isset($from_email)) {
            // Get the site domain and get rid of www.
            $sitename = strtolower($_SERVER['SERVER_NAME']);
            if (substr($sitename, 0, 4) == 'www.') {
                $sitename = substr($sitename, 4);
            }

            $from_email = 'wordpress@' . $sitename;

            if (strpos($from_email, '.') === false) {
                //Last ditch attempt to make a valid domain name
                $from_email .= ".com";
            }
        }

        /**
         * Filter the email address to send from.
         *
         * @since 2.2.0
         *
         * @param string $from_email Email address to send from.
         */
        //$phpmailer->From = apply_filters('wp_mail_from', $from_email);

        /**
         * Filter the name to associate with the "from" email address.
         *
         * @since 2.3.0
         *
         * @param string $from_name Name associated with the "from" email address.
         */
        //$phpmailer->FromName = apply_filters('wp_mail_from_name', $from_name);
        // Set destination addresses
        $to_array = array();
        if (!is_array($to))
            $to = explode(',', $to);

        foreach ((array) $to as $recipient) {
            try {
                // Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
                $recipient_name = '';
                if (preg_match('/(.*)<(.+)>/', $recipient, $matches)) {
                    if (count($matches) == 3) {
                        $recipient_name = $matches[1];
                        $recipient = $matches[2];
                    }
                }
                $to_array[] = array(
                    'email' => $recipient,
                    'name' => $recipient_name,
                    'type' => 'to'
                );
                //$phpmailer->AddAddress($recipient, $recipient_name);
            } catch (phpmailerException $e) {
                continue;
            }
        }

        // Set mail's subject and body
        //$phpmailer->Subject = $subject;
        //$phpmailer->Body = $message;
        // Add any CC and BCC recipients
        if (!empty($cc)) {
            foreach ((array) $cc as $recipient) {
                try {
                    // Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
                    $recipient_name = '';
                    if (preg_match('/(.*)<(.+)>/', $recipient, $matches)) {
                        if (count($matches) == 3) {
                            $recipient_name = $matches[1];
                            $recipient = $matches[2];
                        }
                    }
                    $to_array[] = array(
                        'email' => $recipient,
                        'name' => $recipient_name,
                        'type' => 'cc'
                    );
                    //$phpmailer->AddCc($recipient, $recipient_name);
                } catch (phpmailerException $e) {
                    continue;
                }
            }
        }

        if (!empty($bcc)) {
            foreach ((array) $bcc as $recipient) {
                try {
                    // Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
                    $recipient_name = '';
                    if (preg_match('/(.*)<(.+)>/', $recipient, $matches)) {
                        if (count($matches) == 3) {
                            $recipient_name = $matches[1];
                            $recipient = $matches[2];
                        }
                    }
                    $to_array[] = array(
                        'email' => $recipient,
                        'name' => $recipient_name,
                        'type' => 'bcc'
                    );
                    //$phpmailer->AddBcc($recipient, $recipient_name);
                } catch (phpmailerException $e) {
                    continue;
                }
            }
        }

        // Set to use PHP's mail()
        $phpmailer->IsMail();

        // Set Content-Type and charset
        // If we don't have a content-type from the input headers
        if (!isset($content_type))
            $content_type = 'text/plain';

        /**
         * Filter the wp_mail() content type.
         *
         * @since 2.3.0
         *
         * @param string $content_type Default wp_mail() content type.
         */
        //$content_type = apply_filters('wp_mail_content_type', $content_type);
        //$phpmailer->ContentType = $content_type;
        // Set whether it's plaintext, depending on $content_type
        $html_content = "";
        $plain_text_content = "";
        if ('text/html' == $content_type) {
            $html_content = $message;
            //$phpmailer->IsHTML(true);
        } else {
            $plain_text_content = $message;
        }


        // If we don't have a charset from the input headers
        if (!isset($charset))
            $charset = get_bloginfo('charset');

        // Set the content-type and charset

        /**
         * Filter the default wp_mail() charset.
         *
         * @since 2.3.0
         *
         * @param string $charset Default email charset.
         */
        //$phpmailer->CharSet = apply_filters('wp_mail_charset', $charset);
        // Set custom headers
        $header_array = array();
        if (!empty($headers)) {
            foreach ((array) $headers as $name => $content) {
                //$phpmailer->AddCustomHeader(sprintf('%1$s: %2$s', $name, $content));
                $header_array[$name] = $content;
            }


            if (false !== stripos($content_type, 'multipart') && !empty($boundary))
            //$phpmailer->AddCustomHeader(sprintf("Content-Type: %s;\n\t boundary=\"%s\"", $content_type, $boundary));
                $header_array["Content-Type"] = sprintf("%s;\n\t boundary=\"%s\"", $content_type, $boundary);
        }


        //Send unknown file types as application/octet-stream
        $attachment_array = array();
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                try {
                    $attachment_array[] = array(
                        'type' => 'application/octet-stream',
                        'name' => basename($attachment),
                        'content' => base64_encode(file_get_contents($attachment))
                    );
                    //echo(file_get_contents( $attachment ));
                    // $phpmailer->AddAttachment($attachment);
                } catch (phpmailerException $e) {
                    continue;
                }
            }
        }
        // print_r($attachment_array);

        /**
         * Fires after PHPMailer is initialized.
         *
         * @since 2.2.0
         *
         * @param PHPMailer &$phpmailer The PHPMailer instance, passed by reference.
         */
        //do_action_ref_array( 'phpmailer_init', array( &$phpmailer ) );
        // Send!
        try {
            //return $phpmailer->Send();
        } catch (phpmailerException $e) {
            return false;
        }
        $track_opens = "false";
        if (get_option('wpmaildrill_track_opens') == '1') {
            $track_opens = "true";
        }
        $track_clicks = "false";
        if (get_option('wpmaildrill_track_clicks') == '1') {
            $track_clicks = "true";
        }
        $mandrill_key = sanitize_text_field(get_option('wpmaildrill_mandrill_apikey'));
        $mandrill = new Mandrill($mandrill_key);
        $message = array(
            'html' => $html_content,
            'text' => $plain_text_content,
            'subject' => $subject,
            'from_email' => apply_filters('wp_mail_from', $from_email),
            'from_name' => apply_filters('wp_mail_from_name', $from_name),
            'to' => $to_array,
            'headers' => $header_array,
            'important' => false,
            'track_opens' => $track_opens,
            'track_clicks' => $track_clicks,
            'auto_text' => null,
            'auto_html' => null,
            'inline_css' => null,
            'url_strip_qs' => null,
            'preserve_recipients' => null,
            'view_content_link' => null,
            'tracking_domain' => null,
            'signing_domain' => null,
            'return_path_domain' => null,
            'attachments' => $attachment_array
        );
        if ($store_log) {
            if ($store_raw_log) {
                $email_send_log->update_log_with_send_data(json_encode($message), $email_id);
            } else {
                $email_send_log->update_log_with_send_data("", $email_id);
            }
        }

        $async = false;
        $result = $mandrill->messages->send($message, $async);
        if ($store_log) {
            $email_send_log->update_log_with_response_data($result, $email_id);
        }
        //print_r($result);
        return true;
        // 
    }

}
