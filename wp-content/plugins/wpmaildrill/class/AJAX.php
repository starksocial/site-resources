<?php

class wpmaildrill_AJAX {

    public static function wpmaildrill_load_mandrill_info() {
        $message_id = sanitize_text_field($_POST['messageId']);
        check_ajax_referer('wpmaildrill_get_sent_email_' . $message_id);
        include(plugin_dir_path(__FILE__) . '../api/Mandrill.php');


        $mandrill_key = sanitize_text_field(get_option('wpmaildrill_mandrill_apikey'));
        try {
            $mandrill = new Mandrill($mandrill_key);


            $result = $mandrill->messages->info($message_id);
            $result["error"] = false;
        } catch (Exception $e) {
            $result["error"] = true;
        }

        echo json_encode($result);
        exit;
    }

    public static function wpmaildrill_load_mandrill_stats() {
        check_ajax_referer('wpmaildrill_analytics_page');
        include(plugin_dir_path(__FILE__) . '../api/Mandrill.php');

        $mandrill_key = sanitize_text_field(get_option('wpmaildrill_mandrill_apikey'));
        try {
            $mandrill = new Mandrill($mandrill_key);

            $query = '*';
            $date_from = null;
            $date_to = null;



            if (sanitize_text_field($_POST['from'] != '')) {
                $date_from = sanitize_text_field($_POST['from']);
            }

            if (sanitize_text_field($_POST['to'] != '')) {
                $date_to = sanitize_text_field($_POST['to']);
            }

            $result["stats"] = $mandrill->messages->searchTimeSeries($query, $date_from, $date_to);
            $result["error"] = false;
        } catch (Exception $e) {
            $result["error"] = true;
        }

        echo json_encode($result);
        exit;
    }

}

?>
