<?php

if (!defined('ABSPATH'))
    exit;

class wpmaildrill_install {

    public static function on_activate($network_wide) {
        global $wpdb;

        if (is_multisite() && $network_wide) {

            $current_blog = $wpdb->blogid;

            $blog_ids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
            foreach ($blog_ids as $blog_id) {
                switch_to_blog($blog_id);
                self::_activate();
                restore_current_blog();
            }
        } else {
            self::_activate();
        }
    }

    public static function _activate() {
        if (!get_option('wpmaildrill_version')) {
            add_option('wpmaildrill_version', '1.0.0', '', 'yes');
            add_option('wpmaildrill_last_update', '11th January 2015', '', 'yes');
            add_option('wpmaildrill_mandrill_apikey', '', '', 'yes');
            add_option('wpmaildrill_mandrill_active', '0', '', 'yes');
            add_option('wpmaildrill_store_sent_emails', '1', '', 'yes');
            add_option('wpmaildrill_store_raw_data', '0', '', 'yes');
            add_option('wpmaildrill_track_opens', '1', '', 'yes');
            add_option('wpmaildrill_track_clicks', '1', '', 'yes');
        } else {
//Upgrade plan
        }
        //Create Database
        global $wpdb;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        //Email Send Log
        $table_name = $wpdb->prefix . "wpmaildrill_email_send_log";

        $sql_email_send_log = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        created_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        sent_to text NOT NULL,
        subject text NOT NULL,
        message text NOT NULL,
        headers text NOT NULL,
        sent_date datetime NULL,
        data_sent text NULL,
        response_date datetime NULL,
        response_data text NULL,
        response_email text NULL,
        response_status text NULL,
        response_reject_reason text NULL,
        response_mandrill_id text NULL,
        response_code text NULL,
        response_name text NULL,
        response_message text NULL,        
        UNIQUE KEY id (id)
        );";



        $delta = dbDelta($sql_email_send_log);
    }

    public static function on_create_blog($blog_id, $user_id, $domain, $path, $site_id, $meta) {
        if (is_plugin_active_for_network('wpmaildrill/wpmaildrill.php')) {
            switch_to_blog($blog_id);
            self::_activate();
            restore_current_blog();
        }
    }

    public static function on_delete_blog($tables) {
        global $wpdb;
        $tables[] = $wpdb->$wpdb->prefix . "wpmaildrill_email_send_log";
        return $tables;
    }

}

register_activation_hook(WPMAILDRILL_PLUGIN_FILE, array('wpmaildrill_install', 'on_activate'));

add_action('wpmu_new_blog', array('wpmaildrill_install', 'on_create_blog'), 10, 6);

add_filter('wpmu_drop_tables', array('wpmaildrill_install', 'on_delete_blog'));
?>
