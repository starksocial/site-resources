<?php

class email_send_log {

    public function save_log($to, $subject, $message, $headers) {

        //require_once('../../../../wp-load.php');
        require_once(ABSPATH . 'wp-includes/formatting.php');
        global $wpdb;
        $table_name = $wpdb->prefix . "wpmaildrill_email_send_log";
        $rows_affected = $wpdb->insert($table_name, array(
            'created_date' => current_time('mysql'),
            'sent_to' => sanitize_text_field($to),
            'subject' => sanitize_text_field($subject),
            'message' => sanitize_text_field($message),
            'headers' => sanitize_text_field($headers)
        ));
        return $wpdb->insert_id;
        ;
    }

    public function update_log_with_send_data($sent_data, $email_id) {
        require_once(ABSPATH . 'wp-includes/formatting.php');
        global $wpdb;
        $table_name = $wpdb->prefix . "wpmaildrill_email_send_log";

        $wpdb->update(
                $table_name, array(
            'data_sent' => sanitize_text_field($sent_data),
            'sent_date' => current_time('mysql'),
                ), array('ID' => sanitize_text_field($email_id)), array(
            '%s', // value1
            '%s' // value2
                ), array('%d')
        );
    }

    public function update_log_with_response_data($response, $email_id) {
        require_once(ABSPATH . 'wp-includes/formatting.php');
        global $wpdb;
        $table_name = $wpdb->prefix . "wpmaildrill_email_send_log";

        $wpdb->update(
                $table_name, array(
            'response_data' => sanitize_text_field(json_encode($response)),
            'response_email' => $this->safe_get_array_value($response[0], 'email'),
            'response_status' => $this->safe_get_array_value($response[0], 'status'),
            'response_reject_reason' => $this->safe_get_array_value($response[0], 'reject_reason'),
            'response_mandrill_id' => $this->safe_get_array_value($response[0], '_id'),
            'response_code' => $this->safe_get_array_value($response[0], 'code'),
            'response_name' => $this->safe_get_array_value($response[0], 'name'),
            'response_message' => $this->safe_get_array_value($response[0], 'message'),
            'response_date' => current_time('mysql'),
                ), array('ID' => sanitize_text_field($email_id)), array(
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
                ), array('%d')
        );
    }

    private function safe_get_array_value($array, $key) {
        if (array_key_exists($key, $array)) {
            return sanitize_text_field($array[$key]);
        } else {
            return null;
        }
    }

}

?>
